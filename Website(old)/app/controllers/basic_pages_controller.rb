class BasicPagesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :menu_stats, only: [:dashboard]
  before_filter :graphs, only: [:dashboard]
  layout 'dashboard'
  
  def dashboard
    @invoices = LedgerInfo.where(recipient: current_user.email).order("created_at DESC")
    @invoices_this_month = @invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @invoices_this_month_count = @invoices_this_month.count
    @total_expenses_this_month = @invoices_this_month.sum :total_with_taxes
    
    @invoices_new = @invoices.where(ledger_status: "New")
    @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @invoices_new_this_month_count = @invoices_new_this_month.count
    
    @unpaid_invoices_last_month = @invoices_new.where(created_at: 1.month.ago.beginning_of_month..1.month.ago.end_of_month)
    @unpaid_invoices_last_month_count = @unpaid_invoices_last_month.count
    
    @invoices_paid = @invoices.where(ledger_status: "Paid")
    @invoices_paid_this_month_count = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
    @invoices_paid_this_month_sum = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
    @balance_this_month =  @invoices_paid_this_month_sum - @total_expenses_this_month
   
    @progress = percent_of(@invoices_this_month_count, @invoices_paid_this_month_count)
   
    @invoice_total = @invoices.count
  end
  
  private
  def graphs
    @invoices = LedgerInfo.where(recipient: current_user.email)
    
    if Rails.env.development?
      @this_year_invoices = @invoices.where("strftime('%Y', created_at) + 0 = ?", Date.current.year)
      @this_year_paid_invoices = @this_year_invoices.where(ledger_status: "Paid")
    
      @this_month_invoices = @invoices.where("strftime('%m', created_at)= ?", Date.today.strftime("%m"))
      @this_month_paid_invoices = @this_month_invoices.where(ledger_status: "Paid")
    else
      @this_year_invoices = @invoices.where("extract(year from created_at)=?", Date.current.year)
      @this_year_paid_invoices = @this_year_invoices.where(ledger_status: "Paid")
    
      @this_month_invoices = @invoices.where("extract(month from created_at)=?", Date.today.strftime("%m"))
      @this_month_paid_invoices = @this_month_invoices.where(ledger_status: "Paid")
    end
  end
  
  def menu_stats
    @invoices = LedgerInfo.where(recipient: current_user.email).order("created_at DESC") 
    @invoices_new = @invoices.where(ledger_status: "New")
    @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @invoices_new_this_month_count = @invoices_new_this_month.count
    
    @invoices_paid = @invoices.where(ledger_status: "Paid")
    @invoices_paid_this_month_count = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
    @invoices_paid_this_month_sum = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
  end
  
  def percent_of(total, paid)
    if total == 0 and paid == 0
      0
    else 
      paid.to_f / total.to_f * 100.0
    end
  end
end
