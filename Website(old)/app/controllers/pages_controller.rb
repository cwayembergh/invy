class PagesController < ApplicationController
  layout "pages"
  
  def home
  end
  
  def beta_access
    @beta_access = Whitelist.new
  end
end
