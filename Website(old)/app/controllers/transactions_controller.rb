class TransactionsController < ApplicationController
  before_filter :invoices_stats, only: [:new, :create]
  layout 'dashboard'
  
  def new
    @invoice=LedgerInfo.find_by_token!(params[:id])
    gon.client_token = generate_client_token
  end
  
  def create
    #TODO ADD A CHECK ON THE FIND_BY_TOKEN (RECORD NOT FOUND) IF SOMEONE CHANGES THE TOKEN
    @invoice=LedgerInfo.find_by_token!(params[:token])
    if user_signed_in?
      unless current_user.has_payment_info?
        @result = Braintree::Transaction.sale(
                    amount: @invoice.total_with_taxes,
                    payment_method_nonce: params[:payment_method_nonce],
                    customer: {
                      first_name: params[:first_name],
                      last_name: params[:last_name],
                      email: current_user.email,
                      phone: params[:phone]
                    },
                    options: {
                      store_in_vault: true
                    })
      else
        @result = Braintree::Transaction.sale(amount: @invoice.total_with_taxes, payment_method_nonce: params[:payment_method_nonce])
      end
    elsif corporate_signed_in?
      unless current_corporate.has_payment_info?
        @result = Braintree::Transaction.sale(
                    amount: @invoice.total_with_taxes,
                    payment_method_nonce: params[:payment_method_nonce],
                    customer: {
                      first_name: params[:first_name],
                      last_name: params[:last_name],
                      company: params[:company],
                      email: current_corporate.email,
                      phone: params[:phone]
                    },
                    options: {
                      store_in_vault: true
                    })
        else
        @result = Braintree::Transaction.sale(amount: @invoice.total_with_taxes, payment_method_nonce: params[:payment_method_nonce])
      end
    end
    @invoice.ledger_status = "Paid"
    @invoice.paid_date = Time.now
    
    if @result.transaction.payment_instrument_type == "paypal_account"
      @invoice.payment_method = "PayPal"
    elsif @result.transaction.payment_instrument_type == "credit_card"
        @invoice.payment_method = "Credit Card"
      end
      if @result.success? and @invoice.save
        if user_signed_in?
          current_user.update(braintree_customer_id: @result.transaction.customer_details.id) unless current_user.has_payment_info?
        elsif corporate_signed_in?
          current_corporate.update(braintree_customer_id: @result.transaction.customer_details.id) unless current_corporate.has_payment_info?
        end
        redirect_to root_path, notice: "Your transaction has been successfuly processed!"
      else
        flash[:alert] = "Something went wrong while processing your transaction. Please try again!"
        gon.client_token = generate_client_token
        render :new
      end
    end
  
  
  private
  
  def generate_client_token
    if user_signed_in?
      if current_user.has_payment_info?
        Braintree::ClientToken.generate(customer_id: current_user.braintree_customer_id)
      else
        Braintree::ClientToken.generate
      end
    elsif corporate_signed_in?
      if current_corporate.has_payment_info?
        Braintree::ClientToken.generate(customer_id: current_corporate.braintree_customer_id)
      else
        Braintree::ClientToken.generate
      end
    end
  end
  
  def invoices_stats
    if user_signed_in?
      @invoices = LedgerInfo.where(recipient: current_user.email).order("created_at DESC")
      @invoices_this_month = @invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @invoices_this_month_count = @invoices_this_month.count
      @total_expenses_this_month = @invoices_this_month.sum :total_with_taxes
      
      @invoices_new = @invoices.where(ledger_status: "New")
      @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @invoices_new_this_month_count = @invoices_new_this_month.count
      
      @invoices_paid = @invoices.where(ledger_status: "Payment received")
      @invoices_paid_this_month_count = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
      @invoices_paid_this_month_sum = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
      
    elsif corporate_signed_in?
      @invoices = LedgerInfo.where(recipient: current_corporate.email).order("created_at DESC")
      @invoices_this_month = @invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @invoices_this_month_count = @invoices_this_month.count
      @total_expenses_this_month = @invoices_this_month.sum :total_with_taxes
      
      @invoices_new = @invoices.where(ledger_status: "New")
      @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @invoices_new_this_month_count = @invoices_new_this_month.count
      
      @invoices_paid = @invoices.where(ledger_status: "Payment received")
      @invoices_paid_this_month_count = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
      @invoices_paid_this_month_sum = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
      
      @sent_invoices = LedgerInfo.where(sender: current_corporate.email).order("created_at DESC")
      @sent_invoices_this_month = @sent_invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @sent_invoices_this_month_count = @sent_invoices_this_month.count
      @sent_invoices_this_month_sum = @sent_invoices_this_month.sum :total_with_taxes
      
      @sent_invoices_paid = @sent_invoices.where(ledger_status: "Payment received")
      @sent_invoices_paid_this_month = @sent_invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @sent_invoices_paid_this_month_count = @sent_invoices_paid_this_month.count
      @sent_invoices_paid_this_month_sum = @sent_invoices_paid_this_month.sum :total_with_taxes
    end
  end
end
