class Accountants::RegistrationsController < Devise::RegistrationsController
  protect_from_forgery with: :exception
  before_filter :configure_sign_up_params, only: [:create]
  before_filter :configure_account_update_params, only: [:update]
  before_filter :invoice_stats, only: [:edit, :update]
  
  layout :resolve_layout

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    if Whitelist.exists?(:email => params[:accountant][:email])
      if Whitelist.where(:email => params[:accountant][:email]).take.request_type != "accountant"
        flash[:warning] = "Unfortunately you cannot create an accountant account.<br>You have access to a <strong>#{Whitelist.where(:email => params[:accountant][:email]).take.request_type}</strong> account."
        redirect_to new_accountant_registration_path
      else
        super
      end
    else
      flash[:warning] = "Your email is not registered in our beta list."
      redirect_to new_accountant_registration_path
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  def cancel
    super
  end

  protected

  # You can put the params you want to permit in the empty array.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_name, :last_name, :email, :avatar, :password, :password_confirmation, :address, :postal_code, :city, :telephone, :bank_account, :terms_and_conditions, :last_name, :address_number, :business_name)}
  end

  # You can put the params you want to permit in the empty array.
  def configure_account_update_params
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:first_name, :last_name, :email, :avatar, :password, :password_confirmation, :current_password, :address, :postal_code, :city, :telephone, :bank_account, :last_name, :address_number, :business_name)}
  end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    super(resource)
  end

  # The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    super(resource)
  end
  
  def after_update_path_for(resource)
    edit_accountant_registration_path(resource)
  end
  
  #INVOICE
  def invoice_stats
    @invoices = LedgerInfo.where(recipient: current_accountant.email).order("created_at DESC")
    @total_expenses = LedgerInfo.where(recipient: current_accountant.email).order("created_at DESC").sum :total_with_taxes
    @payments_sent = @invoices.where(ledger_status: "Payment sent").count
    @paid_invoices_count = @invoices.where(ledger_status: "Paid").count
    @sent_invoices = LedgerInfo.where(sender: current_accountant.email)
    @payments_received = @sent_invoices.where(ledger_status: "Payment sent").count
    
    @invoice_total = @invoices.count
    @invoice_count = @invoices.where(ledger_status: "New").count
  end
  
  def resolve_layout
    case action_name
    when "new", "create", "delete"
      'pages'
    when "edit", "update"
      'dashboard'
    end
  end
end
