class CorporatePagesController < ApplicationController
  before_filter :authenticate_corporate!
  before_filter :menu_stats, only: [:accounting, :dashboard, :received_payments, :sent_invoices]
  before_filter :graphs, only: [:dashboard]
  layout 'dashboard'
  
  def accounting
    @accountant = Accountant.where(id: current_corporate.accountant_id)
    @sent_invoices = LedgerInfo.where(sender: current_corporate.email).order("created_at DESC")
    @sent_invoices_this_month = @sent_invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @sent_invoices_this_month_list = @sent_invoices_this_month.paginate(page: params[:page], per_page: 10)
    @sent_invoices_this_month_count = @sent_invoices_this_month.count
    @sent_invoices_this_month_to_accountant = @sent_invoices_this_month.where(sent_to_accountant: true)
    @sent_invoices_this_month_to_accountant_count = @sent_invoices_this_month_to_accountant.count
    @progress_sent_to_accountant = percent_of(@sent_invoices_this_month_count,@sent_invoices_this_month_to_accountant_count)
  end
  
  def dashboard
    @invoices = LedgerInfo.where(recipient: current_corporate.email).order("created_at DESC")
    @invoices_this_month = @invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @invoices_this_month_count = @invoices_this_month.count
    @total_expenses_this_month = @invoices_this_month.sum :total_with_taxes
    
    @invoices_new = @invoices.where(ledger_status: "New")
    @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @invoices_new_this_month_count = @invoices_new_this_month.count
    
    @invoices_paid = @invoices.where(ledger_status: "Paid")
    @invoices_paid_this_month = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @invoices_paid_this_month_count = @invoices_paid_this_month.count
    @invoices_paid_this_month_sum = @invoices_paid_this_month.sum :total_with_taxes
    
    @sent_invoices = LedgerInfo.where(sender: current_corporate.email).order("created_at DESC")
    @sent_invoices_this_month = @sent_invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @sent_invoices_this_month_count = @sent_invoices_this_month.count
    @sent_invoices_this_month_sum = @sent_invoices_this_month.sum :total_with_taxes
    
    @unpaid_invoices = @sent_invoices.where(ledger_status: "New")
    @unpaid_invoices_last_month = @unpaid_invoices.where(created_at: 1.month.ago.beginning_of_month..1.month.ago.end_of_month)
    @unpaid_invoices_last_month_count = @unpaid_invoices_last_month.count
    
    @sent_invoices_paid = @sent_invoices.where(ledger_status: "Paid")
    @sent_invoices_paid_this_month = @sent_invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @sent_invoices_paid_this_month_count = @sent_invoices_paid_this_month.count
    @sent_invoices_paid_this_month_sum = @sent_invoices_paid_this_month.sum :total_with_taxes
    
    @progress = percent_of(@invoices_this_month_count, @invoices_paid_this_month_count)
    @progress_sent_invoices = percent_of(@sent_invoices_this_month_count, @sent_invoices_paid_this_month_count)
    @balance_this_month =  @invoices_paid_this_month_sum - @total_expenses_this_month + (@sent_invoices_paid_this_month_sum - @sent_invoices_this_month_sum)
  end
  
  def received_payments
    @sent_invoices = LedgerInfo.where(sender: current_corporate.email).order("paid_date DESC")
    @sent_invoices_this_month = @sent_invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @sent_invoices_this_month_count = @sent_invoices_this_month.count
    @sent_invoices_this_month_sum = @sent_invoices_this_month.sum :total_with_taxes
    @payment_received_invoices = @sent_invoices.where(ledger_status: "Paid")
    @payment_received_invoices_this_month = @payment_received_invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @payment_received_invoices_this_month_list = @payment_received_invoices_this_month.paginate(page: params[:page], per_page: 10)
    @payment_received_invoices_this_month_count = @payment_received_invoices_this_month.count
    @payment_received_invoices_this_month_sum = @payment_received_invoices_this_month.sum :total_with_taxes    
    @progress_sent_invoices = percent_of(@sent_invoices_this_month_count, @payment_received_invoices_this_month_count)
  end
  
  def send_to_accountant
    @invoice=LedgerInfo.find_by_token!(params[:id])
    @invoice.sent_to_accountant = true
    if @invoice.save
      redirect_to accounting_path, notice: 'The invoice was successfully sent to your accountant.'
    else
      redirect_to accounting_path, notice: 'An error occured while sending the invoice. Please try again.'
    end
  end
  
  def sent_invoices
    @sent_invoices = LedgerInfo.where(sender: current_corporate.email).order("created_at DESC")
    @sent_invoices_this_month = @sent_invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @sent_invoices_this_month_list = @sent_invoices_this_month.paginate(page: params[:page], per_page: 10)
    @sent_invoices_this_month_count = @sent_invoices_this_month.count
    @all_sent_invoices = @sent_invoices.paginate(page: params[:page], per_page: 10)
  end
  
  
  
  private
  
  def graphs 
    @invoices = LedgerInfo.where(recipient: current_corporate.email)
    @sent_invoices = LedgerInfo.where(sender: current_corporate.email)
    
    if Rails.env.development?
      @this_year_invoices = @invoices.where("strftime('%Y', created_at) + 0 = ?", Date.current.year)
      @this_year_paid_invoices = @this_year_invoices.where(ledger_status: "Paid")
    
      @this_month_invoices = @invoices.where("strftime('%m', created_at)= ?", Date.today.strftime("%m"))
      @this_month_paid_invoices = @this_month_invoices.where(ledger_status: "Paid")
      
      @this_year_sent_invoices = @sent_invoices.where("strftime('%Y', created_at) + 0 = ?", Date.current.year)
      @this_year_paid_invoices_sent = @this_year_sent_invoices.where(ledger_status: "Paid")
      @this_month_sent_invoices = @sent_invoices.where("strftime('%m', created_at)= ?", Date.today.strftime("%m"))
    else
      @this_year_invoices = @invoices.where("extract(year from created_at)=?", Date.current.year)
      @this_year_paid_invoices = @this_year_invoices.where(ledger_status: "Paid")
    
      @this_month_invoices = @invoices.where("extract(month from created_at)=?", Date.today.strftime("%m"))
      @this_month_paid_invoices = @this_month_invoices.where(ledger_status: "Paid")
      
      @this_year_sent_invoices = @sent_invoices.where("extract(year from created_at)=?", Date.current.year)
      @this_year_paid_invoices_sent = @this_year_sent_invoices.where(ledger_status: "Paid")
      @this_month_sent_invoices = @sent_invoices.where("extract(month from created_at)=?", Date.today.strftime("%m"))
    end
  end
  
  def menu_stats
    @invoices = LedgerInfo.where(recipient: current_corporate.email).order("created_at DESC") 
    @invoices_new = @invoices.where(ledger_status: "New")
    @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @invoices_new_this_month_count = @invoices_new_this_month.count
    
    @invoices_paid = @invoices.where(ledger_status: "Paid")
    @invoices_paid_this_month_count = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
    @invoices_paid_this_month_sum = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
  end
  
  def percent_of(total, paid)
    if total == 0 and paid == 0
      0
    else 
      paid.to_f / total.to_f * 100.0
    end
  end
end
