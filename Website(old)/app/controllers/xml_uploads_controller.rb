class XmlUploadsController < ApplicationController
  before_filter :invoices_stats, only: [:new, :create]
  before_action :set_xml_upload, only: [:show, :edit, :update, :destroy]
  
  layout 'dashboard'

  # GET /xml_uploads/new
  def new
    @xml_upload = XmlUpload.new
    @xml_from_corporate = find_xml_from_corporate
    @xml_count = number_of_xml
    @ledger_items = get_ledger_item_table
    @ledger_infos = get_ledger_info_table
    @ledger_lines = get_ledger_line_table
  end
  
  # POST /xml_uploads
  # POST /xml_uploads.json
  def create
    @xml_upload = XmlUpload.new(xml_upload_params)
    @xml_upload.corporate_id = current_corporate.id
    @xml_from_corporate = find_xml_from_corporate
    @xml_count = number_of_xml
    @ledger_items = get_ledger_item_table
    @ledger_infos = get_ledger_info_table
    @ledger_lines = get_ledger_line_table
    respond_to do |format|
      if @xml_upload.save
        format.html { redirect_to parser_path, notice: 'XML file has been successfully uploaded' }
        format.json { render :show, status: :created, location: parser_path }
      else
        format.html { render :new }
        format.json { render json: @xml_upload.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xml_upload
      @xml_upload = XmlUpload.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def xml_upload_params
      params.require(:xml_upload).permit(:xml) if params[:xml_upload]
    end

    def invoices_stats
      @invoices = LedgerInfo.where(recipient: current_corporate.email).order("created_at DESC")
      @invoices_this_month = @invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @invoices_this_month_count = @invoices_this_month.count
      @total_expenses_this_month = @invoices_this_month.sum :total_with_taxes
      
      @invoices_new = @invoices.where(ledger_status: "New")
      @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @invoices_new_this_month_count = @invoices_new_this_month.count
      
      @invoices_paid = @invoices.where(ledger_status: "Paid")
      @invoices_paid_this_month_count = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
      @invoices_paid_this_month_sum = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
      
      @sent_invoices = LedgerInfo.where(sender: current_corporate.email).order("created_at DESC")
      @sent_invoices_this_month = @sent_invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @sent_invoices_this_month_count = @sent_invoices_this_month.count
      @sent_invoices_this_month_sum = @sent_invoices_this_month.sum :total_with_taxes
      
      @sent_invoices_paid = @sent_invoices.where(ledger_status: "Paid")
      @sent_invoices_paid_this_month = @sent_invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
      @sent_invoices_paid_this_month_count = @sent_invoices_paid_this_month.count
      @sent_invoices_paid_this_month_sum = @sent_invoices_paid_this_month.sum :total_with_taxes
    end
  
    #PARSER
    def find_xml_from_corporate
      XmlUpload.where(corporate_id: current_corporate.id)
    end
  
    def number_of_xml
      XmlUpload.where(corporate_id: current_corporate.id).count
    end
  
    def get_ledger_item_table
      files_ids = @xml_from_corporate.select(:id)
      ledger_items = LedgerItem.where(xml_upload_id: files_ids)
      ledger_items.order("created_at DESC").paginate(page: params[:page], per_page: 6)
    end
  
    def get_ledger_info_table
      ledger_items_ids = @ledger_items.select(:id)
      ledger_infos = LedgerInfo.where(ledger_item_id: ledger_items_ids)
      ledger_infos
    end
  
    def get_ledger_line_table
       ledger_infos_ids = @ledger_infos.select(:id)
       ledger_lines = LedgerLine.where(ledger_info_id: ledger_infos_ids)
       ledger_lines
    end
end
