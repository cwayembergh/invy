class AdminPagesController < ApplicationController
  before_filter :authenticate_admin!
  before_filter :menu_stats, only: [:accountants, :basics, :corporates, :dashboard, :invoices, :beta_list]
  before_filter :graphs, only: [:dashboard]
  layout 'dashboard'
  
  def accountants
    @accountants = Accountant.all.paginate(page: params[:page], per_page: 10)
  end
  
  def beta_list
    @beta_access = Whitelist.new
    @beta_granted = Whitelist.all.paginate(page: params[:page], per_page: 10)
    logger.info @beta_granted
    @beta_granted_count = @beta_granted.count
  end
  
  def add_access
    @beta_access = Whitelist.new(whitelist_upload_params)
    @beta_access.email = params[:whitelist][:email]
    @beta_access.request_type = params[:whitelist][:request_type]
    if @beta_access.save
      flash[:notice] = "The new access request has been successfully added to the list."
      redirect_to beta_path
    else
      flash[:warning] = "An error happened while adding the access request to the list."
      redirect_to beta_path
    end
  end
  
  def basics
    @users = User.all.paginate(page: params[:page], per_page: 10)
  end
  
  def corporates
    @corporates = Corporate.all.paginate(page: params[:page], per_page: 10)
  end
  
  def dashboard
    @total_basics = User.all.count
    @total_corporates = Corporate.all.count
    @total_accountants = Accountant.all.count
    @total_sent_invoices = LedgerInfo.all.count
  end
  
  def invoices
    @invoices = LedgerInfo.all.paginate(page: params[:page], per_page: 10)
  end
  
  
  private
  def graphs  
    if Rails.env.development?
      @this_year_basics = User.where("strftime('%Y', created_at) + 0 = ?", Date.current.year)
      @this_year_corporates = Corporate.where("strftime('%Y', created_at) + 0 = ?", Date.current.year)
      @this_year_accountants = Accountant.where("strftime('%Y', created_at) + 0 = ?", Date.current.year)
      @this_year_exchanged_invoices = LedgerInfo.where("strftime('%Y', created_at) + 0 = ?", Date.current.year)
    else
      @this_year_basics = User.where("extract(year from created_at)=?", Date.current.year)
      @this_year_corporates = Corporate.where("extract(year from created_at)=?", Date.current.year)
      @this_year_accountants = Accountant.where("extract(year from created_at)=?", Date.current.year)
      @this_year_exchanged_invoices = LedgerInfo.where("extract(year from created_at)=?", Date.current.year)
    end
  end
  
  
  def menu_stats
    @total_basics = User.all.count
    @total_corporates = Corporate.all.count
    @total_accountants = Accountant.all.count
    @total_sent_invoices = LedgerInfo.all.count
  end
  
  
  def whitelist_upload_params
    params.require(:whitelist).permit(:email, :request_type) if params[:whitelist]
  end
end