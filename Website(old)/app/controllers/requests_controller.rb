class RequestsController < ApplicationController
  layout 'pages'
  
  def beta_request
  end
  
  def create_request 
    email = params[:email].downcase
    request_type = params[:request_type]
    type = ""
    case request_type
      when "1"
        type = "classic"
      when "2"
        type = "corporate"
      when "3"
        type = "accountant"
    end
    
    
    if email.empty?
      flash[:warning] = "Please enter an email address."
      redirect_to beta_request_path
    
    elsif !is_a_valid_email?(email)
      flash[:warning] = "Please enter a valid email address."
      redirect_to beta_request_path
    
    elsif !email_already_used?(email)
      flash[:warning] = "Email address already exists.<br>Check your inbox for confirmation email."
      redirect_to beta_request_path
    
    else
      BetaMailer.access_request(email, type).deliver_now
      flash[:notice] = "Your access request has been sent!<br>You'll receive an email when it is accepted."
      redirect_to beta_request_path
    end
  end
  
  private
  def is_a_valid_email?(email)
   if email =~/\A[^@\s]+@([^@.\s]+\.)+[^@.\s]+\z/ then
      return true
    else 
      return false
    end
  end
  
  def email_already_used?(email)
    if Whitelist.where(email: email).count != 0
      return false
    else 
      return true
    end
  end
  
end
