class AccountantPagesController < ApplicationController
  before_filter :authenticate_accountant!
  before_filter :graphs, only: [:dashboard]
  layout 'dashboard'
  
  def dashboard
    @list_customers = Corporate.where(accountant_id: current_accountant.id)
    @number_of_customers = @list_customers.count
    @list_invoices = LedgerInfo.where(sender: @list_customers.map(&:email))
    @list_received_invoices = @list_invoices.where(sent_to_accountant: true)
    @total_invoices = @list_invoices.count
    @total_invoices_received = @list_received_invoices.count
    
    @progress = percent_of(@total_invoices, @total_invoices_received)
  end

  
  def show
    @customer = Corporate.find(params[:id])
    @invoices_sent_from_customer = LedgerInfo.where(sender: @customer.email).order("created_at DESC")
    @invoices_received_from_customer = LedgerInfo.where(recipient: @customer.email).order("created_at DESC")
    @customer_invoices = LedgerInfo.where('sender = ? OR recipient = ?', @customer.email, @customer.email).order("created_at DESC").paginate(page: params[:page], per_page: 10)
  end
  
  def customers 
    @list_customers = Corporate.where(accountant_id: current_accountant.id).order("id").paginate(page: params[:page], per_page: 10)
    @number_of_customers = @list_customers.count
  end
  
  def invoices
    @list_received_invoices = LedgerInfo.where()
  end
  
  def booking 
    @invoice=LedgerInfo.find_by_token!(params[:id])
    @customer = Corporate.find(params[:user])
  end
  
  
  
  private 
  
  def graphs
    @list_customers = Corporate.where(accountant_id: current_accountant.id)
    @list_invoices = LedgerInfo.where(sender: @list_customers.map(&:email))
    @list_received_invoices = @list_invoices.where(sent_to_accountant: true)
    
    if Rails.env.development?
      @this_month_received_invoices = @list_received_invoices.where("strftime('%m', created_at)= ?", Date.today.strftime("%m"))
      @this_year_received_invoices = @list_received_invoices.where("strftime('%Y', created_at) + 0 = ?", Date.current.year)
    else
      @this_month_received_invoices = @list_received_invoices.where("extract(month from created_at)=?", Date.today.strftime("%m"))
      @this_year_received_invoices = @list_received_invoices.where("extract(year from created_at)=?", Date.current.year)
    end
  end
  
  def percent_of(total, paid)
    if total == 0 and paid == 0
      0
    else 
      paid.to_f / total.to_f * 100.0
    end
  end
end
