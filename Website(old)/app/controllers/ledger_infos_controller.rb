class LedgerInfosController < ApplicationController
  before_filter :invoices_stats, only: [:index, :show, :new, :new_invoices, :paid_invoices, :sent_payments, :sent_invoices, :received_payments, :create, :unpaid, :received_invoices]
  
  layout 'dashboard'
  
  def index
    @invoices_this_month = @invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).paginate(page: params[:page], per_page: 10)
    @all_invoices = @invoices.paginate(page: params[:page], per_page: 10)
  end
  
  def show
    @invoice=LedgerInfo.find_by_token!(params[:id])
    respond_to do |format|
      format.html
      format.pdf do
        pdf = InvoicePdf.new(@invoice)
        send_data pdf.render, filename: "invoice_#{@invoice.id}.pdf", type: "application/pdf", disposition: "inline"
      end
      format.xml
    end
  end
  
  def new
    @num_line = 1;
    @ledger_item = LedgerItem.new  
    @new_invoice = LedgerInfo.new
    @new_invoice.ledger_lines.build
  end
  
  def new_invoices 
    @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).order("created_at DESC").paginate(page: params[:page], per_page: 10)
    @invoices_new_to_pay_this_month_sum = @invoices_new_this_month.sum :total_with_taxes
    
    @invoices_new_paid_total = @invoices.where(ledger_status: "Paid")
    @invoices_new_paid_this_month_sum = @invoices_new_paid_total.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
  end
  
  def paid_invoices
    @invoices_paid_this_month = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).paginate(page: params[:page], per_page: 10).order("paid_date DESC")
    
    @remaining_invoices_to_pay_total = @invoices.where(ledger_status: "New")
    @remaining_invoices_to_pay_this_month = @remaining_invoices_to_pay_total.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @remaining_invoices_to_pay_this_month_count = @remaining_invoices_to_pay_this_month.count
    @remaining_invoices_to_pay_this_month_sum = @remaining_invoices_to_pay_this_month.sum :total_with_taxes
  end
  
  def received_invoices
    @invoices_received = @invoices.paginate(page: params[:page], per_page: 10)
  end
  
  def unpaid
    if user_signed_in?
      @invoices_new = @invoices.where(ledger_status: "New")
      @unpaid_invoices_last_month = @invoices_new.where(created_at: 1.month.ago.beginning_of_month..1.month.ago.end_of_month).paginate(page: params[:page], per_page: 10)
       
    elsif corporate_signed_in?
      @unpaid_invoices = @sent_invoices.where(ledger_status: "New")
      @unpaid_invoices_last_month = @unpaid_invoices.where(created_at: 1.month.ago.beginning_of_month..1.month.ago.end_of_month).paginate(page: params[:page], per_page: 10)
    end
  end
  
  def create
    @ledger_item = LedgerItem.new
    @ledger_item.save
    @new_invoice = LedgerInfo.new(ledger_info_params)
    
    @new_invoice.ledger_item_id = @ledger_item.id
    @new_invoice.sender = current_corporate.email
    @new_invoice.issue_date = Time.now
    @new_invoice.ledger_status = "New"
    @new_invoice.token = SecureRandom.urlsafe_base64(16)
    
    
    respond_to do |format|
      if @new_invoice.save
        format.html { redirect_to create_path, notice: 'The invoice was successfully created.' }
        format.json { render :show, status: :created, location: create_path }
      else
        format.html { render :new }
        format.json { render json: @new_invoice.errors, status: :unprocessable_entity }
      end
    end
  end
  
  private
    def ledger_info_params
      params.require(:ledger_info).permit(:ledger_item_id, :recipient, :ledger_info_type, :due_date, :ledger_id, :currency, :total_with_taxes, :total_without_taxes, :total_tax, :token, :paid_date, :payment_method, :sent_to_accountant, :notes,
                                          ledger_lines_attributes: [:description, :product, :quantity, :total_price, :total_price_with_taxes, :unit_price, :tax_rate, :_destroy])
    end
    
    def invoices_stats
      if user_signed_in?
        @invoices = LedgerInfo.where(recipient: current_user.email)
        @invoices_this_month = @invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).order("created_at DESC")
        @invoices_this_month_count = @invoices_this_month.count
        @total_expenses_this_month = @invoices_this_month.sum :total_with_taxes
        
        @invoices_new = @invoices.where(ledger_status: "New")
        @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).order("created_at DESC")
        @invoices_new_this_month_count = @invoices_new_this_month.count
        
        @invoices_paid = @invoices.where(ledger_status: "Paid")
        @invoices_paid_this_month_count = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
        @invoices_paid_this_month_sum = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
      
        
      elsif corporate_signed_in?
        @invoices = LedgerInfo.where(recipient: current_corporate.email)
        @invoices_this_month = @invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).order("created_at DESC")
        @invoices_this_month_count = @invoices_this_month.count
        @total_expenses_this_month = @invoices_this_month.sum :total_with_taxes
        
        @invoices_new = @invoices.where(ledger_status: "New").order("created_at DESC")
        @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
        @invoices_new_this_month_count = @invoices_new_this_month.count
        
        @invoices_paid = @invoices.where(ledger_status: "Paid")
        @invoices_paid_this_month_count = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
        @invoices_paid_this_month_sum = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
        
        @sent_invoices = LedgerInfo.where(sender: current_corporate.email).order("created_at DESC")  
        @sent_invoices_this_month = @sent_invoices.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
        @sent_invoices_this_month_count = @sent_invoices_this_month.count
        @sent_invoices_this_month_sum = @sent_invoices_this_month.sum :total_with_taxes     
        
        @sent_invoices_paid = @sent_invoices.where(ledger_status: "Paid")
        @sent_invoices_paid_this_month = @sent_invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
        @sent_invoices_paid_this_month_count = @sent_invoices_paid_this_month.count
        @sent_invoices_paid_this_month_sum = @sent_invoices_paid_this_month.sum :total_with_taxes 
        
        @progress_sent_invoices = percent_of(@sent_invoices_this_month_count, @sent_invoices_paid_this_month_count)
        
      elsif accountant_signed_in?
        @list_customers = Corporate.where(accountant_id: current_accountant.id)
        @number_of_customer = @list_customers.count
        @invoices = LedgerInfo.where(sender: @list_customers.take.email).order("created_at DESC")
      end
    end
    
    def percent_of(total, paid)
      paid.to_f / total.to_f * 100.0
    end
end
