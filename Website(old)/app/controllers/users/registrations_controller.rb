class Users::RegistrationsController < Devise::RegistrationsController
  protect_from_forgery with: :exception
  before_filter :configure_sign_up_params, only: [:create]
  before_filter :configure_account_update_params, only: [:update]
  before_filter :invoice_stats, only: [:edit, :update]
  
  layout :resolve_layout
  

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    if Whitelist.exists?(:email => params[:user][:email])
      if Whitelist.where(:email => params[:user][:email]).take.request_type != "classic"
        flash[:warning] = "Unfortunately you cannot create a classic account.<br>You have access to a <strong>#{Whitelist.where(:email => params[:user][:email]).take.request_type}</strong> account."
        redirect_to new_user_registration_path
      else
        super
      end
    else
      flash[:warning] = "Your email is not registered in our beta list."
      redirect_to new_user_registration_path
    end
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  def cancel
    super
  end

  protected

  # You can put the params you want to permit in the empty array.
  def configure_sign_up_params
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:first_name, :last_name, :is_female, :date_of_birth, :avatar, :email, :password, :password_confirmation, :address, :postal_code, :city, :telephone, :bank_account, :address_number, :terms_and_conditions)}
  end

  # You can put the params you want to permit in the empty array.
  def configure_account_update_params
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:first_name, :last_name, :email, :is_female, :date_of_birth, :avatar,  :current_password, :password, :password_confirmation, :address, :postal_code, :city, :telephone, :bank_account, :address_number)}
  end

  # The path used after sign up.
  def after_sign_up_path_for(resource)
    super(resource)
  end

  # The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    super(resource)
  end
  
  def after_update_path_for(resource)
    edit_user_registration_path(resource)
  end
  
  
  #INVOICE
  def invoice_stats
    @invoices = LedgerInfo.where(recipient: current_user.email).order("created_at DESC")
    
    @invoices_new = @invoices.where(ledger_status: "New")
    @invoices_new_this_month = @invoices_new.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month)
    @invoices_new_this_month_count = @invoices_new_this_month.count
    
    @invoices_paid = @invoices.where(ledger_status: "Paid")
    @invoices_paid_this_month_count = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).count
    @invoices_paid_this_month_sum = @invoices_paid.where(created_at: Time.now.beginning_of_month..Time.now.end_of_month).sum :total_with_taxes
  end
  
  def resolve_layout
    case action_name
    when "new", "create"
      'pages'
    when "edit", "update"
      'dashboard'
    end
  end
end
