class SuggestionsController < ApplicationController
  before_filter :authenticate_corporate!
  
  def index
    @corporates = Corporate.order(:email).where("email like ?", "%#{params[:term]}%")
    @users = User.order(:email).where("email like ?", "%#{params[:term]}%")
    @suggestions = @users + @corporates
    render json: @suggestions.map{|t| {:label => t.email, :value => t.id, :first_name => t.first_name, :last_name => t.last_name, :address => t.address, :number => t.address_number, :postal_code => t.postal_code, :city => t.city, :bank_account => t.bank_account, :phone => t.telephone}}
  end
end
