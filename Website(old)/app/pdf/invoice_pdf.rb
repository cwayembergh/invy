class InvoicePdf < Prawn::Document
  
  def initialize(invoice)
    super(top_margin: 70)
    @invoice = invoice
    @ledger_lines = LedgerLine.where(ledger_info_id: @invoice.id)
    
    if @invoice.currency == "EUR"
      @currency = "€"
    elsif @invoice.currency == "USD"
      @currency = "$"
    end
    
    font_families.update( "OpenSans" => { :normal => "#{Rails.root}/app/assets/fonts/OpenSans-Regular.ttf", :bold => "#{Rails.root}/app/assets/fonts/OpenSans-Bold.ttf"})
    font "OpenSans"
    default_leading 3
    
    if Corporate.where(email: @invoice.recipient).any?
      @customer_name = Corporate.where(email: @invoice.recipient).take.business_name
      @customer_address = Corporate.where(email: @invoice.recipient).take.address
      @customer_number = Corporate.where(email: @invoice.recipient).take.address_number
      @customer_city = Corporate.where(email: @invoice.recipient).take.city
      @customer_postal_code = Corporate.where(email: @invoice.recipient).take.postal_code
      @customer_phone = Corporate.where(email: @invoice.recipient).take.telephone
      @customer_email = Corporate.where(email: @invoice.recipient).take.email
      @customer_bank_account = Corporate.where(email: @invoice.recipient).take.bank_account
    else
      @customer_name = User.where(email: @invoice.recipient).take.first_name+" "+User.where(email: @invoice.recipient).take.last_name
      @customer_address = User.where(email: @invoice.recipient).take.address
      @customer_number = User.where(email: @invoice.recipient).take.address_number
      @customer_city = User.where(email: @invoice.recipient).take.city
      @customer_postal_code = User.where(email: @invoice.recipient).take.postal_code
      @customer_phone = User.where(email: @invoice.recipient).take.telephone
      @customer_email = User.where(email: @invoice.recipient).take.email
      @customer_bank_account = User.where(email: @invoice.recipient).take.bank_account
    end
    
    
    header
    ledger_line
    notes_and_total_price
    repeat :all do
      footer
    end
  end
 
  
  def header
    y_position = cursor
    bounding_box([0, y_position], :width => 180) do
      if !(Corporate.where(email: @invoice.sender).first).avatar.exists?
        text "#{Corporate.where(email: @invoice.sender).take.business_name}", size: 14, style: :bold
      else
        image "#{(Corporate.where(email: @invoice.sender).first).avatar.path(:medium)}" , :height => 30
      end
      move_down 5
      text "#{Corporate.where(email: @invoice.sender).take.address}, #{Corporate.where(email: @invoice.sender).take.address_number}", size: 8
      text "#{Corporate.where(email: @invoice.sender).take.city}, #{Corporate.where(email: @invoice.sender).take.postal_code}", size: 8
      text "TVA: #{Corporate.where(email: @invoice.sender).take.vat_number}", size: 8
      text "Phone: #{Corporate.where(email: @invoice.sender).take.telephone }", size: 8
      text "Email: #{Corporate.where(email: @invoice.sender).take.email }", size: 8

    end
    
    bounding_box([300, y_position], :width => 250) do
      text_box "#{(@invoice.ledger_info_type).upcase}", size: 14, style: :bold, :at => [102,0]
      move_down 15
      if @invoice.ledger_status == "Paid"
        data = [["Bill to:", "#{@customer_name}"],
                ["", "#{@customer_address}, #{@customer_number}"],
                ["", "#{@customer_city}, #{@customer_postal_code}"],
                ["", "#{@customer_phone}"],
                ["", "#{@customer_email}"],
                ["Issue Date:", "#{@invoice.issue_date.strftime("%d/%m/%Y")}"],
                ["Payment Date:", "#{@invoice.paid_date.strftime("%d/%m/%Y")}"],
                ["Payment Method:", "#{@invoice.payment_method}"],
                ["Invoice #:", "#{@invoice.id}"],
                ["Ref Number:", "#{@invoice.ledger_id}"],
                ["Payment Due Date:", "#{@invoice.due_date.strftime("%d/%m/%Y")}"],
                ["Currency:", "#{@invoice.currency}"],
                ["Account Number:", "#{@customer_bank_account}"]]
      else
        data = [["Bill to:", "#{@customer_name}"],
                ["", "#{@customer_address}, #{@customer_number}"],
                ["", "#{@customer_city}, #{@customer_postal_code}"],
                ["", "#{@customer_phone}"],
                ["", "#{@customer_email}"],
                ["Issue Date:", "#{@invoice.issue_date.strftime("%d/%m/%Y")}"],
                ["Invoice #:", "#{@invoice.id}"],
                ["Ref Number:", "#{@invoice.ledger_id}"],
                ["Payment Due Date:", "#{@invoice.due_date.strftime("%d/%m/%Y")}"],
                ["Currency:", "#{@invoice.currency}"],
                ["Account Number:", "#{@customer_bank_account}"]]
      end
      table(data, :column_widths => [100, 150], :cell_style => {:borders => [], :size => 8, :padding => [3,0,0,3]}) do
        column(0).font_style = :bold
      end
      move_down 10
    end
  end
  
  def ledger_line
    move_down 20
    table ledger_line_rows, :column_widths => [135, 135, 50, 50, 50, 50, 70], :cell_style => {:size => 8, :border_color => "aaaaaa", :border_width => 0.1} do
      row(0).font_style = :bold
      row(0).background_color = 'f5f5f5'
      column(2).align = :center
      column(3).align = :center
      column(4).align = :center
      column(5..6).align = :center
      row(0).column(2..6).align = :center
      self.header = true
    end
  end
  
  def ledger_line_rows
    [["Product", "Description", "Quantity", "Unit Price", "VAT Rate", "Total", "Total VAT incl."]]+
    @ledger_lines.map do |line|
      [line.product, line.description, "%g" % ("%.2f" % line.quantity), @currency+"%g" % ("%.2f" % line.unit_price), "%g" % ("%.2f" % line.tax_rate)+"%", @currency + "%g" % ("%.2f" % line.total_price), @currency + "%g" % ("%.2f" % line.total_price_with_taxes)]
    end
  end
  
  def notes_and_total_price
    move_down 15
    y_position = cursor
    bounding_box([0, y_position], :width =>320, :height => 100) do
      stroke_bounds
      bounding_box([5, cursor - 5], :width => 310) do
        text "Special Notes and Instructions:", size: 9, style: :bold
        move_down 10
        text "#{@invoice.notes}", size: 8
      end
      
     
    end
    bounding_box([370, y_position], :width => 180) do
      data = [["Total (VAT excl.):", @currency+"#{@invoice.total_without_taxes}"],
              ["VAT:", @currency+"#{@invoice.total_tax}"],
              ["Total (VAT incl.):", @currency+"#{@invoice.total_with_taxes}"]]
      table(data, :column_widths => [100, 70], :cell_style => {:size => 8}) do
        column(1).align = :right
        row(2).background_color = 'd9edf7'
        row(2).column(1).font_style = :bold
      end
    end
  end
  
  def footer
    bounding_box [bounds.left, bounds.bottom + 50], width: bounds.width do
      text "Thank you for using Invy!", size: 8, align: :center, style: :bold
      text "Should you have any enquiries concerning this invoice, please contact John Doe at www.invy.be", size: 8, align: :center
      stroke do
        horizontal_rule
        move_down 5
      end
      text "Invy - 119 Boulevard Général Jacques, 1050, Ixelles", size: 8, align: :center
      text "Tel: 02651687 - Fax: 02651786 - Email: info@invy.be - Web: www.invy.be", size: 8, align: :center
    end
  end
  
end