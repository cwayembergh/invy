class LedgerItem < ActiveRecord::Base
  belongs_to :xml_upload
  has_many :ledger_infos, :dependent => :destroy
  has_many :ledger_lines, :through => :ledger_infos
end
