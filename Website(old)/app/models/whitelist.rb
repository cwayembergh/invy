class Whitelist < ActiveRecord::Base
  
  validates :email, :request_type, presence: true
end
