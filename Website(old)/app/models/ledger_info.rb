class LedgerInfo < ActiveRecord::Base
  belongs_to :ledger_item
  has_many :ledger_lines, :dependent => :destroy
  
  attr_accessor :recipient_name, :recipient_address, :recipient_city, :recipient_bank_account, :recipient_phone
  attr_accessible :sender, :recipient, :ledger_info_type, :issue_date, :ledger_id, :currency, :total_with_taxes, :total_without_taxes, :total_tax, :status, :due_date, :period_start, :period_end, :ledger_lines_attributes, :token, :paid_date, :payment_method, :sent_to_accountant, :notes
  accepts_nested_attributes_for :ledger_lines, :allow_destroy => true
  
  validates :recipient, :ledger_info_type, :ledger_id, :currency, :due_date, presence: true
  validates_format_of :recipient,:with => /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
  before_save :format_email
  
  def to_param
    token
  end
  
  private 
  def format_email
    self.recipient = self.recipient.downcase
  end
end
