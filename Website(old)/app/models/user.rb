class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  
  #TODO terms and condition validation (field_with_error is messing up css)
  attr_accessor :terms_and_conditions

  validates :first_name, :last_name, :bank_account, :telephone, :address, :address_number, :postal_code, :city, presence: true

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable
         
  has_attached_file :avatar, styles: {medium: "300x300>", thumb: "100x100"}, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/

  before_save :format_email
         
  def send_devise_notification(notification, *args)
    if Rails.env.production?
      # No worker process in production to handle scheduled tasks
      devise_mailer.send(notification, self, *args).deliver_now
    else
      #devise_mailer.send(notification, self, *args).deliver_later
      devise_mailer.send(notification, self, *args).deliver_now
    end
  end
  
  def has_payment_info?
    braintree_customer_id
  end
  
  private
  def format_email
    self.email = email.downcase
  end
end
