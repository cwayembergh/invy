class XmlUpload < ActiveRecord::Base
  has_many :ledger_items
  belongs_to :corporate
  has_attached_file :xml  
  validates_attachment_presence :xml
  validates_attachment_content_type :xml, :content_type => ["text/xml"]
  before_save :parse_file
  
  
  def parse_file
    tempfile = xml.queued_for_write[:original]
    doc = Nokogiri::XML(tempfile)
    parse_xml(doc)
  end
  
  def parse_xml(doc)
    doc.root.elements.each do |node|
      parse_ledger(node)
    end
  end
  
  def parse_ledger(node) 
    if node.node_name.eql? 'ledger_item'
      tmp_ledger_item = LedgerItem.new
      node.elements.each do |node|
        parse_ledger_info(node, tmp_ledger_item)
      end
    end
    self.ledger_items << tmp_ledger_item
  end
  
  
  def parse_ledger_info(node, tmp_ledger_item)
    if node.node_name.eql? 'ledger_info'
      tmp_info = LedgerInfo.new
      tmp_info.token = SecureRandom.urlsafe_base64(16)
      tmp_info.issue_date = Time.now
      node.elements.each do |node|
        tmp_info.sender = corporate.email
        tmp_info.recipient = node.text.to_s if node.name.eql? 'recipient'
        tmp_info.ledger_info_type = node.text.to_s if node.name.eql? 'type'
        tmp_info.ledger_id = node.text.to_s if node.name.eql? 'ledger_id'
        tmp_info.currency = node.text.to_s if node.name.eql? 'currency'
        tmp_info.total_without_taxes = node.text.to_s if node.name.eql? 'total_without_taxes'
        tmp_info.total_tax = node.text.to_s if node.name.eql? 'total_tax'
        tmp_info.total_with_taxes = node.text.to_s if node.name.eql? 'total_with_taxes'
        tmp_info.due_date = node.text.to_s if node.name.eql? 'due_date'
        tmp_info.ledger_status = "New"
        
        if node.node_name.eql? 'ledger_line'
          parse_line(node, tmp_info)
        end
      end
      tmp_ledger_item.ledger_infos << tmp_info
    end
  end
  
  def parse_line(node, tmp_info)
    tmp_line = LedgerLine.new
    node.elements.each do |node|
      tmp_line.description = node.text.to_s if node.name.eql? 'description'
      tmp_line.product = node.text.to_s if node.name.eql? 'product'
      tmp_line.quantity = node.text.to_s if node.name.eql? 'quantity'
      tmp_line.unit_price = node.text.to_s if node.name.eql? 'unit_price'
      tmp_line.tax_rate = node.text.to_s if node.name.eql? 'tax_rate'
      tmp_line.total_price = node.text.to_s if node.name.eql? 'total_price'
      tmp_line.total_price_with_taxes = node.text.to_s if node.name.eql? 'total_price_with_taxes'
    end
    tmp_info.ledger_lines << tmp_line
  end
  
  
end
