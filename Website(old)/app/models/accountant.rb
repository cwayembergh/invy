class Accountant < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  
  #TODO terms and condition validation (field_with_error is messing up css)
  attr_accessor :terms_and_conditions
  
  validates :first_name, :last_name, :business_name, :bank_account, :telephone, :address, :address_number, :postal_code, :city, presence: true
  
  has_attached_file :avatar, styles: {medium: "300x300>", thumb: "100x100"}, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\Z/
  
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable, :lockable
         
  before_save :format_email
  
  private
  def format_email
    self.email = email.downcase
  end
end
