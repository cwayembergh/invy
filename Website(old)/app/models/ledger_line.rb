class LedgerLine < ActiveRecord::Base
  belongs_to :ledger_info
  attr_accessible :total_price, :total_price_with_taxes, :product, :description, :quantity, :tax_rate, :unit_price
  
  validates :product, :description, :quantity, :unit_price, :tax_rate, presence: true
  validates :quantity, numericality: {greater_than_or_equal_to: 1, only_integer: true}
  validates :unit_price, numericality: {greater_than_or_equal_to: 1}
end
