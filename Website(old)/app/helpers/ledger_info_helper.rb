module LedgerInfoHelper
  
  def link_to_remove_fields() 
    link_to("<i class=\"fa fa-minus-circle\"></i> ".html_safe, "",  :class => "link_to_remove_fields")
  end
  
  def link_to_add_lines(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |line|
      render 'ledger_infos/invoice_lines', :f => line
    end
    link_to("<i class=\"fa fa-plus-circle\"></i> ".html_safe+name, "",  :class => "btn btn-default link_to_add_lines", "data-association" => "#{association}", "data-content" => "#{fields}")
  end
end
