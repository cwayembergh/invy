# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
jQuery ->
	$("#ledger_info_recipient").autocomplete
		source: $('#ledger_info_recipient').data('autocomplete-source')
		minLength: 2
		focus: (event, ui) ->
			@value = ui.item.label
			event.preventDefault()
			return
			
		response: (event, ui) ->
			if ui.content.length == 0
				$('#recipient_name').val("Unregistered recipient.")
				$('#recipient_address').val("An email with the invoice in PDF")
				$('#recipient_city').val("format and a sign up link will be")
				$('#recipient_bank_account').val("sent to the recipient.")
			else
				$('#recipient_name').empty()
				$('#recipient_address').empty
				$('#recipient_city').empty
				$('#recipient_bank_account').empty
			return
			return
			
		select: (event, ui) ->
			event.preventDefault()
			@value = ui.item.label
			$('#recipient_name').val(ui.item.first_name+' '+ui.item.last_name)
			$('#recipient_address').val(ui.item.address+', '+ui.item.number)
			$('#recipient_city').val(ui.item.city+', '+ui.item.postal_code)
			$('#recipient_bank_account').val(ui.item.bank_account)
			$('#recipient_phone').val(ui.item.phone)
			return
	return