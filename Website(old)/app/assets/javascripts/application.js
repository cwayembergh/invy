// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require jquery-ui/autocomplete
//= require turbolinks
//= require bootstrap/alert
//= require bootstrap/collapse
//= require bootstrap/dropdown
//= require bootstrap/modal
//= require bootstrap/tab
//= require bootstrap/transition
//= require_tree .
	var id = 0;
	
	
	/* FUNCTION TO ADD A NEW INVOICE LINE */
	function add_lines(link, association, content) {
		id += 1;
		var regexp = new RegExp("new_" + association, "g");
		var new_content = content.replace(regexp, id);
		$(".invoice-lines").append(new_content);
	}
	
	$(document).ready(function() {
		
		/* FUNCTION TO ADD LINE ON CLICK */
		$("a.link_to_add_lines").on("click", function(e) {
			e.preventDefault();
			var link = $(this);
			var association = $(this).data("association");
			var content = $(this).data("content");
			add_lines(link, association, content);
		});
		
		/* FUNCTION TO REMOVE A LINE */
		$(document).on("click", "a.link_to_remove_fields", function(e) {
			e.preventDefault();
			
			//Get current values
			var total_price_line_value = $(this).closest(".invoice-line-fields").find(".total_price").val();
			var total_with_taxes_line_value = $(this).closest(".invoice-line-fields").find(".total_with_taxes").val();
			var total_without_vat_value = $("#total_without_vat").val();
			var total_with_vat_value = $("#total_with_vat").val();
			
			//Compute new values
			var new_total_without_vat = parseFloat(Math.round((total_without_vat_value - total_price_line_value)*100)/100);
			var new_total_with_vat = parseFloat(Math.round((total_with_vat_value - total_with_taxes_line_value)*100)/100);
			var new_total_vat = parseFloat(Math.round((new_total_with_vat - new_total_without_vat)*100)/100);
			
			//Set values to 0 for hidden line
			$(this).closest(".invoice-line-fields").find(".total_price").val(0);
			$(this).closest(".invoice-line-fields").find(".total_with_taxes").val(0);
			
			//Set bottom values
			$("#total_without_vat").val(new_total_without_vat);
			$("#total_vat").val(new_total_vat);
			$("#total_with_vat").val(new_total_with_vat);
			
			//Hide line
			$(this).prev().val(true);
			$(this).closest(".invoice-line-fields").hide();
		});

		/* UPDATE THE PRICES IN THE INVOICE GENERATOR FORM */
		$(document).on('keyup', ".form-invoice", function() {
			var total_without_vat = 0;
			var total_vat = 0;
			var total_with_vat = 0;
			
			var quantity = $(this).closest('.invoice-line-fields').find('#quantity').val() || 0;
			var unit_price = parseFloat($(this).closest('.invoice-line-fields').find('#unit_price').val()) || 0;
			var tax_rate = $(this).closest('.invoice-line-fields').find('#tax_rate').val() || 0;
		
			var total_price = parseFloat(quantity*unit_price) || 0;
			var total_tax = parseFloat(Math.round((total_price * tax_rate))/100) || 0;
			var total_price_with_taxes = parseFloat(Math.round((total_price + total_tax)*100)/100) || 0;
		
			$(this).closest('.invoice-line-fields').find('.total_price').val(total_price);
			$(this).closest('.invoice-line-fields').find('.total_with_taxes').val(total_price_with_taxes);
			
			// ADD EACH TOTAL_PRICE IF SEVERAL LINES
			$('.total_price').each(function() {
				var value = parseFloat($(this).val());
				total_without_vat = parseFloat(Math.round((total_without_vat + value)*100)/100) || 0;
			});
			
			// ADD EACH TOTAL WITH TAXES IF SEVERAL LINES
			$('.total_with_taxes').each(function() {
				var value = parseFloat($(this).val());
				total_with_vat = parseFloat(Math.round((total_with_vat + value)*100)/100) || 0;
			});
			
			// COMPUTE TOTAL VAT
			total_vat = parseFloat(Math.round((total_with_vat - total_without_vat)*100)/100) || 0;
			
			$("#total_without_vat").val(total_without_vat);
			$("#total_vat").val(total_vat);
			$("#total_with_vat").val(total_with_vat);
		});
		
	
		
		/* SHOW THE CURRENCY VALUE */
		if($("#ledger_info_currency").val() == "EUR") {
			var currency = "€";
		}
		else {
			var currency = "$";
		}
		$(".currency-value").empty();
		$(".currency-value").append(currency);
	
		/* UPDATE THE CURRENCY VALUE */
		$("#ledger_info_currency").on('change', function() {
			if($("#ledger_info_currency").val() == "EUR") {
				var currency = "€";
			}
			else {
				var currency = "$";
			}
			$(".currency-value").empty();
			$(".currency-value").append(currency);
		});
	});