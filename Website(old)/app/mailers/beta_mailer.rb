class BetaMailer < ApplicationMailer
  default to: "clement.wayembergh@gmail.com"
  
  def access_request(email, type)
    @email = email
    @type = type
    mail(from: "no-reply@invy.be", subject: '[Beta access request]')
  end
end
