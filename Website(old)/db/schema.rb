# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160422090214) do

  create_table "accountants", force: :cascade do |t|
    t.string   "first_name"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "address"
    t.string   "postal_code"
    t.string   "city"
    t.string   "telephone"
    t.string   "bank_account"
    t.string   "last_name"
    t.string   "address_number"
    t.string   "business_name"
  end

  add_index "accountants", ["confirmation_token"], name: "index_accountants_on_confirmation_token", unique: true
  add_index "accountants", ["email"], name: "index_accountants_on_email", unique: true
  add_index "accountants", ["reset_password_token"], name: "index_accountants_on_reset_password_token", unique: true
  add_index "accountants", ["unlock_token"], name: "index_accountants_on_unlock_token", unique: true

  create_table "admins", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["confirmation_token"], name: "index_admins_on_confirmation_token", unique: true
  add_index "admins", ["email"], name: "index_admins_on_email", unique: true
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true
  add_index "admins", ["unlock_token"], name: "index_admins_on_unlock_token", unique: true

  create_table "corporates", force: :cascade do |t|
    t.string   "business_name"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "vat_number"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "address"
    t.string   "postal_code"
    t.string   "city"
    t.string   "telephone"
    t.string   "bank_account"
    t.string   "braintree_customer_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "address_number"
    t.integer  "accountant_id",          default: 0
  end

  add_index "corporates", ["accountant_id"], name: "index_corporates_on_accountant_id"
  add_index "corporates", ["confirmation_token"], name: "index_corporates_on_confirmation_token", unique: true
  add_index "corporates", ["email"], name: "index_corporates_on_email", unique: true
  add_index "corporates", ["reset_password_token"], name: "index_corporates_on_reset_password_token", unique: true
  add_index "corporates", ["unlock_token"], name: "index_corporates_on_unlock_token", unique: true

  create_table "ledger_infos", force: :cascade do |t|
    t.integer  "ledger_item_id"
    t.string   "sender"
    t.string   "recipient"
    t.string   "ledger_info_type"
    t.datetime "issue_date"
    t.string   "ledger_id"
    t.string   "currency"
    t.decimal  "total_with_taxes"
    t.decimal  "total_without_taxes"
    t.datetime "due_date"
    t.datetime "period_start"
    t.datetime "period_end"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "ledger_status"
    t.string   "token"
    t.decimal  "total_tax"
    t.datetime "paid_date"
    t.string   "payment_method"
    t.boolean  "sent_to_accountant",  default: false
    t.string   "notes"
  end

  add_index "ledger_infos", ["ledger_item_id"], name: "index_ledger_infos_on_ledger_item_id"
  add_index "ledger_infos", ["token"], name: "index_ledger_infos_on_token"

  create_table "ledger_items", force: :cascade do |t|
    t.integer  "xml_upload_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "ledger_items", ["xml_upload_id"], name: "index_ledger_items_on_xml_upload_id"

  create_table "ledger_lines", force: :cascade do |t|
    t.integer  "ledger_info_id"
    t.decimal  "total_price"
    t.decimal  "total_price_with_taxes"
    t.string   "description"
    t.decimal  "quantity"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "product"
    t.decimal  "unit_price"
    t.decimal  "tax_rate"
  end

  add_index "ledger_lines", ["ledger_info_id"], name: "index_ledger_lines_on_ledger_info_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_female"
    t.date     "date_of_birth"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "address"
    t.string   "postal_code"
    t.string   "city"
    t.string   "telephone"
    t.string   "bank_account"
    t.string   "braintree_customer_id"
    t.string   "address_number"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true

  create_table "whitelists", force: :cascade do |t|
    t.string   "email",        null: false
    t.string   "request_type", null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "xml_uploads", force: :cascade do |t|
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "xml_file_name"
    t.string   "xml_content_type"
    t.integer  "xml_file_size"
    t.datetime "xml_updated_at"
    t.integer  "corporate_id"
  end

  add_index "xml_uploads", ["corporate_id"], name: "index_xml_uploads_on_corporate_id"

end
