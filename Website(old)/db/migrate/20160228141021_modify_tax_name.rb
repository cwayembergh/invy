class ModifyTaxName < ActiveRecord::Migration
  def change
    rename_column :ledger_infos, :total_without_taxe, :total_without_taxes
  end
end
