class AddingProductsToLedgerlines < ActiveRecord::Migration
  def change
    add_column :ledger_lines, :product, :string
    remove_column :ledger_infos, :status
  end
end
