class CreateLedgerItems < ActiveRecord::Migration
  def change
    create_table :ledger_items do |t|
      t.references :xml_upload, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
