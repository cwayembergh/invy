class AddAddressNumber < ActiveRecord::Migration
  def change
    add_column :users, :address_number, :string
    add_column :corporates, :address_number, :string
    add_column :accountants, :address_number, :string
  end
end
