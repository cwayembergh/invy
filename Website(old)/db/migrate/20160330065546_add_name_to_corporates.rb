class AddNameToCorporates < ActiveRecord::Migration
  def change
    add_column :corporates, :first_name, :string
    add_column :corporates, :last_name, :string
    add_column :accountants, :last_name, :string
  end
end
