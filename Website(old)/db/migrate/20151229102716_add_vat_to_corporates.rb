class AddVatToCorporates < ActiveRecord::Migration
  def change
    add_column :corporates, :vat_number, :string
  end
end
