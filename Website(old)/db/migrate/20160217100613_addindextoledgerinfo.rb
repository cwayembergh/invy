class Addindextoledgerinfo < ActiveRecord::Migration
  def change
    add_index :ledger_infos, :token
  end
end
