class AddPaidDate < ActiveRecord::Migration
  def change
    add_column :ledger_infos, :paid_date, :datetime
    add_column :ledger_infos, :payment_method, :string
  end
end
