class CreateLedgerInfos < ActiveRecord::Migration
  def change
    create_table :ledger_infos do |t|
      t.references :ledger_item, index: true, foreign_key: true
      t.string :sender
      t.string :recipient
      t.string :ledger_info_type
      t.datetime :issue_date
      t.string :ledger_id
      t.string :currency
      t.decimal :total_amount
      t.decimal :tax_amount
      t.string :status
      t.datetime :due_date
      t.string :uuid
      t.datetime :period_start
      t.datetime :period_end

      t.timestamps null: false
    end
  end
end
