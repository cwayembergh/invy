class AddSendToLedgerInfo < ActiveRecord::Migration
  def change
    add_column :ledger_infos, :sent_to_accountant, :string
  end
end
