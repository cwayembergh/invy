class CreateXmlUploads < ActiveRecord::Migration
  def change
    create_table :xml_uploads do |t|
      t.timestamps null: false
    end
    add_attachment :xml_uploads, :xml
  end
end
