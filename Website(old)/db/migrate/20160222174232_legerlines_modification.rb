class LegerlinesModification < ActiveRecord::Migration
  def change
    add_column :ledger_lines, :unit_price, :decimal
    add_column :ledger_lines, :tax_rate, :decimal
    rename_column :ledger_lines, :net_amount, :total_price
    rename_column :ledger_lines, :tax_amount, :total_price_with_taxes
    remove_column :ledger_lines, :uuid
    remove_column :ledger_lines, :tax_point
    remove_column :ledger_lines, :creator_id
    remove_column :ledger_infos, :uuid
  end
end
