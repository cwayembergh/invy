class AddAccountantsToLedgerinfo < ActiveRecord::Migration
  def change
    add_column :ledger_infos, :accountant_id, :string
    add_index :ledger_infos, :accountant_id
  end
end
