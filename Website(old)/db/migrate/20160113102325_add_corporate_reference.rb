class AddCorporateReference < ActiveRecord::Migration
  def change
    add_reference :xml_uploads, :corporate, index: true
    add_foreign_key :xml_uploads, :corporates
  end
end
