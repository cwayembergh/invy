class Addthings < ActiveRecord::Migration
  def change
    add_column :ledger_infos, :token, :string
    add_column :users, :address, :string
    add_column :users, :postal_code, :string
    add_column :users, :city, :string
    add_column :corporates, :address, :string
    add_column :corporates, :postal_code, :string
    add_column :corporates, :city, :string
    add_column :accountants, :address, :string
    add_column :accountants, :postal_code, :string
    add_column :accountants, :city, :string
  end
end
