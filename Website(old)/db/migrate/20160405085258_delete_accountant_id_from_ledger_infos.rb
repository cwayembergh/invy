class DeleteAccountantIdFromLedgerInfos < ActiveRecord::Migration
  def change
    remove_column :ledger_infos, :accountant_id
  end
end
