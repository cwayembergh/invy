class AddAvatars < ActiveRecord::Migration
  def change
    add_attachment :corporates, :avatar
  end
  
  def down
    remove_attachement :accountants, :avatar
  end
end
