class AddStatusToLedgerItems < ActiveRecord::Migration
  def change
    add_column :ledger_infos, :ledger_status, :string
  end
end
