class ChangeSentToAccountant < ActiveRecord::Migration
  def change
    remove_column :ledger_infos, :sent_to_accountant
    add_column :ledger_infos, :sent_to_accountant, :boolean, :default => false
  end
end
