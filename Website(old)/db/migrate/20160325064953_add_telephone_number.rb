class AddTelephoneNumber < ActiveRecord::Migration
  def change
    add_column :users, :telephone, :string
    add_column :corporates, :telephone, :string
    add_column :accountants, :telephone, :string
    
    add_column :users, :bank_account, :string
    add_column :corporates, :bank_account, :string
    add_column :accountants, :bank_account, :string
  end
end
