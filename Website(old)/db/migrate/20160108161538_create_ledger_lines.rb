class CreateLedgerLines < ActiveRecord::Migration
  def change
    create_table :ledger_lines do |t|
      t.references :ledger_info, index: true, foreign_key: true
      t.decimal :net_amount
      t.decimal :tax_amount
      t.string :description
      t.string :uuid
      t.decimal :quantity
      t.datetime :tax_point
      t.integer :creator_id
      
      t.timestamps null: false
    end
  end
end
