class AddAvatarsToUsers < ActiveRecord::Migration
  def change
    add_attachment :users, :avatar
  end
  
  def down
    remove_attachement :users, :avatar
  end
end
