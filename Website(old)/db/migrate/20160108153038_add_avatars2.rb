class AddAvatars2 < ActiveRecord::Migration
  def change
    add_attachment :accountants, :avatar
  end
  
  def down
    remove_attachement :corporates, :avatar
  end
end
