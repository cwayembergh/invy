class AddCompanyToAccountatns < ActiveRecord::Migration
  def change
    add_column :accountants, :business_name, :string
    add_reference :corporates, :accountant, index: true
    add_foreign_key :corporates, :accountants
  end
end
