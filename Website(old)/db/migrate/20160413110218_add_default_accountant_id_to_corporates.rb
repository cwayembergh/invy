class AddDefaultAccountantIdToCorporates < ActiveRecord::Migration
  def change
    change_column_default :corporates, :accountant_id, 0
  end
end
