class CreateWhitelists < ActiveRecord::Migration
  def change
    create_table :whitelists do |t|
      t.string :email, null: false
      t.string :request_type, null: false
      t.timestamps null: false
    end
  end
end
