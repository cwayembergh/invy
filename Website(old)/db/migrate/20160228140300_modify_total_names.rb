class ModifyTotalNames < ActiveRecord::Migration
  def change
    add_column :ledger_infos, :total_tax, :decimal
    rename_column :ledger_infos, :total_amount, :total_with_taxes
    rename_column :ledger_infos, :tax_amount, :total_without_taxe
  end
end
