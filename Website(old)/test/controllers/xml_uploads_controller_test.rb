require 'test_helper'

class XmlUploadsControllerTest < ActionController::TestCase
  setup do
    @xml_upload = xml_uploads(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:xml_uploads)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create xml_upload" do
    assert_difference('XmlUpload.count') do
      post :create, xml_upload: { name: @xml_upload.name }
    end

    assert_redirected_to xml_upload_path(assigns(:xml_upload))
  end

  test "should show xml_upload" do
    get :show, id: @xml_upload
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @xml_upload
    assert_response :success
  end

  test "should update xml_upload" do
    patch :update, id: @xml_upload, xml_upload: { name: @xml_upload.name }
    assert_redirected_to xml_upload_path(assigns(:xml_upload))
  end

  test "should destroy xml_upload" do
    assert_difference('XmlUpload.count', -1) do
      delete :destroy, id: @xml_upload
    end

    assert_redirected_to xml_uploads_path
  end
end
