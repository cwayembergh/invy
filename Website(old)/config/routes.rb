Rails.application.routes.draw do
  get 'transactions/new'
  get 'corporate_pages/send_to_accountant'
  post 'requests/create_request'
  post 'admin_pages/add_access'

  resources :accountant_pages
  resources :admin_pages
  resources :basic_pages
  resources :corporate_pages
  resources :ledger_infos
  resources :ledger_items
  resources :suggestions
  resources :transactions, only: [:new, :create] 
  resources :xml_uploads
  
  
  resources :requests
  

  devise_for :accountants, :controllers => {:sessions => "accountants/sessions", :registrations => "accountants/registrations", :passwords => "accountants/passwords", :confirmations => "accountants/confirmations", :unlocks => "accountants/unlocks"}
  devise_for :admins, :controllers => {:sessions => "admins/sessions", :registrations => "admins/registrations", :passwords => "admins/passwords", :confirmations => "admins/confirmations", :unlocks => "admins/unlocks"}
  devise_for :corporates, :controllers => {:sessions => "corporates/sessions", :registrations => "corporates/registrations", :passwords => "corporates/passwords", :confirmations => "corporates/confirmations", :unlocks => "corporates/unlocks"}
  devise_for :users, :controllers => {:sessions => "users/sessions", :registrations => "users/registrations", :passwords => "users/passwords", :confirmations => "users/confirmations", :unlocks => "users/unlocks"}
  
  authenticated :accountant do
    root 'accountant_pages#dashboard', as: :accountant_root
    get 'accountant_invoices' => 'accountant_pages#invoices'
    get 'booking' => 'accountant_pages#booking'
    get 'customers' => 'accountant_pages#customers'
  end
  
  authenticated :admin do
    root 'admin_pages#dashboard', as: :admin_root
    get 'basics' => 'admin_pages#basics'
    get 'corporates' => 'admin_pages#corporates'
    get 'accountants' => 'admin_pages#accountants'
    get 'invoices_admin' => 'admin_pages#invoices'
    get 'beta' => 'admin_pages#beta_list'
  end
  
  authenticated :corporate do
    root 'corporate_pages#dashboard', as: :corporate_root
    get 'accounting' => 'corporate_pages#accounting'
    get 'products' => 'corporate_pages#products'
    get 'sent_invoices' => 'corporate_pages#sent_invoices'
    get 'received_payments' => 'corporate_pages#received_payments'
  end
  
  authenticated :user do
    root 'basic_pages#dashboard', as: :user_root
  end

  root 'pages#home'
  
  #LEDGER_INFOS CONTROLLER
  get 'create' => 'ledger_infos#new'
  get 'invoice' => 'ledger_infos#index'
  get 'new_invoices' => 'ledger_infos#new_invoices'
  get 'paid_invoices' => 'ledger_infos#paid_invoices'
  get 'received_invoices' => 'ledger_infos#received_invoices'
  get 'unpaid' => 'ledger_infos#unpaid'
  
  #PAGES CONTROLLER
  get 'sign_up' => 'pages#sign_up'
 
  #XML_UPLOADS CONTROLLER
  get 'parser' => 'xml_uploads#new'
  
  
  #BETA
  get 'beta_request' => 'requests#beta_request'

end
