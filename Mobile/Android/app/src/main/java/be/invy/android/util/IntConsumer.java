package be.invy.android.util;

public interface IntConsumer {

    void accept(int i);

}
