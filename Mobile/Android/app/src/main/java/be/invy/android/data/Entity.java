package be.invy.android.data;

import java.io.Serializable;

public class Entity implements Serializable {

    private final int entityId;
    private final int entityIdInType;
    private final EntityType entityType;
    private final String name;

    protected Entity(int entityId, int entityIdInType, EntityType entityType, String name) {
        this.entityId = entityId;
        this.entityIdInType = entityIdInType;
        this.entityType = entityType;
        this.name = name;
    }

    public int getEntityId() {
        return entityId;
    }

    public int getEntityIdInType() {
        return entityIdInType;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public String getName() {
        return name;
    }
}
