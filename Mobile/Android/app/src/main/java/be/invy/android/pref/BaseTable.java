package be.invy.android.pref;

import android.provider.BaseColumns;

public interface BaseTable extends BaseColumns {

    String SELECTION_NONE = "";

    String[] COLUMNS_NONE = {};

}
