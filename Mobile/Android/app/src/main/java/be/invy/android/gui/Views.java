package be.invy.android.gui;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import be.invy.android.R;

public final class Views {

    private Views() {}

    public static <T extends View> T findById(ViewGroup group, @IdRes int id) {
        for (int i = 0, l = group.getChildCount(); i < l; i++) {
            View view = group.getChildAt(i);
            if (view.getId() == id) {
                return (T) view;
            } else if (view instanceof ViewGroup) {
                T result = findById((ViewGroup) view, id);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    public static void setText(Activity activity, @IdRes int id, @StringRes int text) {
        TextView view = (TextView) activity.findViewById(id);
        view.setText(text);
    }

    public static void setText(Activity activity, @IdRes int id, CharSequence text) {
        TextView view = (TextView) activity.findViewById(id);
        view.setText(text);
    }

    public static void setText(View view, @IdRes int id, @StringRes int text) {
        TextView textView = (TextView) view.findViewById(id);
        textView.setText(text);
    }

    public static void setText(View view, @IdRes int id, CharSequence text) {
        TextView textView = (TextView) view.findViewById(id);
        textView.setText(text);
    }

    public static void setForeground(Activity activity, TextView textView, @ColorRes int color) {
        textView.setTextColor(ContextCompat.getColor(activity, color));
    }

    public static ProgressBar progressBar(Activity activity, int visibility) {
        ProgressBar progressBar = new ProgressBar(activity, null,
                android.R.attr.progressBarStyleInverse);
        progressBar.setVisibility(visibility);
        return progressBar;
    }
}
