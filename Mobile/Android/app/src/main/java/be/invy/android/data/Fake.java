package be.invy.android.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import be.invy.android.data.Invoice.InvoiceLine;

import static be.invy.android.data.Currency.EURO;
import static be.invy.android.data.Invoice.State.NEW;
import static be.invy.android.data.Invoice.State.PAID;
import static be.invy.android.data.Invoice.Type.CREDIT_NOTE;
import static be.invy.android.data.Invoice.Type.INVOICE;

public class Fake {

    public static User ffo() {
        return new User(1, "Fonteyn", "François", "ffo@invy.com",
                Arrays.asList(classicFfo(), swift(), deloitte()));
    }

    public static User fgo() {
        return new User(2, "Gossart", "François", "fgo@invy.com",
                Collections.<Entity>emptyList());
    }

    public static User cwa() {
        return new User(3, "Wayembergh", "Clément", "cwa@invy.com",
                Collections.<Entity>emptyList());
    }

    public static Classic classicFfo() {
        return new Classic(1, 1, "François Fonteyn");
    }

    public static Corporate swift() {
        return new Corporate(2, 1, "Swift");
    }

    public static Accountant deloitte() {
        return new Accountant(3, 1, "Deloitte");
    }

    public static List<Invoice> invoices() {
        List<Invoice> invoices = new ArrayList<>();
        invoices.add(invoice1());
        invoices.add(invoice2());
        invoices.add(invoice3());
        invoices.add(invoice4());
        invoices.add(invoice5());
        invoices.add(invoice1());
        invoices.add(invoice2());
        invoices.add(invoice3());
        invoices.add(invoice4());
        invoices.add(invoice5());
        return invoices;
    }

    public static Invoice invoice1() {
        List<InvoiceLine> lines = new ArrayList<>();
        lines.add(invoiceLineMuffin());
        lines.add(invoiceLineIphone());
        return new Invoice(swift(),
                classicFfo(),
                EURO,
                lines.get(0).totalAti().add(lines.get(1).totalAti()),
                lines.get(0).totalNti().add(lines.get(1).totalNti()),
                new Date(),
                new Date(),
                new Date(),
                PAID,
                "Muffin and iPhone",
                lines,
                INVOICE);
    }

    public static Invoice invoice2() {
        List<InvoiceLine> lines = new ArrayList<>();
        lines.add(invoiceLineHorse());
        lines.add(invoiceLineBrussels());
        return new Invoice(swift(),
                classicFfo(),
                EURO,
                lines.get(0).totalAti().add(lines.get(1).totalAti()),
                lines.get(0).totalNti().add(lines.get(1).totalNti()),
                new Date(),
                new Date(),
                new Date(),
                PAID,
                "Bojack Horseman and Brussels",
                lines,
                INVOICE);
    }

    public static Invoice invoice3() {
        List<InvoiceLine> lines = new ArrayList<>();
        lines.add(invoiceLineBed());
        lines.add(invoiceLineMuffin());
        lines.add(invoiceLineHorse());
        return new Invoice(swift(),
                classicFfo(),
                EURO,
                lines.get(0).totalAti().add(lines.get(1).totalAti()).add(lines.get(2).totalAti()),
                lines.get(0).totalNti().add(lines.get(1).totalNti()).add(lines.get(2).totalNti()),
                new Date(),
                new Date(),
                null,
                NEW,
                "Bed, Muffin and Bojack Horseman from Horsin' around",
                lines,
                INVOICE);
    }

    public static Invoice invoice4() {
        List<InvoiceLine> lines = new ArrayList<>();
        lines.add(invoiceLineOnions());
        return new Invoice(swift(),
                classicFfo(),
                EURO,
                lines.get(0).totalAti(),
                lines.get(0).totalNti(),
                new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(2)),
                new Date(),
                null,
                NEW,
                "Some onions",
                lines,
                INVOICE);
    }

    public static Invoice invoice5() {
        List<InvoiceLine> lines = new ArrayList<>();
        lines.add(invoiceLineIphone());
        return new Invoice(swift(),
                classicFfo(),
                EURO,
                lines.get(0).totalAti(),
                lines.get(0).totalNti(),
                new Date(),
                new Date(),
                null,
                NEW,
                "676-900098-100",
                lines,
                CREDIT_NOTE);
    }

    public static InvoiceLine invoiceLineIphone() {
        return new InvoiceLine(iphone(), 2, BigDecimal.valueOf(21, 0));
    }

    public static InvoiceLine invoiceLineBed() {
        return new InvoiceLine(bed(), 1, BigDecimal.valueOf(11, 0));
    }

    public static InvoiceLine invoiceLineMuffin() {
        return new InvoiceLine(muffin(), 10, BigDecimal.valueOf(21, 0));
    }

    public static InvoiceLine invoiceLineBrussels() {
        return new InvoiceLine(brussels(), 1, BigDecimal.valueOf(50, 0));
    }

    public static InvoiceLine invoiceLineHorse() {
        return new InvoiceLine(horse(), 3, BigDecimal.valueOf(5, 0));
    }

    public static InvoiceLine invoiceLineOnions() {
        return new InvoiceLine(onions(), 1, BigDecimal.valueOf(21, 0));
    }

    public static Product iphone() {
        return new Product("iPhone 6", "64GB black", BigDecimal.valueOf(699_99, 2));
    }

    public static Product bed() {
        return new Product("Bed", "160x200 black", BigDecimal.valueOf(574_99, 2));
    }

    public static Product muffin() {
        return new Product("Muffin", "Chocolate", BigDecimal.valueOf(2_99, 2));
    }

    public static Product brussels() {
        return new Product("Brussels", "That's right", BigDecimal.valueOf(1, 0));
    }

    public static Product horse() {
        return new Product("Horse", "Bojack", BigDecimal.valueOf(20_000, 0));
    }

    public static Product onions() {
        return new Product("Onions", "Miam", BigDecimal.valueOf(99_99, 2));
    }
}
