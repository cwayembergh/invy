package be.invy.android.activity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;

import java.util.HashSet;
import java.util.Set;

import be.invy.android.data.Entity;
import be.invy.android.data.User;
import be.invy.android.gui.MainFrameLayout;
import be.invy.android.pref.PreferencesDbOpenHelper;

public abstract class ActivityBase extends AppCompatActivity {

    protected final String id;
    private final @LayoutRes int layoutId;
    private final Set<OnGlobalLayoutListener> globalLayoutListeners = new HashSet<>();
    private PreferencesDbOpenHelper dbPreferences;

    private MainFrameLayout mainFrameLayout;
    private User user;
    private int entityIndex;

    protected ActivityBase(String id, @LayoutRes int layoutId) {
        this.id = id;
        this.layoutId = layoutId;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(id, "Starting");
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        setContentView(layoutId);
        mainFrameLayout = MainFrameLayout.getCurrentMainFrameLayout();
        user = (User) getIntent().getSerializableExtra(User.EXTRA_USER);
        entityIndex = getIntent().getIntExtra(User.EXTRA_ENTITY_INDEX, -1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMainFrameLayout().requestFocus();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        for (OnGlobalLayoutListener globalLayoutListener : globalLayoutListeners) {
            unregisterGlobalLayoutListener(globalLayoutListener);
        }
        if (dbPreferences != null) {
            dbPreferences.close();
        }
    }

    protected void registerGlobalLayoutListener(OnGlobalLayoutListener globalLayoutListener) {
        if (globalLayoutListeners.add(globalLayoutListener)) {
            mainFrameLayout.getViewTreeObserver().addOnGlobalLayoutListener(globalLayoutListener);
        }
    }

    protected void unregisterGlobalLayoutListener(OnGlobalLayoutListener globalLayoutListener) {
        if (globalLayoutListeners.remove(globalLayoutListener)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mainFrameLayout.getViewTreeObserver().removeOnGlobalLayoutListener(globalLayoutListener);
            }
        }
    }

    protected SQLiteDatabase getReadablePreferences() {
        if (dbPreferences == null) {
            dbPreferences = new PreferencesDbOpenHelper(this);
        }
        return dbPreferences.getReadableDatabase();
    }

    protected SQLiteDatabase getWritablePreferences() {
        if (dbPreferences == null) {
            dbPreferences = new PreferencesDbOpenHelper(this);
        }
        return dbPreferences.getWritableDatabase();
    }

    protected MainFrameLayout getMainFrameLayout() {
        return mainFrameLayout;
    }

    public User getUser() {
        return user;
    }

    public int getEntityIndex() {
        return entityIndex;
    }

    public Entity getEntity() {
        return user.getEntities().get(entityIndex);
    }
}
