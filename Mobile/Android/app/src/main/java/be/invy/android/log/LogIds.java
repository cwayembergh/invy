package be.invy.android.log;

public final class LogIds {

    private static final String LOG_PREFIX = "Proprietary_";

    private LogIds() {
    }

    public static String of(Class<?> clazz) {
        return of(clazz.getSimpleName());
    }

    public static String of(String name) {
        return LOG_PREFIX + name;
    }

}
