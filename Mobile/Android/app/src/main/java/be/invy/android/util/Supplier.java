package be.invy.android.util;

import java.io.Serializable;

public interface Supplier<T> extends Serializable {

    T supply();

}
