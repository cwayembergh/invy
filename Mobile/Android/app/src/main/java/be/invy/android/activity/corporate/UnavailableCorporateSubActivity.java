package be.invy.android.activity.corporate;

import android.support.annotation.StringRes;

import be.invy.android.R;
import be.invy.android.activity.ActivityBaseWithSuba;
import be.invy.android.activity.SubActivity;
import be.invy.android.gui.Views;
import be.invy.android.log.LogIds;

final class UnavailableCorporateSubActivity extends SubActivity<Integer> {

    UnavailableCorporateSubActivity() {
        super(LogIds.of(UnavailableCorporateSubActivity.class), R.layout.content_corporate_unavailable);
    }

    @Override
    protected void initialize(ActivityBaseWithSuba parent, @StringRes Integer text) {
        super.initialize(parent, text);
        Views.setText(parent, R.id.unavailable_title, parent.getEntity().getName());
        Views.setText(parent, R.id.unavailable_text, text);
    }

}
