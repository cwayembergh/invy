package be.invy.android.pref;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import be.invy.android.Constants;
import be.invy.android.log.LogIds;

public class PreferencesDbOpenHelper extends SQLiteOpenHelper {

    private static final String ID = LogIds.of(PreferencesDbOpenHelper.class);

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "Preference.db";

    public PreferencesDbOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(ID, "Creating database");
        db.execSQL(UserRecord.SQL_CREATE_ENTRIES);
//        db.execSQL(UserRecord.SQL_CREATE_INDEX);
        // TODO remove
        UserRecord.insert(db, new UserRecord()
                .withEmail("fgo@invy.com")
                .withLastLogin(0));
        UserRecord.insert(db, new UserRecord()
                .withEmail("cwa@invy.com")
                .withLastLogin(0));
        UserRecord.insert(db, new UserRecord()
                .withEmail("ffo@invy.com")
                .withLastLogin(1));
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(UserRecord.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
