package be.invy.android.activity.entity;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import be.invy.android.R;
import be.invy.android.data.Entity;
import be.invy.android.gui.Fonts;

import static android.view.View.GONE;
import static be.invy.android.gui.Fonts.IMAGE;

public class EntityAdapter extends ArrayAdapter<Entity> {

    public EntityAdapter(Context context, List<Entity> entities) {
        super(context, 0, entities);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            convertView = layoutInflater.inflate(R.layout.item_entity, parent, false);
        }
        TextView logo = (TextView) convertView.findViewById(R.id.entity_item_logo);
        View progressBar = convertView.findViewById(R.id.progress_bar);
        progressBar.setVisibility(GONE);
        Fonts.setFont(getContext(), logo, IMAGE);
        TextView name = (TextView) convertView.findViewById(R.id.entity_item_name);
        TextView desc = (TextView) convertView.findViewById(R.id.entity_item_desc);
        Entity entity = getItem(position);
        name.setText(entity.getName());
        switch (entity.getEntityType()) {
            case CLASSIC:
                logo.setText(R.string.logo_entity_classic);
                desc.setText(R.string.desc_entity_classic);
                break;
            case CORPORATE:
                logo.setText(R.string.logo_entity_corporate);
                desc.setText(R.string.desc_entity_corporate);
                break;
            case ACCOUNTANT:
                logo.setText(R.string.logo_entity_accountant);
                desc.setText(R.string.desc_entity_accountant);
                break;
        }
        return convertView;
    }
}
