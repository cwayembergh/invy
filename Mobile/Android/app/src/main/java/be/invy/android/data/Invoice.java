package be.invy.android.data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Invoice {

    public enum Type {
        INVOICE("Invoice"),
        CREDIT_NOTE("Credit note")
        ;
        private final String repr;

        Type(String repr) {
            this.repr = repr;
        }
        @Override
        public String toString() {
            return repr;
        }
    }

    public enum State {
        NEW, PAID
    }

    private final Entity sender;
    private final Entity receiver;
    private final Currency currency;
    private final BigDecimal totalAti;
    private final BigDecimal totalNti;
    private final Date dueDate;
    private final Date createdAt;
    private final Date paidAt;
    private final State state;
    private final String communication;
    private final List<InvoiceLine> lines;
    private final Type type;

    public Invoice(Entity sender,
                   Entity receiver,
                   Currency currency,
                   BigDecimal totalAti,
                   BigDecimal totalNti,
                   Date dueDate,
                   Date createdAt,
                   Date paidAt,
                   State state,
                   String communication,
                   List<InvoiceLine> lines,
                   Type type) {
        this.sender = sender;
        this.receiver = receiver;
        this.currency = currency;
        this.totalAti = totalAti;
        this.totalNti = totalNti;
        this.dueDate = dueDate;
        this.createdAt = createdAt;
        this.paidAt = paidAt;
        this.state = state;
        this.communication = communication;
        this.lines = lines;
        this.type = type;
    }

    public Entity getSender() {
        return sender;
    }

    public Entity getReceiver() {
        return receiver;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getTotalAti() {
        return totalAti;
    }

    public BigDecimal getTotalNti() {
        return totalNti;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getPaidAt() {
        return paidAt;
    }

    public State getState() {
        return state;
    }

    public String getCommunication() {
        return communication;
    }

    public List<InvoiceLine> getLines() {
        return lines;
    }

    public Type getType() {
        return type;
    }

    public static final class InvoiceLine {

        private final Product product;
        private final int quantity;
        private final BigDecimal taxRate;

        public InvoiceLine(Product product, int quantity, BigDecimal taxRate) {
            this.product = product;
            this.quantity = quantity;
            this.taxRate = taxRate;
        }

        public Product getProduct() {
            return product;
        }

        public int getQuantity() {
            return quantity;
        }

        public BigDecimal getTaxRate() {
            return taxRate;
        }

        public BigDecimal totalAti() {
            return totalNti().multiply(taxRate);
        }

        public BigDecimal totalNti() {
            return product.getPrice().multiply(BigDecimal.valueOf(quantity));
        }
    }

}
