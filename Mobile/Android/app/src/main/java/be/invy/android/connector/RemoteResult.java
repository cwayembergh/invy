package be.invy.android.connector;

import android.support.annotation.StringRes;

import be.invy.android.data.User;

public class RemoteResult<T> {

    private final boolean success;
    private final @StringRes int errorMessage;
    private final T result;

    public RemoteResult(@StringRes int errorMessage) {
        this(false, errorMessage, null);
    }

    public RemoteResult(T result) {
        this(true, 0, result);
    }

    private RemoteResult(boolean success, @StringRes int errorMessage, T result) {
        this.success = success;
        this.errorMessage = errorMessage;
        this.result = result;
    }

    boolean isSuccess() {
        return success;
    }

    int getErrorMessage() {
        return errorMessage;
    }

    T getResult() {
        return result;
    }
}
