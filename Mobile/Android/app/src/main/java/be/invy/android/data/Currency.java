package be.invy.android.data;

import android.support.annotation.StringRes;

import be.invy.android.R;

public enum Currency {

    EURO(R.string.fa_eur)
    ;
    private final @StringRes int logoId;

    Currency(int logoId) {
        this.logoId = logoId;
    }

    @StringRes
    public int getLogoId() {
        return logoId;
    }
}
