package be.invy.android.util;

public interface Consumer<T> {

    void accept(T t);

}
