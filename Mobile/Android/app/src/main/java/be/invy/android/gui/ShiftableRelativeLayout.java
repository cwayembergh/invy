package be.invy.android.gui;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.annotation.IntegerRes;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import be.invy.android.log.LogIds;
import be.invy.android.util.IntConsumer;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;

public class ShiftableRelativeLayout extends RelativeLayout {

    private static final String ID = LogIds.of(ShiftableRelativeLayout.class);

    private View overBackground;
    private int maximumHeight;

    public ShiftableRelativeLayout(Context context) {
        super(context);
    }

    public ShiftableRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ShiftableRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ShiftableRelativeLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setOverBackground(View overBackground) {
        this.overBackground = overBackground;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        getLayoutParams().height = MATCH_PARENT;
//        overBackground.getLayoutParams().height = MATCH_PARENT;
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        Log.d(ID, "measured_height: " + getMeasuredHeight());
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        Log.d(ID, "layout: "+changed+", "+left+", "+right+", "+top+", "+bottom);
        int height = bottom - top;
        maximumHeight = Math.max(maximumHeight, height);
        ((MarginLayoutParams)overBackground.getLayoutParams()).bottomMargin = maximumHeight - height;
//        setPadding(0, 0, 0, maximumHeight - height);
//        overBackground.getLayoutParams().height = height;
//        getLayoutParams().height = maximumHeight;
//        Log.d(ID, "layouts: "+overBackground.getLayoutParams().height+", "+getLayoutParams().height);
        super.onLayout(changed, left, top, right, maximumHeight);
    }

//    private void dom() {
//        addOnLayoutChangeListener(new OnLayoutChangeListener() {
//            @Override
//            public void onLayoutChange(View view, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
//                int newWidth = right - left;
//                int oldWidth = oldRight - oldLeft;
//                if (newWidth == oldWidth) {
//                    int newHeight = bottom - top;
//                    int oldHeight = oldBottom - oldTop;
//                    int heightDiff = newHeight - oldHeight;
//                    if (heightDiff > 0) {
//                        Log.d(ID, "back: "+heightDiff);
//                    } else if (heightDiff < 0) {
//                        MarginLayoutParams layoutParams = (MarginLayoutParams) getLayoutParams();
//                        layoutParams.height = oldHeight;
//                        layoutParams.bottomMargin = -heightDiff;
//                        setLayoutParams(layoutParams);
//                        Log.d(ID, "forth: "+heightDiff);
//                    }
//                }
//            }
//        });
//    }
//
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//    }
//
//    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//    }
}
