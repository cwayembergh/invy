package be.invy.android.data;

public class Classic extends Entity {

    public Classic(int entityId, int entityIdInType, String name) {
        super(entityId, entityIdInType, EntityType.CLASSIC, name);
    }
}
