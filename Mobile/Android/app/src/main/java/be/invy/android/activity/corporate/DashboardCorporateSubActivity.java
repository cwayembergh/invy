package be.invy.android.activity.corporate;

import be.invy.android.R;
import be.invy.android.activity.ActivityBaseWithSuba;
import be.invy.android.activity.SubActivity;
import be.invy.android.gui.Views;
import be.invy.android.log.LogIds;

final class DashboardCorporateSubActivity extends SubActivity<Void> {

    DashboardCorporateSubActivity() {
        super(LogIds.of(DashboardCorporateSubActivity.class), R.layout.content_corporate_dashboard);
    }

    @Override
    protected void initialize(ActivityBaseWithSuba parent, Void context) {
        super.initialize(parent, context);
        Views.setText(parent, R.id.dashboard_title, parent.getEntity().getName());
        // TODO
    }

}
