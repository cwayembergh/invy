package be.invy.android.activity;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import be.invy.android.R;
import be.invy.android.gui.Menus;

import static be.invy.android.gui.Fonts.CORE;
import static be.invy.android.gui.Fonts.IMAGE;
import static be.invy.android.gui.Fonts.INVY;
import static be.invy.android.gui.Fonts.setFontRecursively;

public abstract class ActivityBaseWithSubaAndDrawer extends ActivityBaseWithSuba
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String SELECTED_MENU_ITEM = "SELECTED_MENU_ITEM";

    private final @IdRes int drawerId;
    private final @IdRes int toolbarId;
    private final @IdRes int navigationId;

    protected LinearLayout toolbar;
    protected DrawerLayout drawer;
    protected NavigationView navigation;

    private MenuItem selectedMenuItem;

    protected ActivityBaseWithSubaAndDrawer(String id, @LayoutRes int layoutId, @IdRes int drawerId,
                                            @IdRes int toolbarId, @IdRes int navigationId,
                                            SubActivityChooser subActivityChooser) {
        super(id, layoutId, subActivityChooser);
        this.drawerId = drawerId;
        this.toolbarId = toolbarId;
        this.navigationId = navigationId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        toolbar = (LinearLayout) findViewById(toolbarId);
        drawer = (DrawerLayout) findViewById(drawerId);
        navigation = (NavigationView) findViewById(navigationId);
        final Menu menu = navigation.getMenu();

        setFontRecursively(this, navigation.getHeaderView(0), INVY);
        setFontRecursively(this, menu, IMAGE, CORE);
        setFontRecursively(this, toolbar, IMAGE);

        if (savedInstanceState != null) {
            int selectedMenuItemId = savedInstanceState.getInt(SELECTED_MENU_ITEM);
            if (selectedMenuItemId != 0) {
                selectedMenuItem = menu.findItem(selectedMenuItemId);
            }
        }

        navigation.setNavigationItemSelectedListener(this);
        drawer.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (selectedMenuItem == null) {
                    Menus.uncheckAll(menu);
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED_MENU_ITEM,
                selectedMenuItem == null ? 0 : selectedMenuItem.getItemId());
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public final boolean onNavigationItemSelected(MenuItem item) {
        if (item == selectedMenuItem) {
            return true;
        }
        int itemId = item.getItemId();
        switch (itemId) {
            case R.id.nav_entity_chooser:
                launchSubView(ENTITY_CHOOSER_SUBA);
                break;
            case R.id.nav_log_out:
                logOut();
                break;
            default:
                onNavigation(itemId);
        }
        if (selectedMenuItem != null) {
            selectedMenuItem.setChecked(false);
        }
        selectedMenuItem = item;
        item.setChecked(true);
        closeDrawer(null);
        return true;
    }

    protected abstract void onNavigation(int itemId);

    public void showDrawer(View view) {
        drawer.openDrawer(GravityCompat.END);
    }

    public void closeDrawer(View view) {
        drawer.closeDrawer(GravityCompat.END);
    }

    public void goToDashboard(View view) {
        closeDrawer(null);
        if (selectedMenuItem != null) {
            selectedMenuItem.setChecked(false);
            selectedMenuItem = null;
            launchSubView(DASHBOARD_SUBA);
        }
    }

}
