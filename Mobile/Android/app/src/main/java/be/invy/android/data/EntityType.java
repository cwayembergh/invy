package be.invy.android.data;

import java.io.Serializable;

public enum EntityType implements Serializable {

    CLASSIC("Classic"),
    CORPORATE("Corporate"),
    ACCOUNTANT("Accountant")
    ;

    private final String repr;

    EntityType(String repr) {
        this.repr = repr;
    }

    @Override
    public String toString() {
        return repr;
    }
}
