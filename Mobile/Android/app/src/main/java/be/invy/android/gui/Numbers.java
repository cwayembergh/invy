package be.invy.android.gui;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

import static java.util.Locale.US;

public final class Numbers {

    private static final DecimalFormat INTEGER_FORMATTER = (DecimalFormat) NumberFormat.getInstance(US);

    static {
        DecimalFormatSymbols symbols = INTEGER_FORMATTER.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(' ');
        INTEGER_FORMATTER.setDecimalFormatSymbols(symbols);
    }

    private Numbers() {}

    public static String format(BigDecimal number) {
        int left = number.intValue();
        BigDecimal decimal = number.subtract(BigDecimal.valueOf(left));
        int right = decimal.movePointRight(decimal.scale()).intValue();
        return format(left) + "." + right;
    }

    public static String format(int number) {
        return INTEGER_FORMATTER.format(number);
    }

}
