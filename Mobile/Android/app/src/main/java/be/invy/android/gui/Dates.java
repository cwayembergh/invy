package be.invy.android.gui;

import android.text.format.DateFormat;

import java.util.Date;

public final class Dates {

    private Dates() {}

    public static CharSequence format(Date date) {
        return DateFormat.format("yyyy-MM-dd", date);
    }

}
