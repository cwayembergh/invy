package be.invy.android.gui;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class MainFrameLayout extends FrameLayout {

    private static MainFrameLayout currentMainFrameLayout;

    public MainFrameLayout(Context context) {
        super(context);
        setup();
    }

    public MainFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public MainFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MainFrameLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setup();
    }

    private void setup() {
        setFocusable(true);
        setFocusableInTouchMode(true);
        currentMainFrameLayout = this;
    }

    public static MainFrameLayout getCurrentMainFrameLayout() {
        return currentMainFrameLayout;
    }

}
