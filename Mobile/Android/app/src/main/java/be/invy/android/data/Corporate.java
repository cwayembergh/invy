package be.invy.android.data;

public class Corporate extends Entity {

    public Corporate(int entityId, int entityIdInType, String name) {
        super(entityId, entityIdInType, EntityType.CORPORATE, name);
    }
}
