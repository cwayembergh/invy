package be.invy.android.connector;

import android.os.AsyncTask;
import android.support.annotation.StringRes;
import android.util.Log;

import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

import be.invy.android.R;
import be.invy.android.activity.welcome.WelcomeActivity;
import be.invy.android.data.Fake;
import be.invy.android.data.User;
import be.invy.android.log.LogIds;

public abstract class RemoteTask<T> extends AsyncTask<Void, Void, RemoteResult<T>> {

    private final String id;
    private HttpsURLConnection connection;

    protected RemoteTask(String id) {
        this.id = id;
    }

    @Override
    protected final RemoteResult<T> doInBackground(Void... params) {
        try {
            connection = null; // TODO
            return asyncCall(connection);
        } catch (Exception e) {
            return new RemoteResult<>(R.string.Network_error);
        }
    }

    protected abstract RemoteResult<T> asyncCall(HttpsURLConnection connection);

    protected abstract void onSuccess(T result);

    protected abstract void onFailure(@StringRes int error);

    @Override
    protected void onPostExecute(RemoteResult<T> result) {
        if (result.isSuccess()) {
            onSuccess(result.getResult());
        } else {
            onFailure(result.getErrorMessage());
        }
    }

    @Override
    protected void onCancelled() {
        if (connection != null) {
            try {
                connection.disconnect();
            } catch (Exception e) {
                Log.e(id, "Could not cancel connection", e);
            }
        }
    }

}
