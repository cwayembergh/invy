package be.invy.android.activity.welcome;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;


import java.util.Map;

import be.invy.android.R;
import be.invy.android.activity.ActivityBase;
import be.invy.android.activity.entity.EntityChooserActivity;
import be.invy.android.data.User;
import be.invy.android.gui.Fonts;
import be.invy.android.gui.Keyboards;
import be.invy.android.log.LogIds;
import be.invy.android.pref.UserRecord;
import be.invy.android.pref.UserRecord.FullSelectResult;
import be.invy.android.util.ButtonListener;

import static android.util.Patterns.EMAIL_ADDRESS;
import static be.invy.android.Constants.PASSWORD_MIN_LENGTH;

public class WelcomeActivity extends ActivityBase {

    private AutoCompleteTextView emailView;
    private TextView passwordView;
    private CheckBox rememberEmailView;
    private LoginTask loginTask;
    private ProgressBar progressBar;
    private ViewGroup loginForm;
    private Map<String, UserRecord> users;

    public WelcomeActivity() {
        super(LogIds.of(WelcomeActivity.class), R.layout.activity_welcome);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        emailView = (AutoCompleteTextView) findViewById(R.id.email);
        passwordView = (TextView) findViewById(R.id.password);
        rememberEmailView = (CheckBox) findViewById(R.id.remember_email);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        loginForm = (ViewGroup) findViewById(R.id.login_form);

        setupFonts();
        setupAutoComplete();
        setupAttemptLoginOnDone();
    }

    private void setupFonts() {
        Fonts.setFont(this, (TextView) findViewById(R.id.invy), Fonts.INVY);
        Fonts.setFont(this, emailView, Fonts.CORE);
        Fonts.setFont(this, passwordView, Fonts.CORE);
        Fonts.setFont(this, rememberEmailView, Fonts.CORE);
        Fonts.setFont(this, (TextView) findViewById(R.id.sign_in), Fonts.CORE);
    }

    private void setupAutoComplete() {
        emailView.setThreshold(1);
        FullSelectResult result = UserRecord.fullSelect(getReadablePreferences());
        users = result.users;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                        android.R.layout.simple_dropdown_item_1line, result.sortedEmails);
        emailView.setAdapter(adapter);
        Log.d(id, "Setup sorted emails auto-complete to " + result.sortedEmails);
        emailView.addTextChangedListener(new TextWatcher() {
            private String email;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                email = s.toString();
            }
            @Override
            public void afterTextChanged(Editable s) {
                UserRecord userRecord = users.get(email);
                if (userRecord != null) {
                    passwordView.requestFocus();
                    rememberEmailView.setChecked(true);
                }
            }
        });
        if (!result.sortedEmails.isEmpty()) {
            String bestEmail = result.sortedEmails.get(0);
            Log.d(id, "Using best email: " + bestEmail);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                emailView.setText(bestEmail, false);
            } else {
                emailView.setText(bestEmail);
            }
        }
    }

    private void setupAttemptLoginOnDone() {
        passwordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.sign_in_action || id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin(textView);
                    return true;
                }
                return false;
            }
        });
    }

    @ButtonListener(R.id.sign_in)
    public void attemptLogin(View view) {
        if (loginTask != null) {
            return;
        }
        emailView.setError(null);
        passwordView.setError(null);

        String email = emailView.getText().toString();
        if (TextUtils.isEmpty(email) || !EMAIL_ADDRESS.matcher(email).matches()) {
            emailView.setError(getString(R.string.Invalid_email));
            emailView.requestFocus();
            return;
        }
        String password = passwordView.getText().toString();
        if (TextUtils.isEmpty(password) || password.length() < PASSWORD_MIN_LENGTH) {
            passwordView.setError(getString(R.string.Invalid_password));
            passwordView.requestFocus();
            return;
        }
        boolean rememberEmail = rememberEmailView.isChecked();

        UserRecord userRecord = users.get(email);
        if (userRecord == null) {
            if (rememberEmail) {
                userRecord = new UserRecord()
                        .withEmail(email)
                        .withLoginCount(1)
                        .withLastLogin((int) (System.currentTimeMillis() / 1000));
                UserRecord.insert(getWritablePreferences(), userRecord);
            }
        } else {
            if (rememberEmail) {
                userRecord
                        .withLoginCount(userRecord.getLoginCount() + 1)
                        .withLastLogin((int) (System.currentTimeMillis() / 1000));
                UserRecord.update(getWritablePreferences(), userRecord);
            } else {
                UserRecord.delete(getWritablePreferences(), userRecord);
            }
        }
        doAttemptLogin(email, password);
    }

    private void doAttemptLogin(String email, String password) {
        Log.d(id, "Attempt login");
        showProgress(true);
        loginTask = new LoginTask(this, email, password);
        loginTask.execute();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    void showProgress(final boolean show) {
        Keyboards.hideKeyboard(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            loginForm.setVisibility(show ? View.GONE : View.VISIBLE);
            loginForm.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    loginForm.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            loginForm.setVisibility(show ? View.GONE : View.VISIBLE);
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    void launchNextActivity(User user) {
        Intent intent = new Intent(this, EntityChooserActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(User.EXTRA_USER, user);
        startActivity(intent);
    }

    void showErrorMessage(@StringRes int errorMessageId) {
        new AlertDialog.Builder(this)
                .setTitle("Could not authenticate")
                .setMessage(getString(errorMessageId))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        getMainFrameLayout().requestFocus();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    void nullLoginTask() {
        loginTask = null;
    }
}
