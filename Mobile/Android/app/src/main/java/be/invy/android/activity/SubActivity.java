package be.invy.android.activity;

import android.support.annotation.LayoutRes;
import android.util.Log;

import be.invy.android.R;
import be.invy.android.data.User;

import static be.invy.android.gui.Fonts.CORE;
import static be.invy.android.gui.Fonts.setFontRecursively;

public abstract class SubActivity<T> {

    protected final String id;
    protected final @LayoutRes int rootLayout;

    protected SubActivity(String id, @LayoutRes int rootLayout) {
        this.id = id;
        this.rootLayout = rootLayout;
    }

    protected void initialize(ActivityBaseWithSuba parent, T context) {
        Log.d(id, "Initializing");
        setFontRecursively(parent, parent.findViewById(R.id.inner_content), CORE);
    }

}
