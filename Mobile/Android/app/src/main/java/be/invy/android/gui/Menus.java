package be.invy.android.gui;

import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

public final class Menus {

    private Menus() {}

    public static void uncheckAll(Menu menu) {
        for (int i = 0, l = menu.size(); i < l; i++) {
            MenuItem item = menu.getItem(i);
            item.setChecked(false);
            SubMenu subMenu = item.getSubMenu();
            if (subMenu != null) {
                uncheckAll(subMenu);
            }
        }
    }

}
