package be.invy.android.util;

import java.io.Serializable;

public interface Function<X, Y> extends Serializable {

    Y apply(X x);

}
