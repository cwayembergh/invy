package be.invy.android.activity.welcome;

import android.support.annotation.StringRes;

import javax.net.ssl.HttpsURLConnection;

import be.invy.android.R;
import be.invy.android.connector.RemoteResult;
import be.invy.android.connector.RemoteTask;
import be.invy.android.data.Fake;
import be.invy.android.data.User;
import be.invy.android.log.LogIds;

public class LoginTask extends RemoteTask<User> {

    private final WelcomeActivity activity;
    private final String email;
    private final String password;

    LoginTask(WelcomeActivity activity, String email, String password) {
        super(LogIds.of(LoginTask.class));
        this.activity = activity;
        this.email = email;
        this.password = password;
    }

    @Override
    protected RemoteResult<User> asyncCall(HttpsURLConnection connection) {
        // TODO: attempt authentication against a network service.
        try {
            // Simulate network access.
            Thread.sleep(500);
        } catch (InterruptedException e) {
            return new RemoteResult<>(R.string.Network_error);
        }

        if (email.equals("ffo@invy.com") && password.equals("ffoffo")) {
            return new RemoteResult<>(Fake.ffo());
        } else if (email.equals("fgo@invy.com") && password.equals("fgofgo")) {
            return new RemoteResult<>(Fake.fgo());
        } else if (email.equals("cwa@invy.com") && password.equals("cwacwa")) {
            return new RemoteResult<>(Fake.cwa());
        } else {
            return new RemoteResult<>(R.string.Invalid_credentials);
        }
    }

    @Override
    protected void onSuccess(User result) {
        activity.nullLoginTask();
        activity.launchNextActivity(result);
    }

    @Override
    protected void onFailure(@StringRes int error) {
        activity.nullLoginTask();
        activity.showProgress(false);
        activity.showErrorMessage(error);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        activity.nullLoginTask();
        activity.showProgress(false);
    }

}
