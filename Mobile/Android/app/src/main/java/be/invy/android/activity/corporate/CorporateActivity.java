package be.invy.android.activity.corporate;

import android.view.ViewOverlay;

import be.invy.android.R;
import be.invy.android.activity.ActivityBaseWithSubaAndDrawer;
import be.invy.android.activity.SubActivity;
import be.invy.android.activity.SubActivityChooser;
import be.invy.android.activity.SubActivityChooser.SubActivityId;
import be.invy.android.activity.entity.EntityChooserSubActivity;
import be.invy.android.log.LogIds;
import be.invy.android.util.Supplier;

public class CorporateActivity extends ActivityBaseWithSubaAndDrawer {

    private static final @SubActivityId int UNAVAILABLE_SUBA = 2;
    private static final @SubActivityId int SENT_INVOICES_SUBA = 3;
    private static final @SubActivityId int UNDER_DEV_SUBA = 4;

    public CorporateActivity() {
        super(LogIds.of(CorporateActivity.class), R.layout.activity_corporate,
                R.id.drawer, R.id.toolbar, R.id.navigation, subActivityChooser());
    }

    private static SubActivityChooser subActivityChooser() {
        return new SubActivityChooser(DASHBOARD_SUBA,
                new Supplier<SubActivity>() {
                    @Override
                    public SubActivity supply() {
                        return new DashboardCorporateSubActivity();
                    }
                },
                new Supplier<SubActivity>() {
                    @Override
                    public SubActivity supply() {
                        return new EntityChooserSubActivity();
                    }
                },
                new Supplier<SubActivity>() {
                    @Override
                    public SubActivity supply() {
                        return new UnavailableCorporateSubActivity();
                    }
                },
                new Supplier<SubActivity>() {
                    @Override
                    public SubActivity supply() {
                        return new SentInvoicesCorporateSubActivity();
                    }
                },
                new Supplier<SubActivity>() {
                    @Override
                    public SubActivity supply() {
                        return new UnderDevCorporateSubActivity();
                    }
                }
        );
    }

    @Override
    protected final void onNavigation(int itemId) {
        switch (itemId) {
            case R.id.nav_sent_invoices:
                launchSubView(SENT_INVOICES_SUBA);
                break;
            // TODO routing
            case R.id.nav_payments_received:
            case R.id.nav_unpaid_last_month:
            case R.id.nav_new_invoices:
            case R.id.nav_paid_invoices:
            case R.id.nav_all_invoices:
            case R.id.nav_by_hand:
                launchSubView(UNDER_DEV_SUBA);
        }
    }

}
