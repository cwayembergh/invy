package be.invy.android.activity.corporate;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;
import java.util.List;

import be.invy.android.R;
import be.invy.android.activity.ActivityBase;
import be.invy.android.activity.ActivityBaseWithSuba;
import be.invy.android.activity.SubActivity;
import be.invy.android.data.Fake;
import be.invy.android.data.Invoice;
import be.invy.android.gui.Dates;
import be.invy.android.gui.Numbers;
import be.invy.android.log.LogIds;

import static be.invy.android.data.Invoice.State.PAID;
import static be.invy.android.gui.Fonts.CORE;
import static be.invy.android.gui.Fonts.IMAGE;
import static be.invy.android.gui.Fonts.setFont;
import static be.invy.android.gui.Fonts.setFontRecursively;
import static be.invy.android.gui.Views.setForeground;
import static be.invy.android.gui.Views.setText;

final class SentInvoicesCorporateSubActivity extends SubActivity<Void>
        implements AdapterView.OnItemClickListener {

    private ActivityBase parent;

    SentInvoicesCorporateSubActivity() {
        super(LogIds.of(SentInvoicesCorporateSubActivity.class),
                R.layout.content_corporate_sent_invoices);
    }

    @Override
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    protected void initialize(ActivityBaseWithSuba parent, Void context) {
        super.initialize(parent, context);
        this.parent = parent;
//        new FetchSentInvoicesTask().execute();
        ListView list = (ListView) parent.findViewById(R.id.sent_invoices);
        list.setAdapter(new SentInvoicesAdapter(parent, Fake.invoices()));
        list.setOnItemClickListener(this);
        list.setEmptyView(parent.findViewById(R.id.no_invoice));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this.parent, "TODO: " + position, Toast.LENGTH_SHORT).show();
        // TODO
    }

    private class SentInvoicesAdapter extends ArrayAdapter<Invoice> {

        private SentInvoicesAdapter(Context context, List<Invoice> invoices) {
            super(context, 0, invoices);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater layoutInflater = LayoutInflater.from(getContext());
                convertView = layoutInflater.inflate(R.layout.item_sent_invoice, parent, false);
            }
            ActivityBase parentActivity = SentInvoicesCorporateSubActivity.this.parent;
            setFontRecursively(parentActivity, convertView, CORE);
            TextView currency = (TextView) convertView.findViewById(R.id.sent_invoice_item_currency);
            TextView flag = (TextView) convertView.findViewById(R.id.sent_invoice_item_flag);
            setFont(parentActivity, currency, IMAGE);
            setFont(parentActivity, flag, IMAGE);
            Invoice invoice = getItem(position);
            setText(convertView, R.id.sent_invoice_item_desc, invoice.getCommunication());
            setText(convertView, R.id.sent_invoice_item_currency, invoice.getCurrency().getLogoId());
            setText(convertView, R.id.sent_invoice_item_amount, Numbers.format(invoice.getTotalAti()));
            setText(convertView, R.id.sent_invoice_item_date, Dates.format(invoice.getDueDate()));
            setForeground(parentActivity, flag, invoice.getState() == PAID ?
                    R.color.invy :
                    invoice.getDueDate().compareTo(new Date()) > 0 ? R.color.later : R.color.warn);
            setText(convertView, R.id.sent_invoice_item_to, invoice.getReceiver().getName());
            setText(convertView, R.id.sent_invoice_item_to_type,
                    invoice.getReceiver().getEntityType().toString());
            setText(convertView, R.id.sent_invoice_item_type, invoice.getType().toString());
            return convertView;
        }
    }
}
