package be.invy.android.activity.corporate;

import be.invy.android.R;
import be.invy.android.activity.ActivityBaseWithSuba;
import be.invy.android.activity.SubActivity;
import be.invy.android.gui.Views;
import be.invy.android.log.LogIds;

final class UnderDevCorporateSubActivity extends SubActivity<Void> {

    UnderDevCorporateSubActivity() {
        super(LogIds.of(UnderDevCorporateSubActivity.class), R.layout.content_corporate_under_dev);
    }

    @Override
    protected void initialize(ActivityBaseWithSuba parent, Void context) {
        super.initialize(parent, context);
        Views.setText(parent, R.id.under_dev_title, parent.getEntity().getName());
    }

}
