package be.invy.android;

import java.net.MalformedURLException;
import java.net.URL;

public final class Constants {

    public static final int PASSWORD_MIN_LENGTH = 3;

    public static final int USER_TYPE_BASIC = 0;
    public static final int USER_TYPE_CORPORATE = 1;
    public static final int USER_TYPE_ACCOUNTANT = 2;

    public static final URL SERVER_URL = resolveUrl("http://www.google.com");

    private Constants() {
    }

    private static URL resolveUrl(String url) {
        try {
            return new URL(url);
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

}
