package be.invy.android.activity.entity;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import be.invy.android.R;
import be.invy.android.activity.ActivityBase;
import be.invy.android.activity.ActivityBaseWithSuba;
import be.invy.android.activity.SubActivity;
import be.invy.android.activity.corporate.CorporateActivity;
import be.invy.android.data.Entity;
import be.invy.android.data.User;
import be.invy.android.gui.Views;
import be.invy.android.log.LogIds;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static be.invy.android.gui.Fonts.CORE;
import static be.invy.android.gui.Fonts.setFontRecursively;

public class EntityChooserSubActivity extends SubActivity<Void>
        implements OnItemClickListener {

    private ActivityBase parent;
    private ProgressBar progressBar;

    public EntityChooserSubActivity() {
        super(LogIds.of(EntityChooserSubActivity.class), R.layout.content_entity_chooser);
    }

    @Override
    protected void initialize(ActivityBaseWithSuba parent, Void context) {
        super.initialize(parent, context);
        this.parent = parent;
        setFontRecursively(parent, parent.findViewById(R.id.entity_chooser), CORE);
        List<Entity> entities = parent.getUser().getEntities();
        ListView list = (ListView) parent.findViewById(R.id.entities);
        list.setAdapter(new EntityAdapter(parent, entities));
        list.setOnItemClickListener(this);
        list.setEmptyView(parent.findViewById(R.id.no_entity));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (this.progressBar != null) {
            this.progressBar.setVisibility(GONE);
        }
        ProgressBar progressBar = Views.findById((ViewGroup) view, R.id.progress_bar);
        if (progressBar != null) {
            progressBar.setVisibility(VISIBLE);
            this.progressBar = progressBar;
        }
        List<Entity> entities = this.parent.getUser().getEntities();
        Entity entity = entities.get(position);
        Intent intent = null;
        switch (entity.getEntityType()) {
            case CLASSIC:
                Toast.makeText(this.parent, "TODO classic dashboard", Toast.LENGTH_SHORT).show();
                // TODO
                return;
            case CORPORATE:
                if (this.parent instanceof CorporateActivity) {
                    ((CorporateActivity) this.parent).goToDashboard(null);
                    return;
                }
                intent = new Intent(this.parent, CorporateActivity.class);
                break;
            case ACCOUNTANT:
                Toast.makeText(this.parent, "TODO accountant dashboard", Toast.LENGTH_SHORT).show();
                // TODO
                return;
        }
        intent.putExtra(User.EXTRA_USER, this.parent.getUser());
        intent.putExtra(User.EXTRA_ENTITY_INDEX, position);
        this.parent.startActivity(intent);
    }
}
