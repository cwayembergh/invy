package be.invy.android.gui;

import android.app.Activity;
import android.support.annotation.IdRes;
import android.util.Log;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;

public class ShiftLayoutOnHeightChange implements OnGlobalLayoutListener {

    private final Activity activity;
    private final MainFrameLayout mainFrameLayout;

    public ShiftLayoutOnHeightChange(Activity activity, MainFrameLayout mainFrameLayout) {
        this.activity = activity;
        this.mainFrameLayout = mainFrameLayout;
    }

    @Override
    public void onGlobalLayout() {
        int rootViewHeight = mainFrameLayout.getRootView().getHeight();
        int rootLayoutHeight = mainFrameLayout.getHeight();
        int heightDiff = rootViewHeight - rootLayoutHeight;
        int contentViewTop = activity.getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int keyboardHeight = heightDiff - contentViewTop;
        Log.w("HERE", rootViewHeight + ", " + rootLayoutHeight + ", " + heightDiff
                + ", " + contentViewTop + ", " + keyboardHeight);
    }
}
