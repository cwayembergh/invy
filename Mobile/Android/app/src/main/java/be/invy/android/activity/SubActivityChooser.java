package be.invy.android.activity;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import be.invy.android.util.Supplier;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.LOCAL_VARIABLE;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;

public class SubActivityChooser implements Serializable {

    public static final String SUB_ACTIVITY_CHOOSER = SubActivityChooser.class.getSimpleName();
    private static final long serialVersionUID = 0L;

    private final Supplier<SubActivity>[] subActivityBuilders;
    private @SubActivityId int currentSubActivityId;
    private transient SubActivity[] cachedSubActivities;

    @SafeVarargs
    public SubActivityChooser(@SubActivityId int subActivityId,
                              Supplier<SubActivity>... subActivityBuilders) {
        this.subActivityBuilders = subActivityBuilders;
        this.currentSubActivityId = subActivityId;
    }

    public final <T> SubActivity<T> choose(@SubActivityId int subActivityId) {
        if (cachedSubActivities == null) {
            cachedSubActivities = new SubActivity[subActivityBuilders.length];
        }
        SubActivity<T> cachedSubActivity = cachedSubActivities[subActivityId];
        if (cachedSubActivity == null) {
            cachedSubActivity = subActivityBuilders[subActivityId].supply();
            cachedSubActivities[subActivityId] = cachedSubActivity;
        }
        currentSubActivityId = subActivityId;
        return cachedSubActivity;
    }

    @SubActivityId
    public int current() {
        return currentSubActivityId;
    }

    @Target({LOCAL_VARIABLE, FIELD, PARAMETER, METHOD})
    @Retention(RetentionPolicy.CLASS)
    public @interface SubActivityId {}

}
