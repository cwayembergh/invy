package be.invy.android.gui;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.IdRes;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public final class Keyboards {

    private Keyboards() {
    }

    public static void setupNextFocusChain(Activity activity, @IdRes int... ids) {
        TextView lastView = (TextView) activity.findViewById(ids[0]);
        for (int i = 1; i < ids.length; i++) {
            TextView toView = (TextView) activity.findViewById(ids[i]);
            setupNextFocus(lastView, toView);
            lastView = toView;
        }
    }

    public static void setupNextFocusChain(TextView... views) {
        TextView lastView = views[0];
        for (int i = 1; i < views.length; i++) {
            TextView toView = views[i];
            setupNextFocus(lastView, toView);
            lastView = toView;
        }
    }

    public static void setupNextFocus(TextView lastView, final View toView) {
        lastView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    toView.requestFocus();
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

//    private static void setupShowOnTouch(final Activity activity, View view) {
//        view.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent event) {
//                showKeyboard(activity, view);
//                return false;
//            }
//        });
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showKeyboard(activity, view);
//            }
//        });
//    }
//
    public static void hideKeyboard(Activity activity) {
        View currentFocus = activity.getCurrentFocus();
        if (currentFocus != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(currentFocus.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }
//
//    public static void showKeyboard(Activity activity, View view) {
//        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//        imm.toggleSoftInputFromWindow(view.getWindowToken(), InputMethodManager.SHOW_FORCED,
//                InputMethodManager.HIDE_IMPLICIT_ONLY);
//    }
//
//    public static void setupMutlilineEditor(Activity activity, @IdRes int id, final int maxLines) {
//        final TextView view = (TextView) activity.findViewById(id);
//        view.setMaxLines(maxLines);
//        view.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//                    int lineCount = view.getLineCount();
//                    if (lineCount < maxLines) {
//                        view.setLines(lineCount + 1);
//                    }
//                }
//                return false;
//            }
//        });
//    }

}
