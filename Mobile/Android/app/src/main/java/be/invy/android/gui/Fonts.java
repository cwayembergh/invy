package be.invy.android.gui;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public final class Fonts {

    public static final int FONT_AWESOME = 0;
    public static final int ROBOTO = 1;
    public static final int OLEOSCRIPT = 2;
    public static final int MONTSERRAT = 3;

    public static final int IMAGE = FONT_AWESOME;
    public static final int INVY = OLEOSCRIPT;
    public static final int CORE = ROBOTO;

    private static final String PATH[] = new String[]{
            "fonts/fontawesome-webfont.ttf",
            "fonts/Roboto-Medium.ttf",
            "fonts/OleoScript-Bold.ttf",
            "fonts/Montserrat-Regular.ttf"
    };
    private static final boolean LOADED[];
    private static final Typeface FONT[];
    private static final CustomTypefaceSpan SPAN[];

    static {
        LOADED = new boolean[PATH.length];
        FONT = new Typeface[PATH.length];
        SPAN = new CustomTypefaceSpan[PATH.length];
    }

    public static void setFontRecursively(Context activity, View view, int font) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0, len = viewGroup.getChildCount(); i < len; ++i) {
                View child = viewGroup.getChildAt(i);
                setFontRecursively(activity, child, font);
            }
        } else if (view instanceof TextView) {
            setFont(activity, (TextView) view, font);
        }
    }

    public static void setFontRecursively(Context activity, Menu menu, int font1, int font2) {
        for (int i = 0; i < menu.size(); i++) {
            setFont(activity, menu.getItem(i), font1, font2);
        }
    }

    public static void setFont(Context activity, MenuItem menuItem, int font1, int font2) {
        if (menuItem instanceof SubMenu) {
            setFontRecursively(activity, (SubMenu) menuItem, font1, font2);
        } else {
            ensureFont(activity, font1);
            ensureFont(activity, font2);
            SpannableString title = new SpannableString(menuItem.getTitle());
            title.setSpan(SPAN[font1], 0, 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            title.setSpan(SPAN[font2], 2, title.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            menuItem.setTitle(title);
        }
        SubMenu subMenu = menuItem.getSubMenu();
        if (subMenu != null) {
            setFontRecursively(activity, subMenu, font1, font2);
        }
    }

    public static void setFont(Context activity, TextView textView, int font) {
        ensureFont(activity, font);
        textView.setTypeface(FONT[font]);
    }

    private static void ensureFont(Context activity, int font) {
        if (!LOADED[font]) {
            FONT[font] = Typeface.createFromAsset(activity.getAssets(), PATH[font]);
            SPAN[font] = new CustomTypefaceSpan(FONT[font]);
            LOADED[font] = true;
        }
    }

    private Fonts() {}

    private static final class CustomTypefaceSpan extends TypefaceSpan {

        private final Typeface typeface;

        private CustomTypefaceSpan(Typeface typeface) {
            super("");
            this.typeface = typeface;
        }

        @Override
        public void updateDrawState(TextPaint paint) {
            paint.setTypeface(typeface);
        }

        @Override
        public void updateMeasureState(TextPaint paint) {
            paint.setTypeface(typeface);
        }

    }
}
