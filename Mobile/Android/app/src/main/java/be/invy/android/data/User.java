package be.invy.android.data;

import java.io.Serializable;
import java.util.List;

public class User implements Serializable {

    public static final String EXTRA_USER = User.class.getName();
    public static final String EXTRA_ENTITY_INDEX = "UserEntityIndex";

    private final int id;
    private final String lastName;
    private final String firstName;
    private final String email;
    private final List<Entity> entities;

    public User(int id, String lastName, String firstName, String email, List<Entity> entities) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.email = email;
        this.entities = entities;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getEmail() {
        return email;
    }

    public List<Entity> getEntities() {
        return entities;
    }
}
