package be.invy.android.data;

import java.io.Serializable;

public class DbUser implements Serializable {

    private final String fullName;

    public DbUser(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public boolean isConnected() {
        return true;
    }

}
