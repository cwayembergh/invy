package be.invy.android.activity.entity;

import be.invy.android.R;
import be.invy.android.activity.ActivityBaseWithSuba;
import be.invy.android.activity.SubActivity;
import be.invy.android.activity.SubActivityChooser;
import be.invy.android.activity.SubActivityChooser.SubActivityId;
import be.invy.android.log.LogIds;
import be.invy.android.util.Supplier;

public class EntityChooserActivity extends ActivityBaseWithSuba {

    private static final @SubActivityId int ENTITY_SUBA = 2;

    public EntityChooserActivity() {
        super(LogIds.of(EntityChooserActivity.class),
                R.layout.activity_entity_chooser,
                new SubActivityChooser(ENTITY_SUBA,
                        new Supplier<SubActivity>() {
                            @Override
                            public SubActivity supply() {
                                return null;
                            }
                        },
                        new Supplier<SubActivity>() {
                            @Override
                            public SubActivity supply() {
                                return null;
                            }
                        },
                        new Supplier<SubActivity>() {
                            @Override
                            public SubActivity supply() {
                                return new EntityChooserSubActivity();
                            }
                        }
                ));
    }

}
