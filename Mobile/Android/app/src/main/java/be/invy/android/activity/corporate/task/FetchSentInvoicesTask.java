package be.invy.android.activity.corporate.task;

import android.support.annotation.StringRes;

import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import be.invy.android.R;
import be.invy.android.connector.RemoteResult;
import be.invy.android.connector.RemoteTask;
import be.invy.android.data.Fake;
import be.invy.android.data.Invoice;
import be.invy.android.log.LogIds;

public class FetchSentInvoicesTask extends RemoteTask<List<Invoice>> {

    public FetchSentInvoicesTask() {
        super(LogIds.of(FetchSentInvoicesTask.class));
    }

    @Override
    protected RemoteResult<List<Invoice>> asyncCall(HttpsURLConnection connection) {
        // TODO
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            return new RemoteResult<>(R.string.Network_error);
        }
        return new RemoteResult<>(Fake.invoices());
    }

    @Override
    protected void onSuccess(List<Invoice> result) {

    }

    @Override
    protected void onFailure(@StringRes int error) {

    }
}
