package be.invy.android.activity;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.ViewOverlay;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import be.invy.android.R;
import be.invy.android.activity.SubActivityChooser.SubActivityId;
import be.invy.android.activity.welcome.WelcomeActivity;
import be.invy.android.gui.Menus;

import static be.invy.android.activity.SubActivityChooser.SUB_ACTIVITY_CHOOSER;
import static be.invy.android.gui.Fonts.CORE;
import static be.invy.android.gui.Fonts.IMAGE;
import static be.invy.android.gui.Fonts.INVY;
import static be.invy.android.gui.Fonts.setFontRecursively;

public abstract class ActivityBaseWithSuba extends ActivityBase {

    protected static final @SubActivityId int DASHBOARD_SUBA = 0;
    protected static final @SubActivityId int ENTITY_CHOOSER_SUBA = 1;

    private ViewGroup allContent;
    protected SubActivityChooser subActivityChooser;

    protected ActivityBaseWithSuba(String id, @LayoutRes int layoutId,
                                   SubActivityChooser subActivityChooser) {
        super(id, layoutId);
        this.subActivityChooser = subActivityChooser;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        allContent = (ViewGroup) findViewById(R.id.all_content);
        if (savedInstanceState != null) {
            subActivityChooser = (SubActivityChooser)
                    savedInstanceState.getSerializable(SUB_ACTIVITY_CHOOSER);
        }
        launchSubView(subActivityChooser.current(), null);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(SUB_ACTIVITY_CHOOSER, subActivityChooser);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            if (!moveTaskToBack(false)) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    public final void launchSubView(@SubActivityId int subActivityId) {
        launchSubView(subActivityId, null);
    }

    public final <T> void launchSubView(@SubActivityId int subActivityId, T context) {
        SubActivity<T> subActivity = subActivityChooser.choose(subActivityId);
        ViewGroup innerContent = (ViewGroup) findViewById(R.id.inner_content);
        innerContent.removeAllViews();
        innerContent.addView(View.inflate(this, subActivity.rootLayout, null));
        subActivity.initialize(this, context);
    }

    public void logOut() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    public ViewGroupOverlay getOverlay() {
        return allContent.getOverlay();
    }
}
