package be.invy.android.pref;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import be.invy.android.log.LogIds;

public final class UserRecord implements BaseTable {

    private static final String ID = LogIds.of(UserRecord.class);

    public static final String TABLE_NAME = "user";

    public static final String COLUMN_NAME_EMAIL = "email";
    // if password not null, auto-login is on
    public static final String COLUMN_NAME_LOGIN_COUNT = "logins";
    public static final String COLUMN_NAME_LAST_LOGIN = "lastLogin";

    public static final String[] COLUMNS_ALL = {
            COLUMN_NAME_EMAIL,
            COLUMN_NAME_LOGIN_COUNT,
            COLUMN_NAME_LAST_LOGIN
    };
    public static final int COLUMNS_ALL_INDEX_EMAIL = 0;
    public static final int COLUMNS_ALL_INDEX_LOGIN_COUNT = 1;
    public static final int COLUMNS_ALL_INDEX_LAST_LOGIN = 2;

    public static final String SORT_BY_LAST_LOGIN = COLUMN_NAME_LAST_LOGIN + " DESC";

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_NAME_EMAIL + " TEXT UNIQUE NOT NULL, " +
                    COLUMN_NAME_LOGIN_COUNT + " INT DEFAULT 0, " +
                    COLUMN_NAME_LAST_LOGIN + " DATETIME DEFAULT (strftime('%s','now'))" +
            ")";
//    public static final String SQL_CREATE_INDEX = "CREATE INDEX " + COLUMN_NAME_EMAIL
//            + "Index ON " + TABLE_NAME + " (" + COLUMN_NAME_EMAIL + ")";
    public static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static boolean insert(SQLiteDatabase database, UserRecord userRecord) {
        long result = database.insert(TABLE_NAME, null, userRecord.values);
        boolean success = result != -1;
        Log.d(ID, "Inserting [" + userRecord + "], " + (success ? "success" : "failure"));
        return success;
    }

    public static boolean update(SQLiteDatabase database, UserRecord userRecord) {
        long result = database.update(TABLE_NAME, userRecord.values,
                COLUMN_NAME_EMAIL + "=?", new String[]{userRecord.getEmail()});
        boolean success = result != -1;
        Log.d(ID, "Updating [" + userRecord + "], " + (success ? "success" : "failure"));
        return success;
    }

    public static boolean delete(SQLiteDatabase database, UserRecord userRecord) {
        long result = database.delete(TABLE_NAME,
                COLUMN_NAME_EMAIL + "=?", new String[]{userRecord.getEmail()});
        boolean success = result != -1;
        Log.d(ID, "Deleting [" + userRecord + "], " + (success ? "success" : "failure"));
        return success;
    }

    public static FullSelectResult fullSelect(SQLiteDatabase database) {
        Map<String, UserRecord> users = new HashMap<>();
        List<String> emails = new ArrayList<>();
        Cursor cursor = database.query(
                UserRecord.TABLE_NAME,
                UserRecord.COLUMNS_ALL,
                UserRecord.SELECTION_NONE,
                UserRecord.COLUMNS_NONE,
                null,
                null,
                UserRecord.SORT_BY_LAST_LOGIN);
        try {
            for (boolean hasNext = cursor.moveToFirst(); hasNext; hasNext = cursor.moveToNext()) {
                UserRecord user = new UserRecord()
                        .withEmail(cursor.getString(UserRecord.COLUMNS_ALL_INDEX_EMAIL))
                        .withLoginCount(cursor.getInt(UserRecord.COLUMNS_ALL_INDEX_LOGIN_COUNT))
                        .withLastLogin(cursor.getInt(UserRecord.COLUMNS_ALL_INDEX_LAST_LOGIN));
                users.put(user.getEmail(), user);
                emails.add(user.getEmail());
            }
        } finally {
            cursor.close();
        }
        return new FullSelectResult(users, emails);
    }

    public static final class FullSelectResult {
        public final Map<String, UserRecord> users;
        public final List<String> sortedEmails;

        private FullSelectResult(Map<String, UserRecord> users, List<String> emails) {
            this.users = users;
            this.sortedEmails = emails;
        }
    }

    private final ContentValues values = new ContentValues(3);

    public UserRecord() {}

    public String getEmail() {
        return values.getAsString(COLUMN_NAME_EMAIL);
    }

    public int getLoginCount() {
        return values.getAsInteger(COLUMN_NAME_LOGIN_COUNT);
    }

    public int getLastLogin() {
        return values.getAsInteger(COLUMN_NAME_LAST_LOGIN);
    }

    public UserRecord withEmail(String email) {
        values.put(COLUMN_NAME_EMAIL, email);
        return this;
    }

    public UserRecord withLoginCount(int loginCount) {
        values.put(COLUMN_NAME_LOGIN_COUNT, loginCount);
        return this;
    }

    public UserRecord withLastLogin(int lastLogin) {
        values.put(COLUMN_NAME_LAST_LOGIN, lastLogin);
        return this;
    }

    @Override
    public String toString() {
        return UserRecord.class.getSimpleName() + "{" + values + "}";
    }
}
