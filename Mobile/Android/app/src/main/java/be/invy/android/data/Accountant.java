package be.invy.android.data;

public class Accountant extends Entity {

    public Accountant(int entityId, int entityIdInType, String name) {
        super(entityId, entityIdInType, EntityType.ACCOUNTANT, name);
    }
}
