module AccountantsHelper
  
  private
  def classic_account?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("classic_id > ?", 0).exists?
      true
    else
      false
    end
  end
end
