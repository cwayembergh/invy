module ClassicPagesHelper
  
  private
  def other_accounts_than_classic?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("corporate_id > ?", 0).exists? || accounts.where("accountant_id > ?", 0).exists?
      true
    else
      false
    end
  end
  
  def corporate_account?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("corporate_id > ?", 0).exists?
      true
    else
      false
    end
  end
  
  def accountant_account?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("accountant_id > ?", 0).exists?
      true
    else
      false
    end
  end
end
