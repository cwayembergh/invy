module SessionsHelper
  protected 
  def current_user
    @current_user ||= User.find(session[:invy]) if session[:invy]
  end
end
