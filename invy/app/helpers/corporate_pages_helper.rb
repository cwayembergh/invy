module CorporatePagesHelper
  
  private
  def other_accounts_than_corporate?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("classic_id > ?", 0).exists? || accounts.where("accountant_id > ?", 0).exists?
      true
    else
      false
    end
  end
  
  def classic_account?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("classic_id > ?", 0).exists?
      true
    else
      false
    end
  end
  
  def accountant_account?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("accountant_id > ?", 0).exists?
      true
    else
      false
    end
  end
end
