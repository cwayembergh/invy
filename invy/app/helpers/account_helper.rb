module AccountHelper
  
  def current_corporate_account
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    accounts.where("corporate_id > ?", 0)
  end
  
  def current_classic_account
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    accounts.where("classic_id > ?", 0)
  end
  
  def classic?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("classic_id > ?", 0).exists?
      classic_id = accounts.where("classic_id > ?", 0).take.classic_id
      if Classic.where(id: classic_id).take.default_account == true
        true
      else 
        false
      end
    else
      false
    end
  end

  def corporate?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("corporate_id > ?", 0).exists?
      @corporate_id = accounts.where("corporate_id > ?", 0).take.corporate_id
      if Corporate.where(id: @corporate_id).take.default_account == true
        true
      else 
        false
      end
    else
      false
    end
  end

  def accountant?
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    if accounts.where("accountant_id > ?", 0).exists?
      accountant_id = accounts.where("accountant_id > ?", 0).take.accountant_id
      if Accountant.where(id: accountant_id).take.default_account == true
        true
      else 
        false
      end
    else
      false
    end
  end
end
