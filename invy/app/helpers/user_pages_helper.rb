module UserPagesHelper
  
  protected
  # Returns if an account (classic, corporate, accountant) has been added to the profile or not
  def account_added?(type)
    if UserAccount.where(user_id: current_user.id).exists?
      accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
      case type
      when "classic"
        accounts.where("classic_id > ?", 0).exists?
      when "corporate"
        accounts.where("corporate_id > ?", 0).exists?
      when "accountant"
        accounts.where("accountant_id > ?", 0).exists?
      end
    else
      false
    end
  end
  
  # Returns the date when the account has been added
  def account_added_date(type)
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    accounts.each do |account|
      if account.classic_id > 0
        @classic_id = account.classic_id
      end
      if account.corporate_id > 0
        @corporate_id = account.corporate_id
      end
      if account.accountant_id > 0
        @accountant_id = account.accountant_id
      end
    end
    
    case type
    when "classic"
      Classic.where(id: @classic_id).take.created_at.strftime("%d/%m/%Y")
    when "corporate"
      Corporate.where(id: @corporate_id).take.created_at.strftime("%d/%m/%Y")
    when "accountant"
      Accountant.where(id: @accountant_id).take.created_at.strftime("%d/%m/%Y")
    end
  end
end
