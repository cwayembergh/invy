class InvoicePdf < Prawn::Document
  
  include ActionView::Helpers::NumberHelper
  
  def initialize(invoice)
    super(top_margin: 70)
    @invoice = invoice
    @ledger_lines = LedgerLine.where(ledger_item_id: @invoice.id)
    
    if @invoice.currency == "EUR"
      @currency = "€"
    elsif @invoice.currency == "USD"
      @currency = "$"
    end
    
    font_families.update( "OpenSans" => { :normal => "#{Rails.root}/app/assets/fonts/OpenSans-Regular.ttf", :bold => "#{Rails.root}/app/assets/fonts/OpenSans-Bold.ttf"})
    font "OpenSans"
    default_leading 3
    
    
    #SENDER
    @sender_account = Account.where(id: @invoice.sender_id).first
    @sender_user_account = UserAccount.where(account_id: @sender_account).first
    @sender = User.where(id: @sender_user_account.user_id).first
    
    #RECEIVER
    @receiver_account = Account.where(id: @invoice.receiver_id).first
    @receiver_user_account = UserAccount.where(account_id: @receiver_account).first
    @receiver = User.where(id: @receiver_user_account.user_id).first
    
    
    
    
    
    header
    ledger_line
    notes_and_total_price
    repeat :all do
      footer
    end
  end
 
  
  def header
    y_position = cursor
    bounding_box([0, y_position], :width => 180) do
      if @sender_account.corporate.company_image.present?
        image "#{@sender_account.corporate.company_image.path()}", :height => 35
      end
      move_down 10
      
      text "#{@sender_account.corporate.company_name}", size: 14, style: :bold
      move_down 5
      text "#{@sender_account.corporate.company_street}, #{@sender_account.corporate.company_street_number}", size: 8
      text "#{@sender_account.corporate.company_city}, #{@sender_account.corporate.company_postal_code}", size: 8
      text "Phone: #{@sender_account.corporate.company_phone}", size: 8
      text "Email: #{@sender.emails.take.email}", size: 8
      text "TVA: #{@sender_account.corporate.tva_number}", size: 8

    end
    
    bounding_box([300, y_position], :width => 260) do
      text_box "#{(@invoice.ledger_item_type).upcase}", size: 14, style: :bold, :at => [102,0]
      move_down 15
      
      #ADD IF RECEIVER IS A COMPANY (DISPLAY COMPANY NAME, ...)
      if @invoice.ledger_status == "paid"
        
        if @receiver_account['classic_id'] > 0
          data = [["Bill to:", "#{@receiver.first_name} #{@receiver.last_name}"],
                  ["", "#{@receiver_account.classic.classic_street}, #{@receiver_account.classic.classic_street_number}"],
                  ["", "#{@receiver_account.classic.classic_city}, #{@receiver_account.classic.classic_postal_code}"],
                  ["", "#{@receiver_account.classic.classic_phone}"],
                  ["", "#{@receiver.emails.take.email}"],
                  ["Invoice #:", "#{@invoice.ref_number}"],
                  ["Issue Date:", "#{@invoice.issue_date.strftime("%d/%m/%Y")}"],
                  ["Due Date:", "#{@invoice.due_date.strftime("%d/%m/%Y")}"],
                  ["Payment Date:", "#{@invoice.payment_date.strftime("%d/%m/%Y")}"],
                  ["Payment Method:", "#{@invoice.payment_method}"],
                  ["Account Number:", "#{@sender_account.corporate.bank_account}"]]
        else
          data = [["Bill to:", "#{@receiver_account.corporate.company_name}"],
                  ["", "#{@receiver_account.corporate.company_street}, #{@receiver_account.corporate.company_street_number}"],
                  ["", "#{@receiver_account.corporate.company_city}, #{@receiver_account.corporate.company_postal_code}"],
                  ["", "#{@receiver_account.corporate.company_phone}"],
                  ["", "#{@receiver.emails.take.email}"],
                  ["Invoice #:", "#{@invoice.ref_number}"],
                  ["Issue Date:", "#{@invoice.issue_date.strftime("%d/%m/%Y")}"],
                  ["Due Date:", "#{@invoice.due_date.strftime("%d/%m/%Y")}"],
                  ["Payment Date:", "payment"],
                  ["Payment Method:", "method"],
                  ["Account Number:", "#{@sender_account.corporate.bank_account}"]]
        
        end
        
      else
        
        if @receiver_account['classic_id'] > 0
          data = [["Bill to:", "#{@receiver.first_name} #{@receiver.last_name}"],
                  ["", "#{@receiver_account.classic.classic_street}, #{@receiver_account.classic.classic_street_number}"],
                  ["", "#{@receiver_account.classic.classic_city}, #{@receiver_account.classic.classic_postal_code}"],
                  ["", "#{@receiver_account.classic.classic_phone}"],
                  ["", "#{@receiver.emails.take.email}"],
                  ["Invoice #:", "#{@invoice.ref_number}"],
                  ["Issue Date:", "#{@invoice.issue_date.strftime("%d/%m/%Y")}"],
                  ["Due Date:", "#{@invoice.due_date.strftime("%d/%m/%Y")}"],
                  ["Account Number:", "#{@sender_account.corporate.bank_account}"]]
        else
          data = [["Bill to:", "#{@receiver_account.corporate.company_name}"],
                  ["", "#{@receiver_account.corporate.company_street}, #{@receiver_account.corporate.company_street_number}"],
                  ["", "#{@receiver_account.corporate.company_city}, #{@receiver_account.corporate.company_postal_code}"],
                  ["", "#{@receiver_account.corporate.company_phone}"],
                  ["", "#{@receiver.emails.take.email}"],
                  ["Invoice #:", "#{@invoice.ref_number}"],
                  ["Issue Date:", "#{@invoice.issue_date.strftime("%d/%m/%Y")}"],
                  ["Due Date:", "#{@invoice.due_date.strftime("%d/%m/%Y")}"],
                  ["Account Number:", "#{@sender_account.corporate.bank_account}"]]
        end
      end
      table(data, :column_widths => [100, 160], :cell_style => {:borders => [], :size => 8, :padding => [3,0,0,3]}) do
              column(0).font_style = :bold
      end
      move_down 80
    end
  end
  
  def ledger_line
    move_down 20
    table ledger_line_rows, :column_widths => [135, 135, 50, 50, 50, 50, 70], :cell_style => {:size => 8, :border_color => "aaaaaa", :border_width => 0.1} do
      row(0).font_style = :bold
      row(0).background_color = 'f5f5f5'
      column(2).align = :center
      column(3).align = :center
      column(4).align = :center
      column(5..6).align = :center
      row(0).column(2..6).align = :center
      self.header = true
    end
  end
  
  def ledger_line_rows
    [["Product", "Description", "Quantity", "Unit Price", "Tax %", "Total", "Total (tax incl.)"]]+
    @ledger_lines.map do |line|
      [line.product_name, line.product_description, line.quantity, "#{number_to_currency(line.unit_price, unit: "€", separator: ",", delimiter: ".", precision: 2)}", line.tax_rate, "#{number_to_currency(line.total_price, unit: "€", separator: ",", delimiter: ".", precision: 2)}", "#{number_to_currency(line.total_price_with_taxes, unit: "€", separator: ",", delimiter: ".", precision: 2)}"]
    end
  end
  
  def notes_and_total_price
    move_down 15
    y_position = cursor
    bounding_box([0, y_position], :width =>320, :height => 100) do
      stroke_bounds
      bounding_box([5, cursor - 5], :width => 310) do
        text "Special Notes and Instructions:", size: 9, style: :bold
        move_down 10
        text "#{@invoice.communication}", size: 8
      end
      
     
    end
    bounding_box([370, y_position], :width => 180) do
      data = [["Total (tax excl.):", "#{number_to_currency(@invoice.total_without_taxes, unit: "€", separator: ",", delimiter: ".", precision: 2)}"],
              ["Total tax:", "#{number_to_currency(@invoice.total_taxes, unit: "€", separator: ",", delimiter: ".", precision: 2)}"],
              ["Total (tax incl.):", "#{number_to_currency(@invoice.total_with_taxes, unit: "€", separator: ",", delimiter: ".", precision: 2)}"]]
      table(data, :column_widths => [100, 70], :cell_style => {:size => 8}) do
        column(1).align = :right
        row(2).background_color = 'd9edf7'
        row(2).column(1).font_style = :bold
      end
    end
  end
  
  def footer
    bounding_box [bounds.left, bounds.bottom + 50], width: bounds.width do
      text "Thank you for using Invy!", size: 8, align: :center, style: :bold
      text "Should you have any enquiries concerning this invoice, please contact John Doe at www.invy.be", size: 8, align: :center
      stroke do
        horizontal_rule
        move_down 5
      end
      text "Invy - 119 Boulevard Général Jacques, 1050, Ixelles", size: 8, align: :center
      text "Tel: 02651687 - Fax: 02651786 - Email: info@invy.be - Web: www.invy.be", size: 8, align: :center
    end
  end
  
end