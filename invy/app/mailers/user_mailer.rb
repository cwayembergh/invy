class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.password_reset.subject
  #
  def password_reset(user)
    @user = user
    @user_email = Email.where(user_id: user).take.email
    mail :to => @user_email, :subject => "Password Reset"
  end
  
  def activate_account(user)
    @user = user
    @user_email = Email.where(user_id: user).take.email
    mail :to => @user_email, :subject => "Account activation instructions"
  end
  
  def send_invoice_and_invitation(user, hash)
    @user = user
    @password = hash
    @user_email = Email.where(user_id: user).take.email
    mail :to => @user_email, :subject => "You have a received a new invoice"
  end
end
