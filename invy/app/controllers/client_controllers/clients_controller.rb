class ClientControllers::ClientsController < ApplicationController
  include AccountHelper
  
  layout 'dashboards/dashboard_corporate'
  
  def index 
    @corporate_account = current_corporate_account.first.corporate
    @clients = Client.where(id: @corporate_account.list_clients.map(&:client_id))
    @classic_clients = @clients.where(company_name: nil)
    @corporate_clients = @clients.where.not(company_name: nil)
    @new_client = Client.new
  end
  
  
  def new
    @new_client = Client.new
  end
  
  def create
    @new_client = Client.new(client_params)
    @list = ListClient.new
    @list.corporate_id = current_corporate_account.first.corporate.id
    respond_to do |format|
      # CLASSIC CLIENT
      if @new_client.company_name.blank?
        if @new_client.save
          @list.client_id = @new_client.id
          @list.save
          format.html {redirect_to myclients_path}
          format.js   {render action: 'add_classic_client'}
        else
          format.html {render action: 'new'}
          format.js {render json: @new_client.errors}
        end
      else
        # CORPORATE CLIENT
        if @new_client.save
          @list.client_id = @new_client.id
          @list.save
          format.html {redirect_to myclients_path}
          format.js   {render action: 'add_corporate_client'}
        else
          format.html {render action: 'new'}
          format.js {render json: @new_client.errors}
        end
      end
    end
  end
 
  private
  def client_params
    params.require(:client).permit(:email, :first_name, :last_name, :address_number, :postal_code_city, :country, :phone_number, :mobile_number, :fax_number, :website, :company_name)
  end
  
end
