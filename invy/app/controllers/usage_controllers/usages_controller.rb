class UsageControllers::UsagesController < ApplicationController
  include AccountHelper
  
  layout :resolve_layout
  
  
  def usage
    if classic?
      @current_classic = current_classic_account.first
      
      #THIS MONTH
      if Rails.env.development?
        @received_ledger_list_year = @current_classic.ledger_items_as_receiver.where("strftime('%Y', created_at)= ?", Date.today.strftime("%Y"))
        @received_ledger_list = @current_classic.ledger_items_as_receiver.where("strftime('%m', created_at)= ?", Date.today.strftime("%m"))
      else
        @received_ledger_list_year = @current_classic.ledger_items_as_receiver.where("extract(year from created_at)=?", Date.today.strftime("%Y"))
        @received_ledger_list = @current_classic.ledger_items_as_receiver.where("extract(month from created_at)=?", Date.today.strftime("%m"))
      end
     
      @paid_ledger_list = @received_ledger_list.where(ledger_status: 'paid')
      @expenses_this_month = @received_ledger_list.sum :total_with_taxes
      @expenses_this_year = @received_ledger_list_year.sum :total_with_taxes
    end
    
    if corporate?
      @current_corporate = current_corporate_account.first  
      @clients = Client.where(id: @current_corporate.corporate.list_clients.map(&:client_id))
      
      #THIS MONTH
      if Rails.env.development?
        @sent_ledger_list = @current_corporate.ledger_items_as_sender.where("strftime('%m', created_at)= ?", Date.today.strftime("%m"))
        @received_ledger_list = @current_corporate.ledger_items_as_receiver.where("strftime('%m', created_at)= ?", Date.today.strftime("%m"))
        @clients_this_month = @clients.where("strftime('%m', created_at)= ?", Date.today.strftime("%m"))
      else
        @sent_ledger_list = @current_corporate.ledger_items_as_sender.where("extract(year from created_at)=?", Date.today.strftime("%m"))
        @received_ledger_list = @current_corporate.ledger_items_as_receiver.where("extract(month from created_at)=?", Date.today.strftime("%m"))
        @clients_this_month = @clients.where("extract(month from created_at)=?", Date.today.strftime("%m"))
      end
      
      @paid_ledger_list = @sent_ledger_list.where(ledger_status: 'paid')
      @gross_volume_value_this_month = @sent_ledger_list.sum :total_with_taxes
    end
  end
  
  protected
  def resolve_layout
    case action_name
    when "usage"
      if classic?
        'dashboards/dashboard_classic'
      elsif corporate?
        'dashboards/dashboard_corporate'
      elsif accountant?
        'dashboards/dashboard_accountant'
      end
    end
  end
end
