class AccountPagesControllers::ClassicPagesController < ApplicationController
  include SessionsHelper
  include AccountHelper
  
  layout 'dashboards/dashboard_classic'
  
  def dashboard
    @current_classic = current_classic_account.first
    
    if Rails.env.development?
      @received_ledger_list = @current_classic.ledger_items_as_receiver.order("created_at DESC").where("strftime('%m', created_at)= ?", Date.today.strftime("%m"))
    else
      @received_ledger_list = @current_classic.ledger_items_as_receiver.order("created_at DESC").where("extract(month from created_at)=?", Date.today.strftime("%m"))
    end
    
    @new_invoices_list = @received_ledger_list.where(ledger_status: "new").order("created_at DESC")
    @paid_ledger_list = @received_ledger_list.where(ledger_status: "paid").order("created_at DESC")
    @open_amount = @received_ledger_list.sum :total_with_taxes
    @paid_amount = @paid_ledger_list.sum :total_with_taxes
  end
  
  
  
end
