class AccountPagesControllers::CorporatePagesController < ApplicationController
  include SessionsHelper
  include AccountHelper
  
  layout 'dashboards/dashboard_corporate'
  
  def dashboard
    @current_corporate = current_corporate_account.first
    @clients_list = @current_corporate.corporate.list_clients
    @clients = Client.where(id: @clients_list.map(&:client_id))
    
    if Rails.env.development?
      @sent_ledger_list = @current_corporate.ledger_items_as_sender.where("strftime('%m', created_at)= ?", Date.today.strftime("%m")).order("created_at DESC")
      @received_ledger_list = @current_corporate.ledger_items_as_receiver.where("strftime('%m', created_at)= ?", Date.today.strftime("%m")).order("created_at DESC")
    else
      @sent_ledger_list = @current_corporate.ledger_items_as_sender.where("extract(month from created_at)=?", Date.today.strftime("%m")).order("created_at DESC")
      @received_ledger_list = @current_corporate.ledger_items_as_receiver.where("extract(month from created_at)=?", Date.today.strftime("%m")).order("created_at DESC")
    end
    
    @paid_received_ledger_list = @received_ledger_list.where(ledger_status: "paid")
    @due_amount = @sent_ledger_list.where(ledger_status: "new").sum :total_with_taxes
    @open_amount = @received_ledger_list.sum :total_with_taxes
    @paid_amount = @received_ledger_list.where(ledger_status: "paid").sum :total_with_taxes
  end
  
end
