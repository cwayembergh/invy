class AccountPagesControllers::AccountPagesController < ApplicationController
  
  def add
  end
  
  def switch_to_classic
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    classic_id = accounts.where("classic_id > ?", 0).take.classic_id
    classic_account = Classic.where(id: classic_id).first
    classic_account.update_attribute(:default_account, true)
    if accounts.where("corporate_id > ?", 0).exists?
      corporate_id = accounts.where("corporate_id > ?", 0).take.corporate_id
      corporate_account = Corporate.where(id: corporate_id).first
      corporate_account.update_attribute(:default_account, false)
    end
    if accounts.where("accountant_id > ?", 0).exists?
      accountant_id = accounts.where("accountant_id > ?", 0).take.accountant_id
      accountant_account = Accountant.where(id: accountant_id).first
      accountant_account.update_attribute(:default_account, false)
    end
    redirect_to root_path
  end
  
  def switch_to_corporate
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    corporate_id = accounts.where("corporate_id > ?", 0).take.corporate_id
    corporate_account = Corporate.where(id: corporate_id).first
    corporate_account.update_attribute(:default_account, true)
    if accounts.where("classic_id > ?", 0).exists?
      classic_id = accounts.where("classic_id > ?", 0).take.classic_id
      classic_account = Classic.where(id: classic_id).first
      classic_account.update_attribute(:default_account, false)
    end
    if accounts.where("accountant_id > ?", 0).exists?
      accountant_id = accounts.where("accountant_id > ?", 0).take.accountant_id
      accountant_account = Accountant.where(id: accountant_id).first
      accountant_account.update_attribute(:default_account, false)
    end
    redirect_to root_path
  end
  
  def switch_to_accountant
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    accountant_id = accounts.where("accountant_id > ?", 0).take.accountant_id
    accountant_account = Accountant.where(id: accountant_id).first
    accountant_account.update_attribute(:default_account, true)
    if accounts.where("classic_id > ?", 0).exists?
      classic_id = accounts.where("classic_id > ?", 0).take.classic_id
      classic_account = Classic.where(id: classic_id).first
      classic_account.update_attribute(:default_account, false)
    end
    if accounts.where("corporate_id > ?", 0).exists?
      corporate_id = accounts.where("corporate_id > ?", 0).take.corporate_id
      corporate_account = Corporate.where(id: corporate_id).first
      corporate_account.update_attribute(:default_account, false)
    end
    redirect_to root_path
  end
  
  def account_dispatcher
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    accounts.each do |account|
      if account.classic_id.present? && Classic.where(id: account.classic_id).take.default_account == true
        redirect_to classic_account_path
      elsif account.corporate_id.present? && Corporate.where(id: account.corporate_id).take.default_account == true
         redirect_to corporate_account_path
      elsif account.accountant_id.present? && Accountant.where(id: account.accountant_id).take.default_account == true
        redirect_to accountant_account_path
      end
    end
  end
end
