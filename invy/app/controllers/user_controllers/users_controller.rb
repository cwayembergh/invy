class UserControllers::UsersController < ApplicationController
  include AccountHelper
  before_action :set_user, only: [:show, :edit, :update, :destroy]
    
  layout :resolve_layout
  
  def index
    @users = User.all
  end


  def show
  end


  def new
    @user = User.new
    @user.emails.build
  end

  def edit
    @user = User.find(params[:id])
  end


  def create
    @user = User.new(user_params)
    if @user.save
      #create blank account & user_account
      account = Account.create
      UserAccount.create(user_id: @user.id, account_id: account.id)
      
      @user.send_activate_account
      redirect_to verification_path, flash: {email: @user.emails.take.email}
    else
      render :new
    end
  end
  
  def update
    @user = User.find(params[:id])
    
    respond_to do |format|
      if @user.update_attributes(user_update_params)
        format.html {redirect_to profile_path}
        format.js {render :js => "window.location.href='"+profile_path+"'"}
      else
        format.js {render json: @user.errors}
      end
    end
  end

  def verification
  end

  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :password, :password_confirmation, :confirmation_token, :confirmation_sent_at, :profile_picture, emails_attributes: [:email])
    end
    
    def user_update_params 
      params.require(:user).permit(:first_name, :last_name, :current_password, :password, :password_confirmation, :profile_picture, emails_attributes: [:email])
    end
    
    
    def resolve_layout
      case action_name
      when "new", "create", "verification"
        'layouts/static'
      when "profile", "update"
        if classic?
          'dashboards/dashboard_classic'
        elsif corporate?
          'dashboards/dashboard_corporate'
        elsif accountant?
          'dashboards/dashboard_accountant'
        end
      end
    end
end
