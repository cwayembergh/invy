class UserControllers::UserPagesController < ApplicationController
  include AccountHelper
  layout :resolve_layout
  
  def dashboard
  end
  
  def profile
    @user = current_user
    if corporate?
      @corporate_account = current_corporate_account.first
      @corporate = Corporate.where(id: @corporate_account.corporate_id).first
      @clients_list = @corporate.list_clients
      @clients = Client.where(id: @clients_list.map(&:client_id))
    end
     
    if classic?
      @classic_account = current_classic_account.first
      @classic = Classic.where(id: @classic_account.classic_id).first
    end
      
    if accountant?
      @accountant_account = current_accountant_account.first
      @accountant = Accountant.where(id: @accountant_account.accountant_id).first
    end
  end
  
  protected
  def resolve_layout
    case action_name
    when "dashboard"
      'dashboards/dashboard_new'
      
    when "profile"
      if classic?
        'dashboards/dashboard_classic'
      elsif corporate?
        'dashboards/dashboard_corporate'
      elsif accountant?
        'dashboards/dashboard_accountant'
      end
    end
  end
end
