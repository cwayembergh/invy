class UserControllers::PasswordResetsController < ApplicationController  
  layout 'static'
  
  def new
  end
  
  def create
    if Email.find_by_email(params[:email])
      user = Email.find_by_email(params[:email]).user
      user.send_password_reset if user
      flash[:notice] = "Email sent with reset instructions."
      redirect_to reset_password_path
    else
      flash[:error] = "There was a problem with the sending."
      redirect_to reset_password_path
    end
  end
  
  def edit
    @user = User.find_by_reset_password_token!(params[:id])
  end
  
  def update
    @user = User.find_by_reset_password_token!(params[:id])
    if @user.reset_password_sent_at < 2.hours.ago
      flash[:error] = "Password reset has expired."
      redirect_to new_password_reset_path
      
    elsif @user.update_attributes(params.require(:user).permit(:password, :password_confirmation))
      flash[:notice] = "Password has been reset successfully."
      redirect_to login_path
    else
      render :edit
    end
  end
end
