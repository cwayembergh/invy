class UserControllers::SessionsController < ApplicationController
  
  layout 'static'
  
  def new
  end

  def create
    if Email.find_by_email(params[:email])
      user = Email.find_by_email(params[:email]).user
      if user && user.authenticate(params[:password])
        if user.activated?
          reset_session
          session[:invy] = user.id
          redirect_to root_path
        else
          flash[:error] = "Account not activated yet.<br>Check your emails for the activation link.<br>Click "+view_context.link_to("here", resend_activation_path)+" to send new activation mail."
          redirect_to login_path
        end
      else
        flash[:error] = "There was a problem with your login."
        redirect_to login_path
      end
    else
      flash[:error] = "There was a problem with your login."
      redirect_to login_path
    end
  end

  def destroy
    session[:invy] = nil
    redirect_to root_url
  end
end
