class UserControllers::AccountActivationsController < ApplicationController
  
  layout 'static'
  
  def new
  end
  
  def create
    if Email.find_by_email(params[:email])
      user = Email.find_by_email(params[:email]).user
      if user
        if !user.activated?
          user.send_activate_account
          flash[:notice] = "Email sent with reset instructions."
          redirect_to resend_activation_path
        else
          flash[:error] = "This account has already been activated."
          redirect_to resend_activation_path
        end
      else
        flash[:error] = "There was a problem with the sending."
        redirect_to resend_activation_path
      end
    else
      flash[:error] = "There was a problem with the sending."
      redirect_to resend_activation_path
    end
  end
  

  def edit
    if Email.find_by_email(params[:email])
      user = Email.find_by_email(params[:email]).user
        
      if user && !user.activated? && user.correct_confirmed?(params[:id])
        user.activate_account
        flash[:notice] = "Your account has been successfully activated."
        redirect_to login_path
      else
        flash[:error] = "Invalid activation link."
        redirect_to root_path
      end
    else
      flash[:error] = "Invalid activation link."
      redirect_to root_path
    end
  end
  
end
