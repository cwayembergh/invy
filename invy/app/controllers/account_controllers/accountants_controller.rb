class AccountControllers::AccountantsController < ApplicationController
  include SessionsHelper
  before_action :set_accountant, only: [:show, :edit, :update, :destroy]

  layout 'dashboards/dashboard_new'

  # GET /accountants/new
  def new
    @accountant = Accountant.new
  end

  # GET /accountants/1/edit
  def edit
  end

  # POST /accountants
  # POST /accountants.json
  def create
    @accountant = Accountant.new(accountant_params)
    @accountant.default_account = true
    
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    
    #If the user already has a accountant account
    if accounts.where("accountant_id > ?", 0).exists?
      flash[:error] = "An accountant account has already been added to your profile."
      redirect_to add_corporate_path
    
    #Else create a new accountant account
    else
      if @accountant.save
        account = accounts.first
        account.update(accountant_id: @accountant.id)
      
        #SET DEFAULTS OF CLASSIC AND ACCOUNTANT TO FALSE IF EXIST
        if accounts.where("classic_id > ?", 0).exists?
          classic_id = accounts.where("classic_id > ?", 0).take.classic_id
          classic_account = Classic.where(id: classic_id).first
          classic_account.update_attribute(:default_account, false)
        end
        if accounts.where("corporate_id > ?", 0).exists?
          corporate_id = accounts.where("corporate_id > ?", 0).take.corporate_id
          corporate_account = Corporate.where(id: corporate_id).first
          corporate_account.update_attribute(:default_account, false)
        end
        redirect_to root_path
      else
        render :new
      end
    end
  end

  # PATCH/PUT /accountants/1
  # PATCH/PUT /accountants/1.json
  def update
    respond_to do |format|
      if @accountant.update(accountant_params)
        format.html { redirect_to @accountant, notice: 'Accountant was successfully updated.' }
        format.json { render :show, status: :ok, location: @accountant }
      else
        format.html { render :edit }
        format.json { render json: @accountant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /accountants/1
  # DELETE /accountants/1.json
  def destroy
    @accountant.destroy
    respond_to do |format|
      format.html { redirect_to accountants_url, notice: 'Accountant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_accountant
      @accountant = Accountant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def accountant_params
      params.require(:accountant).permit(:bank_account)
    end
end
