class AccountControllers::CorporatesController < ApplicationController
  include SessionsHelper
  before_action :set_corporate, only: [:show, :edit, :update, :destroy]

  layout 'dashboards/dashboard_new'

  # GET /corporates/new
  def new
    @corporate = Corporate.new
  end

  # GET /corporates/1/edit
  def edit
    @corporate = Corporate.find(params[:id])
  end

  # POST /corporates
  # POST /corporates.json
  def create
    @corporate = Corporate.new(corporate_params)
    @corporate.default_account = true
    
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    
    #If the user already has a classic account
    if accounts.where("corporate_id > ?", 0).exists?
      flash[:error] = "A corporate account has already been added to your profile."
      redirect_to add_corporate_path
    
    #Else create a new corporate account
    else
      if @corporate.save
        account = accounts.first
        account.update(corporate_id: @corporate.id)
      
        #SET DEFAULTS OF CLASSIC AND ACCOUNTANT TO FALSE IF EXIST
        if accounts.where("classic_id > ?", 0).exists?
          classic_id = accounts.where("classic_id > ?", 0).take.classic_id
          classic_account = Classic.where(id: classic_id).first
          classic_account.update_attribute(:default_account, false)
        end
        if accounts.where("accountant_id > ?", 0).exists?
          accountant_id = accounts.where("accountant_id > ?", 0).take.accountant_id
          accountant_account = Accountant.where(id: accountant_id).first
          accountant_account.update_attribute(:default_account, false)
        end
        redirect_to root_path
      else
        render :new
      end
    end
  end


  # PATCH/PUT /corporates/1
  # PATCH/PUT /corporates/1.json
  def update    
    @corporate = Corporate.find(params[:id])
    
    respond_to do |format|
      if @corporate.update_attributes(corporate_update_params)
        format.html {redirect_to profile_path}
        format.js {render :js => "window.location.href='"+profile_path+"'"}
      else
        format.js {render json: @corporate.errors}
      end
    end
  end

  # DELETE /corporates/1
  # DELETE /corporates/1.json
  def destroy
    @corporate.destroy
    respond_to do |format|
      format.html { redirect_to corporates_url, notice: 'Corporate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_corporate
      @corporate = Corporate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def corporate_params
      params.require(:corporate).permit(:bank_account, :company_name, :company_phone, :company_street, :company_street_number, :company_postal_code, :company_city, :tva_number, :company_image)
    end
    
    def corporate_update_params
      params.require(:corporate).permit(:bank_account, :company_name, :company_phone, :company_street, :company_street_number, :company_postal_code, :company_city, :tva_number, :company_image, :current_corporate_password)
    end
end
