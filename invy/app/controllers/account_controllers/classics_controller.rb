class AccountControllers::ClassicsController < ApplicationController
  include SessionsHelper
  before_action :set_classic, only: [:show, :edit, :update, :destroy]
  
  layout 'dashboards/dashboard_new'
  

  # GET /classics/new
  def new
    @classic = Classic.new
  end

  # GET /classics/1/edit
  def edit
  end

  # POST /classics
  # POST /classics.json
  def create
    @classic = Classic.new(classic_params)
    @classic.default_account = true
    
    accounts = Account.where(id: current_user.user_accounts.map(&:account_id))
    
    #If the user already has a classic account
    if accounts.where("classic_id > ?", 0).exists?
      flash[:error] = "A classic account has already been added to your profile."
      redirect_to add_classic_path
    
    #Else create a new classic account
    else
      if @classic.save
        account = accounts.first
        account.update(classic_id: @classic.id)
        
        #SET DEFAULTS OF CORPORATE AND ACCOUNTANT TO FALSE IF EXIST
        if accounts.where("corporate_id > ?", 0).exists?
          corporate_id = accounts.where("corporate_id > ?", 0).take.corporate_id
          corporate_account = Corporate.where(id: corporate_id).first
          corporate_account.update_attribute(:default_account, false)
        end
        if accounts.where("accountant_id > ?", 0).exists?
          accountant_id = accounts.where("accountant_id > ?", 0).take.accountant_id
          accountant_account = Accountant.where(id: accountant_id).first
          accountant_account.update_attribute(:default_account, false)
        end
        redirect_to root_path
      else
        render :new
      end
    end
  end

  # PATCH/PUT /classics/1
  # PATCH/PUT /classics/1.json
  def update
    respond_to do |format|
      if @classic.update(classic_params)
        format.html { redirect_to @classic, notice: 'Classic was successfully updated.' }
        format.json { render :show, status: :ok, location: @classic }
      else
        format.html { render :edit }
        format.json { render json: @classic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /classics/1
  # DELETE /classics/1.json
  def destroy
    @classic.destroy
    respond_to do |format|
      format.html { redirect_to classics_url, notice: 'Classic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_classic
      @classic = Classic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def classic_params
      params.require(:classic).permit(:bank_account, :classic_phone, :classic_street, :classic_street_number, :classic_postal_code, :classic_city)
    end
end
