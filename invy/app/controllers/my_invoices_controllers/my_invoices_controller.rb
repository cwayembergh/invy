class MyInvoicesControllers::MyInvoicesController < ApplicationController
  include AccountHelper
  layout :resolve_layout
  
  def invoices
    if classic?
      @current_classic = current_classic_account.first
      @sent_ledger_list = @current_classic.ledger_items_as_sender.order("created_at DESC")
      @received_ledger_list = @current_classic.ledger_items_as_receiver.order("created_at DESC")
    end
    
    if corporate?
      @current_corporate = current_corporate_account.first
      @sent_ledger_list = @current_corporate.ledger_items_as_sender.order("created_at DESC")
      @received_ledger_list = @current_corporate.ledger_items_as_receiver.order("created_at DESC")
    end
  end
  
  private
  def resolve_layout
    case action_name
    when "invoices"
      if classic?
        'dashboards/dashboard_classic'
      elsif corporate?
        'dashboards/dashboard_corporate'
      elsif accountant?
        'dashboards/dashboard_accountant'
      end
    end
  end
end
