class LedgerItemControllers::PaymentController < ApplicationController
  
  def pay
    invoice = LedgerItem.find_by_token!(params[:id])
    invoice.update_attribute(:payment_date, Time.now)
    invoice.update_attribute(:ledger_status, 'paid')
    invoice.update_attribute(:payment_method, 'visa')
    
    redirect_to root_path
  end
end
