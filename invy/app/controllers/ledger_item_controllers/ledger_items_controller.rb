class LedgerItemControllers::LedgerItemsController < ApplicationController
  include AccountHelper
  
  layout :resolve_layout
  
  def pay
    invoice = LedgerItem.find_by_token!(params[:id])
    invoice.update_attribute(:payment_date, Time.now)
    invoice.update_attribute(:ledger_status, 'paid')
    invoice.update_attribute(:payment_method, 'visa')
    redirect_to root_path
  end
  
  def new
    @sending_account = current_corporate_account.first
    @clients_list = @sending_account.corporate.list_clients
    @clients = Client.where(id: @clients_list.map(&:client_id))
    @new_invoice = LedgerItem.new
    @new_invoice.ledger_lines.build
    
    #creating the invoice Counter if not already created
    if !Counter.first.present?
      Counter.create
    end
    
    @ref_number = Counter.first.invoice_counter.next
  end
  
  def show
    @current_corporate 
    
    if corporate?
      @current_corporate = current_corporate_account.first
    end
    
    @invoice = LedgerItem.find_by_token!(params[:id])
    
    @sender_account = Account.where(id: @invoice.sender_id).first
    @sender_user_account = UserAccount.where(account_id: @sender_account).first
    @sender = User.where(id: @sender_user_account.user_id).first
    
    @receiver_account = Account.where(id: @invoice.receiver_id).first
    @receiver_user_account = UserAccount.where(account_id: @receiver_account).first
    @receiver = User.where(id: @receiver_user_account.user_id).first
    
    respond_to do |format|
      format.html
      format.pdf do
        pdf = InvoicePdf.new(@invoice)
        send_data pdf.render, filename: "invoice_#{@invoice.id}.pdf", type: "application/pdf", disposition: "inline"
      end
    end
  end
  
  def create
    @sending_account = current_corporate_account.first
    
    @clients_list = @sending_account.corporate.list_clients
    @clients = Client.where(id: @clients_list.map(&:client_id))
    
    @ref_number = Counter.first.invoice_counter.next
    
    @new_invoice = LedgerItem.new(ledger_item_params)
    @new_invoice.sender_id = @sending_account.id
    @new_invoice.issue_date = Time.now
    @new_invoice.token = SecureRandom.urlsafe_base64(16)
    @new_invoice.ref_number = @ref_number
    
    if @new_invoice.save
      Counter.first.update(invoice_counter: @ref_number)
      flash[:notice] = "The invoice has been successfully created."
      redirect_to create_invoice_path
    else
      render :new
    end
  end
  
  private
  def ledger_item_params
    params.require(:ledger_item).permit(:receiver_id, :due_date, :sender_ref_number, :currency, :total_taxes, :total_without_taxes, :total_with_taxes, :communication, :email,
                                        ledger_lines_attributes: [:id, :product_name, :product_description, :quantity, :unit_price, :tax_rate, :total_price, :total_price_with_taxes, :_destroy])
  end
  
  def resolve_layout
    case action_name
    when "new", "create"
      'dashboards/dashboard_corporate'
    when "show"
      if classic?
        'dashboards/dashboard_classic'
      elsif corporate?
        'dashboards/dashboard_corporate'
      elsif accountant?
        'dashboards/dashboard_accountant'
      end
    end
  end
end
