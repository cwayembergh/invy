class Accountant < ActiveRecord::Base
  has_one :account, dependent: :destroy
  
  validates :bank_account, presence: true
end
