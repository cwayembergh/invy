class Account < ActiveRecord::Base
  belongs_to :classic
  belongs_to :corporate
  belongs_to :accountant
  has_one :user_account
  has_many :ledger_items_as_sender, class_name: "LedgerItem", foreign_key: :sender_id
  has_many :ledger_items_as_receiver, class_name: "LedgerItem", foreign_key: :receiver_id
end
