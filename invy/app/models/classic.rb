class Classic < ActiveRecord::Base
  has_one :account, dependent: :destroy
  validates :classic_phone, presence: true
  validates :classic_street, presence: true
  validates :classic_street_number, presence: true
  validates :classic_postal_code, presence: true
  validates :classic_city, presence: true
  validates :bank_account, presence: true
end
