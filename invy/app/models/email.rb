class Email < ActiveRecord::Base
  belongs_to :user
  before_save :downcase_email
  
  validates :email, presence: true, 
                    uniqueness: true, 
                    format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i},
                    :on => :create
                    
  private
  def downcase_email
    self.email = self.email.downcase
  end
end
