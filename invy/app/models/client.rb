class Client < ActiveRecord::Base
  has_many :list_clients
  validates :email, presence: true, uniqueness: true, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates :first_name, :last_name, presence: true
  
  before_save :create_user
  
  
  def create_user
    if !Email.where(email: self.email.downcase).exists?
      hash = SecureRandom.hex(4)
      #create user
      user = User.new(first_name: self.first_name, last_name: self.last_name, password: hash, password_confirmation: hash)
      user.save(:validate => false)
      #create email
      Email.create(email: self.email.downcase, user_id: user.id)
      #create blank account
      account = Account.create
      #create user_account
      UserAccount.create(user_id: user.id, account_id: account.id)
    end
  end
end
