class LedgerLine < ActiveRecord::Base
  belongs_to :ledger_item
  
  validates :product_name, :product_description, :quantity, :unit_price, presence: true
  validates :quantity, numericality: {greater_than_or_equal_to: 1, only_integer: true}
  validates :unit_price, numericality: {greater_than_or_equal_to: 1}
end
