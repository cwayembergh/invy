class User < ActiveRecord::Base
  attr_accessor :current_password
  has_many :user_accounts, dependent: :destroy
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :password, presence: true, :length => {:within => 8..128}, :on => :create
  validates :password, presence: true, :length => {:within => 8..128}, :on => :update
  has_many :emails, dependent: :destroy
  accepts_nested_attributes_for :emails, allow_destroy: true
  has_secure_password
  validate :current_password_is_correct, :on => :update
  
  mount_uploader :profile_picture, ProfilePictureUploader
  
  # Update only if current_password is correct
  def current_password_is_correct
    if User.find(id).authenticate(current_password) == false
      errors.add(:current_password, "is missing or incorrect.")
    end
  end
  
  # Password reset token
  def send_password_reset
    generate_token(:reset_password_token)
    self.reset_password_sent_at = Time.zone.now
    save!(:validate => false)
    UserMailer.password_reset(self).deliver_now
  end
  
  # Activate account token
  def send_activate_account 
    generate_token(:confirmation_token)
    self.confirmation_sent_at = Time.zone.now
    save!(:validate => false)
    UserMailer.activate_account(self).deliver_now
  end
  
  # Send invoice and invitation
  def send_invoice_and_invitation
    generate_token(:confirmation_token)
    self.confirmation_sent_at = Time.zone.now
    hash = SecureRandom.hex(4)
    self.password = hash
    self.password_confirmation = hash
    save!(:validate => false)
    UserMailer.send_invoice_and_invitation(self, hash).deliver_now
  end
  
  # Generate a token
  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end
  
  # Returns true if the given token matches the one saved in DB
  def correct_confirmed?(token)
    if token == self.confirmation_token
      return true
    else
      return false
    end
  end
  
  # Returns if the user has already confirmed his email address or not
  def activated?
    return self.confirmed
  end
  
  def activate_account
    self.update_attribute(:confirmed, true)
    self.update_attribute(:confirmed_at, Time.zone.now)
  end
end
