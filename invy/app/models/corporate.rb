class Corporate < ActiveRecord::Base
  attr_accessor :current_corporate_password
  has_one :account, dependent: :destroy
  has_many :list_clients, dependent: :destroy
  validates :company_name, presence: true
  validates :company_phone, presence: true
  validates :company_street, presence: true
  validates :company_street_number, presence: true
  validates :company_postal_code, presence: true
  validates :company_city, presence: true
  validates :tva_number, presence: true
  validates :bank_account, presence: true
  
  validate :current_corporate_password_is_correct, :on => :update
  
  mount_uploader :company_image, CompanyImageUploader
  
  
  # Update only if current_password of the User managing that particular corporate account is correct
  def current_corporate_password_is_correct
    if User.find(Corporate.find(id).account.user_account.user_id).authenticate(current_corporate_password) == false
      errors.add(:current_corporate_password, "is missing or incorrect.")
    end
  end
end
