class LedgerItem < ActiveRecord::Base
  belongs_to :sender, :class_name => 'Account'
  belongs_to :receiver, :class_name => 'Account'
  
  has_many :ledger_lines
  
  before_create :set_receiver, :send_invoice_email

  accepts_nested_attributes_for :ledger_lines, :allow_destroy => true
  
  attr_accessor :email
  
  validates :email, :due_date, presence: true
  
  def set_receiver
    user_id = Email.where(email: self.email.downcase).take.user_id
    user = User.where(id: user_id).first
    user_accounts = user.user_accounts.first
    account_id = user_accounts.account_id
    self.receiver_id = account_id
  end
  
  def send_invoice_email 
    user_id = Email.where(email: self.email.downcase).take.user_id
    user = User.where(id: user_id).first
    user_accounts = user.user_accounts.first
    account = Account.where(id: user_accounts.account_id)
    # IF THE USER DOESN'T HAVE A COMPLETE ACCOUNT YET, SEND AN EMAIL
    if !account.where("classic_id > ?", 0).exists? && !account.where("corporate_id > ?",0).exists? && !account.where("accountant_id > ?", 0).exists?
      #send invitation link
      user.send_invoice_and_invitation
    end
  end
  
  def to_param
    token
  end  
end
