# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).ready ->
	$(document).bind 'ajaxError', 'form#edit_corporate', (event, jqxhr, settings, exception) ->
		$(event.data).render_corporate_edit_form_errors $.parseJSON(jqxhr.responseText)
		return
	return


(($) ->
	$.fn.render_corporate_edit_form_errors = (errors) ->
		$form = this
		model = 'corporate'
		$('.error_messages').html ''
		$(".errors-container").removeClass 'hide'
		
		$.each errors, (field, messages) ->
			$input = $('input[name="corporate[' + field + ']"]')
			$input.closest('.form-control').addClass('edit_with_errors')
			$('.error_messages').append ("<li>"+field+" "+messages+"</li>")
			return
		return
	return
	) jQuery