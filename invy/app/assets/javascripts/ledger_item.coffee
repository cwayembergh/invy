# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery -> 
	id = 0
	
	### FUNCTION TO ADD LINE ###
	add_lines = (link, association, content) ->
		id += 1
		regexp = new RegExp('new_' + association, 'g')
		new_content = content.replace(regexp, id)
		$('#ledger-lines').append new_content

	### ADDING LINE ###
	$('a.link_to_add_lines').on 'click', (e) ->
		e.preventDefault()
		link = $(this)
		association = $(this).data('association')
		content = $(this).data('content')
		add_lines link, association, content
		
	### REMOVING A LINE ###
	$(document).on 'click', 'a.link_to_remove_fields', (e) ->
		e.preventDefault()
		### GET CURRENT VALUES ###
		total_price_line_value = $(this).closest('.nested-fields').find('.total_price').val()
		total_with_taxes_line_value = $(this).closest('.nested-fields').find('.total_price_with_taxes').val()
		total_without_vat_value = $('#total_without_vat').val()
		total_with_vat_value = $('#total_with_vat').val()
		
		### COMPUTE NEW VALUES ###
		new_total_without_vat = parseFloat(Math.round((total_without_vat_value - total_price_line_value) * 100) / 100)
		new_total_with_vat = parseFloat(Math.round((total_with_vat_value - total_with_taxes_line_value) * 100) / 100)
		new_total_vat = parseFloat(Math.round((new_total_with_vat - new_total_without_vat) * 100) / 100)
		
		### SET VALUES TO 0 FOR HIDDEN LINE ###
		$(this).closest('.nested-fields').find('.total_price').val 0
		$(this).closest('.nested-fields').find('.total_price_with_taxes').val 0
		
		### SET BOTTOM VALUES ###
		$('#total_without_vat').val new_total_without_vat
		$('#total_vat').val new_total_vat
		$('#total_with_vat').val new_total_with_vat
		$('#total_amount').text new_total_with_vat
		
		
		$(this).prev().val true
		$(this).closest('.nested-fields').hide()
	
	
	### DISPLAY CURRENCY ON LOAD ###
	if $('#ledger_item_currency').val() == 'EUR'
		currency = '€'
	else
		currency = '$'
	$('.currency-value').empty()
	$('.currency-value').append currency
	
	
	### UPDATE CURRENCY VALUE ###
	$('#ledger_item_currency').on 'change', ->
		if $('#ledger_item_currency').val() == 'EUR'
			currency = '€'
		else
			currency = '$'
		$('.currency-value').empty()
		$('.currency-value').append currency
	
	### CALCULATING PRICES ###
	$(document).on 'keyup', '.form-invoice', ->
		total_without_vat = 0
		total_vat = 0
		total_with_vat = 0
		
		quantity = $(this).closest('.nested-fields').find('#quantity').val() or 0
		unit_price = parseFloat($(this).closest('.nested-fields').find('#unit_price').val()) or 0
		tax_rate = $(this).closest('.nested-fields').find('#tax_rate').val() or 0
		
		total_price = parseFloat(quantity * unit_price) or 0
		total_tax = parseFloat(Math.round(total_price * tax_rate) / 100) or 0
		total_price_with_taxes = parseFloat(Math.round((total_price + total_tax) * 100) / 100) or 0
		
		$(this).closest('.nested-fields').find('.total_price').val total_price
		$(this).closest('.nested-fields').find('.total_price_with_taxes').val total_price_with_taxes
		
		$('.total_price').each ->
			value = parseFloat($(this).val())
			total_without_vat = parseFloat(Math.round((total_without_vat + value) * 100) / 100) or 0
		
		$('.total_price_with_taxes').each ->
			value = parseFloat($(this).val())
			total_with_vat = parseFloat(Math.round((total_with_vat + value) * 100) / 100) or 0
			
		# COMPUTE TOTAL VAT
		total_vat = parseFloat(Math.round((total_with_vat - total_without_vat) * 100) / 100) or 0
		$('#total_without_vat').val total_without_vat
		$('#total_vat').val total_vat
		$('#total_with_vat').val total_with_vat
		$('#total_amount').text total_with_vat
		
	### UPDATE VALUES WITH NEW TAX ###	
	$(document).on 'change', '#tax_rate', ->
		total_without_vat = 0
		total_vat = 0
		total_with_vat = 0
		
		tax_rate = $(this).closest('.nested-fields').find('#tax_rate').val()
		total_price = $(this).closest('.nested-fields').find('.total_price').val()
		total_tax = parseFloat(Math.round(total_price * tax_rate) / 100) or 0
		total_price_with_taxes = parseFloat(Math.round((parseFloat(total_price) + parseFloat(total_tax)) * 100) / 100) or 0
		$(this).closest('.nested-fields').find('.total_price_with_taxes').val total_price_with_taxes
		
		$('.total_price').each ->
			value = parseFloat($(this).val())
			total_without_vat = parseFloat(Math.round((total_without_vat + value) * 100) / 100) or 0
		
		$('.total_price_with_taxes').each ->
			value = parseFloat($(this).val())
			total_with_vat = parseFloat(Math.round((total_with_vat + value) * 100) / 100) or 0
			
		# COMPUTE TOTAL VAT
		total_vat = parseFloat(Math.round((total_with_vat - total_without_vat) * 100) / 100) or 0
		$('#total_without_vat').val total_without_vat
		$('#total_vat').val total_vat
		$('#total_with_vat').val total_with_vat
		$('#total_amount').text total_with_vat