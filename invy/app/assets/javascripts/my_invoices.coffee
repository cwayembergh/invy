ready = ->
	$('#invoices_menu_list li:first').addClass 'active'
	$('.invoices-content').hide()
	$('.invoices-content:first').show()
	
	$('#invoices_menu_list li a').click ->
		id = $(this).attr('id')
		
		if !$(this).closest('li').hasClass('active')
			$('#invoices_menu_list li').removeClass 'active'
			$(this).closest('li').addClass 'active'
			$('.invoices-content').hide()
			$('.error_messages').html ''
			$(".errors-profile-container").addClass 'hide'
			$('.form-control').removeClass 'edit_with_errors'
			
			$(id).fadeIn 'fast'
	
	
		
$(document).ready(ready)
$(document).on('page:load', ready)