# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#binding ajax error
$(document).ready ->
	$(document).bind 'ajaxError', 'form#edit_user', (event, jqxhr, settings, exception) ->
		$(event.data).render_user_edit_form_errors $.parseJSON(jqxhr.responseText)
		return
	return


(($) ->
	$.fn.render_user_edit_form_errors = (errors) ->
		$form = this
		model = 'user'
		$('.error_messages').html ''
		$(".errors-profile-container").removeClass 'hide'
		
		
		
		$.each errors, (field, messages) ->
			$input = $('input[name="' + model + '[' + field + ']"]')
			$input.closest('.form-control').addClass('edit_with_errors')
			$('.error_messages').append ("<li>"+field+" "+messages+"</li>")
			return
		return
	return
	) jQuery