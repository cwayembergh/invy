# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
	$('#menu_list li:first').addClass 'active'
	$('.profile-content').hide()
	$('.profile-content:first').show()
	
	$('#menu_list li a').click ->
		id = $(this).attr('id')
		
		if !$(this).closest('li').hasClass('active')
			$('#menu_list li').removeClass 'active'
			$(this).closest('li').addClass 'active'
			$('.profile-content').hide()
			$('.error_messages').html ''
			$(".errors-profile-container").addClass 'hide'
			$('.form-control').removeClass 'edit_with_errors'
			
			$(id).fadeIn 'fast'
			
			
		return
$(document).ready(ready)
$(document).on('page:load', ready)