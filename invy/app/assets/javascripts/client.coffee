# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
	$('#client_menu_list li:first').addClass 'active'
	$('.client-content').hide()
	$('.client-content:first').show()
	
	$('#client_menu_list li a').click ->
		id = $(this).attr('id')
		
		if !$(this).closest('li').hasClass('active')
			$('#client_menu_list li').removeClass 'active'
			$(this).closest('li').addClass 'active'
			$('.client-content').hide()
			$('.error_messages').html ''
			$(".errors-profile-container").addClass 'hide'
			$('.form-control').removeClass 'edit_with_errors'
			
			$(id).fadeIn 'fast'
		return
	
	$("#add_classic_client_button").on "click", ->
		$("#new_client").find('input[type="text"]').val("");
		$("#new_client").find('input[type="text"]').removeAttr("checked");
		$("#new_client").find('input[type="text"]').removeAttr("selected");
		$(".errors-client-container").addClass 'hide'
		$('.error_messages').html ''
		$('.form-group.has-error', "#new_client").each ->
			$(this).removeClass 'has-error'
			return
		return
	
	$("#add_corporate_client_button").on "click", ->
		$("#new_client").find('input[type="text"]').val("");
		$("#new_client").find('input[type="text"]').removeAttr("checked");
		$("#new_client").find('input[type="text"]').removeAttr("selected");
		$(".errors-client-container").addClass 'hide'
		$('.error_messages').html ''
		$('.form-group.has-error', "#new_client").each ->
			$(this).removeClass 'has-error'
			return
		return	
	
$(document).ready(ready)
$(document).on('page:load', ready)


#binding ajax error
$(document).ready ->
	$(document).bind 'ajaxError', 'form#new_client', (event, jqxhr, settings, exception) ->
		$(event.data).render_client_form_errors $.parseJSON(jqxhr.responseText)
		return
	return


(($) ->

	$.fn.render_client_form_errors = (errors) ->
		$form = this
		$('.error_messages').html ''
		model = @data('model')
		$(".errors-client-container").removeClass 'hide'
		$.each errors, (field, messages) ->
			$input = $('input[name="' + model + '[' + field + ']"]')
			$input.closest('.form-group').addClass('field_with_errors')
			$('.error_messages').append ("<li>"+field+" "+messages+"</li>")
			return
		return
	return
	) jQuery