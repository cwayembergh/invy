# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

ready = ->
	$('#usage_menu_list li:first').addClass 'active'
	$('.usage-content').hide()
	$('.usage-content:first').show()
	
	$('#usage_menu_list li a').click ->
		id = $(this).attr('id')
		
		if !$(this).closest('li').hasClass('active')
			$('#usage_menu_list li').removeClass 'active'
			$(this).closest('li').addClass 'active'
			$('.usage-content').hide()
			$('.error_messages').html ''
			$(".errors-profile-container").addClass 'hide'
			$('.form-control').removeClass 'edit_with_errors'
			
			$(id).fadeIn 'fast'
			
		# DISPLAY GRAPHS	
		if id == "#overview"
			gross_volume_graph()
			
		if id == "#payments_sent"
			sent_payments_graph()
			
		if id == "#payments_received"
			received_payments_graph()	
			
		return
	
	
		
$(document).ready(ready)
$(document).on('page:load', ready)