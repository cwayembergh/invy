module Constraints
  class AccountantAccount
    def matches?(request)
      user = User.find_by_id(request.session['invy'])
      accountant?(user)
    end
    
    def accountant?(user)
      if UserAccount.where(user_id: user.id).exists?
        account = Account.where(id: user.user_account.account_id).first
        if account['accountant_id'] > 0
          return true
        else 
          return false
        end
      else
        return false
      end
    end
  end
end