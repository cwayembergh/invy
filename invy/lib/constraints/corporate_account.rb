module Constraints
  class CorporateAccount
    def matches?(request)
      user = User.find_by_id(request.session['invy'])
      corporate?(user)
    end
    
    def corporate?(user)
      if UserAccount.where(user_id: user.id).exists?
        account = Account.where(id: user.user_account.account_id).first
        if account['corporate_id'] > 0
          return true
        else 
          return false
        end
      else
        return false
      end
    end
  end
end