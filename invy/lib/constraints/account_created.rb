module Constraints
  class AccountCreated
    def matches?(request)
      user = User.find_by_id(request.session['invy'])
      created?(user)
    end
    
    def created?(user)
      # IF USER ALREADY HAS AN ACCOUNT THEN TRUE OTHERWISE FALSE
      accounts = Account.where(id: user.user_accounts.map(&:account_id))
      if accounts.where("classic_id > ?",0).exists? or accounts.where("corporate_id > ?",0).exists? or accounts.where("accountant_id > ?",0).exists?
        true
      else
        false
      end
    end
  end
end