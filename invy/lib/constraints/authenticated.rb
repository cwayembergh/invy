module Constraints
  class Authenticated
    def matches?(request)
      request.session['invy'].present?
    end
  end
end