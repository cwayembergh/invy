module Constraints
  class ClassicAccount
    def matches?(request)
      user = User.find_by_id(request.session['invy'])
      classic?(user)
    end
    
    def classic?(user)
      if UserAccount.where(user_id: user.id).exists?
        account = Account.where(id: user.user_account.account_id).first
        if account['classic_id'] > 0
          return true
        else 
          return false
        end
      else
        return false
      end
    end
  end
end