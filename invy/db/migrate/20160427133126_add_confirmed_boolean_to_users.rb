class AddConfirmedBooleanToUsers < ActiveRecord::Migration
  def change
    add_column :users, :confirmed, :boolean, default: false
    add_index :users, :reset_password_token
    add_index :users, :confirmation_token
  end
end
