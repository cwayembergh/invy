class AddIndexToEmails < ActiveRecord::Migration
  def change
    add_index :emails, :email
  end
end
