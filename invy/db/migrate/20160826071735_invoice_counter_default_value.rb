class InvoiceCounterDefaultValue < ActiveRecord::Migration
  def change
    change_column :counters, :invoice_counter, :string, :default => "INV"+"%.7d"%0
  end
end
