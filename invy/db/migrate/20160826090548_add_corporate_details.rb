class AddCorporateDetails < ActiveRecord::Migration
  def change
    add_column :corporates, :company_name, :string
    add_column :corporates, :company_phone, :string
    add_column :corporates, :company_street, :string
    add_column :corporates, :company_street_number, :string
    add_column :corporates, :company_postal_code, :string
    add_column :corporates, :company_city, :string
    add_column :corporates, :tva_number, :string
  end
end
