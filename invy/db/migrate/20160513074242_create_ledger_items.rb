class CreateLedgerItems < ActiveRecord::Migration
  def change
    create_table :ledger_items do |t|
      t.references :sender, index: true
      t.references :receiver, index: true
      t.string :token, index: true
      t.string :ledger_item_type, default: "invoice"
      t.string :ref_number, index: true
      t.string :sender_ref_number, index: true
      t.string :currency
      t.datetime :issue_date
      t.datetime :due_date
      t.datetime :payment_date
      t.datetime :period_start
      t.datetime :period_end
      t.string :ledger_status, default: "new"
      t.string :payment_method
      t.decimal :total_taxes, default: 0
      t.decimal :total_with_taxes, default: 0
      t.decimal :total_without_taxes, default: 0
      t.string :communication
      t.timestamps null: false
    end
  end
end
