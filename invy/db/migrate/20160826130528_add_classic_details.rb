class AddClassicDetails < ActiveRecord::Migration
  def change
    add_column :classics, :classic_street, :string
    add_column :classics, :classic_street_number, :string
    add_column :classics, :classic_postal_code, :string
    add_column :classics, :classic_city, :string
    add_column :classics, :classic_phone, :string
  end
end
