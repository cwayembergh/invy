class AddDetailsToClients < ActiveRecord::Migration
  def change
    add_column :clients, :name, :string
    add_column :clients, :address_number, :string
    add_column :clients, :postal_code_city, :string
    add_column :clients, :country, :string
    add_column :clients, :phone_number, :string
    add_column :clients, :mobile_number, :string
    add_column :clients, :fax_number, :string
    add_column :clients, :website, :string
    change_column_null :clients, :name, false
    change_column_null :clients, :address_number, false
    change_column_null :clients, :postal_code_city, false
    change_column_null :clients, :country, false
    change_column_null :clients, :phone_number, false
    change_column_null :clients, :mobile_number, false
    change_column_null :clients, :fax_number, false
    change_column_null :clients, :website, false
    add_index :clients, :email
  end
end
