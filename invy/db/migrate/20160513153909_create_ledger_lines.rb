class CreateLedgerLines < ActiveRecord::Migration
  def change
    create_table :ledger_lines do |t|
      t.references :ledger_item, index: true, foreign_key: true
      t.string :product_name
      t.string :product_description
      t.integer :quantity, default: 0
      t.decimal :unit_price, default: 0
      t.integer :tax_rate, default: 0
      t.decimal :total_price, default: 0
      t.decimal :total_price_with_taxes, default: 0
      t.timestamps null: false
    end
  end
end
