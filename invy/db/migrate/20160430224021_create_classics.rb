class CreateClassics < ActiveRecord::Migration
  def change
    create_table :classics do |t|
      t.boolean :default_account, default: false
      t.timestamps null: false
    end
  end
end
