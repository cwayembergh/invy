class CreateUserAccounts < ActiveRecord::Migration
  def change
    create_table :user_accounts do |t|
      t.references :user, index: true, foreign_key: true, null: false, default: 0
      t.references :account, index: true, foreign_key: true, null: false, default: 0
      
      t.timestamps null: false
    end
  end
end
