class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.references :classic, index: true, foreign_key: true
      t.references :corporate, index: true, foreign_key: true
      t.references :accountant, index: true, foreign_key: true
      
      t.timestamps null: false
    end
  end
end
