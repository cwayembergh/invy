class CreateCounters < ActiveRecord::Migration
  def change
    create_table :counters do |t|
      t.string :invoice_counter, null: false

      t.timestamps null: false
    end
  end
end
