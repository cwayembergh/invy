class AddCompanyImageToCorporates < ActiveRecord::Migration
  def change
    add_column :corporates, :company_image, :string
  end
end
