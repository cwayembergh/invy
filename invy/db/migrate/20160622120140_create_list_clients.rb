class CreateListClients < ActiveRecord::Migration
  def change
    create_table :list_clients do |t|
      t.references :corporate, index: true, foreign_key: true, null: false, default: 0
      t.references :client, index: true, foreign_key: true, null: false, default: 0

      t.timestamps null: false
    end
  end
end
