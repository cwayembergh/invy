class AddBankAccounts < ActiveRecord::Migration
  def change
    add_column :classics, :bank_account, :string
    add_column :corporates, :bank_account, :string
    add_column :accountants, :bank_account, :string
    change_column_null :classics, :bank_account, false
    change_column_null :corporates, :bank_account, false
    change_column_null :accountants, :bank_account, false
  end
end
