# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161015100817) do

  create_table "accountants", force: :cascade do |t|
    t.boolean  "default_account", default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "bank_account",                    null: false
  end

  create_table "accounts", force: :cascade do |t|
    t.integer  "classic_id"
    t.integer  "corporate_id"
    t.integer  "accountant_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "accounts", ["accountant_id"], name: "index_accounts_on_accountant_id"
  add_index "accounts", ["classic_id"], name: "index_accounts_on_classic_id"
  add_index "accounts", ["corporate_id"], name: "index_accounts_on_corporate_id"

  create_table "classics", force: :cascade do |t|
    t.boolean  "default_account",       default: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "bank_account",                          null: false
    t.string   "classic_street"
    t.string   "classic_street_number"
    t.string   "classic_postal_code"
    t.string   "classic_city"
    t.string   "classic_phone"
  end

  create_table "clients", force: :cascade do |t|
    t.string   "email",            null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "address_number",   null: false
    t.string   "postal_code_city", null: false
    t.string   "country",          null: false
    t.string   "phone_number",     null: false
    t.string   "mobile_number",    null: false
    t.string   "fax_number",       null: false
    t.string   "website",          null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "company_name"
  end

  add_index "clients", ["email"], name: "index_clients_on_email"

  create_table "corporates", force: :cascade do |t|
    t.boolean  "default_account",       default: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.string   "bank_account",                          null: false
    t.string   "company_name"
    t.string   "company_phone"
    t.string   "company_street"
    t.string   "company_street_number"
    t.string   "company_postal_code"
    t.string   "company_city"
    t.string   "tva_number"
    t.string   "company_image"
  end

  create_table "counters", force: :cascade do |t|
    t.string   "invoice_counter", default: "INV0000000", null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "emails", force: :cascade do |t|
    t.string   "email",      null: false
    t.integer  "user_id",    null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "emails", ["email"], name: "index_emails_on_email"
  add_index "emails", ["user_id"], name: "index_emails_on_user_id"

  create_table "ledger_items", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.string   "token"
    t.string   "ledger_item_type",    default: "invoice"
    t.string   "ref_number"
    t.string   "sender_ref_number"
    t.string   "currency"
    t.datetime "issue_date"
    t.datetime "due_date"
    t.datetime "payment_date"
    t.datetime "period_start"
    t.datetime "period_end"
    t.string   "ledger_status",       default: "new"
    t.string   "payment_method"
    t.decimal  "total_taxes",         default: 0.0
    t.decimal  "total_with_taxes",    default: 0.0
    t.decimal  "total_without_taxes", default: 0.0
    t.string   "communication"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  add_index "ledger_items", ["receiver_id"], name: "index_ledger_items_on_receiver_id"
  add_index "ledger_items", ["ref_number"], name: "index_ledger_items_on_ref_number"
  add_index "ledger_items", ["sender_id"], name: "index_ledger_items_on_sender_id"
  add_index "ledger_items", ["sender_ref_number"], name: "index_ledger_items_on_sender_ref_number"
  add_index "ledger_items", ["token"], name: "index_ledger_items_on_token"

  create_table "ledger_lines", force: :cascade do |t|
    t.integer  "ledger_item_id"
    t.string   "product_name"
    t.string   "product_description"
    t.integer  "quantity",               default: 0
    t.decimal  "unit_price",             default: 0.0
    t.integer  "tax_rate",               default: 0
    t.decimal  "total_price",            default: 0.0
    t.decimal  "total_price_with_taxes", default: 0.0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "ledger_lines", ["ledger_item_id"], name: "index_ledger_lines_on_ledger_item_id"

  create_table "list_clients", force: :cascade do |t|
    t.integer  "corporate_id", default: 0, null: false
    t.integer  "client_id",    default: 0, null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "list_clients", ["client_id"], name: "index_list_clients_on_client_id"
  add_index "list_clients", ["corporate_id"], name: "index_list_clients_on_corporate_id"

  create_table "user_accounts", force: :cascade do |t|
    t.integer  "user_id",    default: 0, null: false
    t.integer  "account_id", default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "user_accounts", ["account_id"], name: "index_user_accounts_on_account_id"
  add_index "user_accounts", ["user_id"], name: "index_user_accounts_on_user_id"

  create_table "users", force: :cascade do |t|
    t.string   "first_name",                             null: false
    t.string   "last_name",                              null: false
    t.string   "password_digest",                        null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.string   "confirmation_token"
    t.datetime "confirmation_sent_at"
    t.boolean  "confirmed",              default: false
    t.datetime "confirmed_at"
    t.string   "profile_picture"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token"
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token"

end
