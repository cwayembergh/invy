require 'constraints/authenticated'
require 'constraints/account_created'

Rails.application.routes.draw do
  
  scope module: 'user_controllers' do
    resources :account_activations, only: [:new, :create, :edit]
    resources :password_resets, only: [:new, :create, :edit, :update]
    resources :sessions, only: [:new, :create, :destroy]
    resources :users, only: [:new, :create, :edit, :update]
    resources :user_pages
  end
  
  scope module: 'account_controllers' do
    resources :classics
    resources :accountants
    resources :corporates
  end
  
  scope module: 'ledger_item_controllers' do
    resources :ledger_items
  end
  
  scope module: 'client_controllers' do
    resources :clients
  end
  
  
  
  constraints(Constraints::Authenticated.new) do
    #IF ACCOUNT IS CREATED
    constraints(Constraints::AccountCreated.new) do
      root 'account_pages_controllers/account_pages#account_dispatcher', as: :account_authenticated
      get 'add_account' => 'account_pages_controllers/account_pages#add'
      get 'pay' => 'ledger_item_controllers/payment#pay'
      get 'switch_classic' => 'account_pages_controllers/account_pages#switch_to_classic'
      get 'switch_corporate' => 'account_pages_controllers/account_pages#switch_to_corporate'
      get 'switch_accountant' => 'account_pages_controllers/account_pages#switch_to_accountant'
      get 'usage' => 'usage_controllers/usages#usage'
      get 'invoices' => 'my_invoices_controllers/my_invoices#invoices'
      
      scope '/classic' do
        root 'account_pages_controllers/classic_pages#dashboard', as: :classic_account
        get 'classic_usage' => 'usage_controllers/usages#show'
        
        
      end
      
      scope '/corporate' do
        root 'account_pages_controllers/corporate_pages#dashboard', as: :corporate_account
        get 'create_invoice' => 'ledger_item_controllers/ledger_items#new'
        get 'clients' => 'client_controllers/clients#index'
        get 'corporate_usage' => 'usage_controllers/usages#show'
      end
      
      scope '/accountant' do
        root 'account_pages_controllers/accountant_pages#dashboard', as: :accountant_account
      end
    end
    
    #IF ACCOUNT IS NOT YET CREATED
    root 'user_controllers/user_pages#dashboard', as: :user_authenticated
    get 'add_classic' => 'account_controllers/classics#new'
    get 'add_corporate' => 'account_controllers/corporates#new'
    get 'add_accountant' => 'account_controllers/accountants#new'
    get 'profile' => 'user_controllers/user_pages#profile'
  end
  
  
  root 'static_pages#home'
  #ACCOUNT_ACTIVATIONS CONTROLLER
  get 'resend_activation' => 'user_controllers/account_activations#new'
  #PASSWORD_RESET CONTROLLER
  get 'reset_password' => 'user_controllers/password_resets#new'
  #SESSION CONTROLLER
  get 'login' => 'user_controllers/sessions#new'
  delete 'logout' => 'user_controllers/sessions#destroy'
  #USER CONTROLLER
  get 'signup' => 'user_controllers/users#new'
  get 'verification' => 'user_controllers/users#verification'
  
  #Redirec to root if trying to access other pages
  get '*path' => redirect('/')
end
